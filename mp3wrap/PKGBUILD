# Maintainer (Arch): Tobias Powalowski <tpowa@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=mp3wrap
pkgver=0.5
_debver=$pkgver
_debrel=4
pkgrel=1
pkgdesc="Commandline tool for wrapping mp3 files"
arch=('i686' 'x86_64')
url="http://mp3wrap.sourceforge.net"
license=('LGPL-2')
depends=('glibc')
makedepends=('quilt')
source=("https://downloads.sourceforge.net/sourceforge/$pkgname/$pkgname-$pkgver-src.tar.gz"
        "https://deb.debian.org/debian/pool/main/m/mp3wrap/mp3wrap_$_debver-$_debrel.debian.tar.xz")
sha512sums=('2dc7a72a12b9245956000443a51cd5341e1d48795b629f6277f9dd26ac9b3ab2511a119478a10daa9516cb2526073956af42f83410aff1e4eae0c972fdd8b0f1'
            'a5f515492fb22ea21cfd822f1c4db1060619d83f97a81e640a7f102461267a8c0e8b18a9dc57c52446c2ccc76a764dcc2ef99c1dbabc4c3c7b714f047fee1d3b')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr --mandir=/usr/share/man
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
