# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=qt-sensors
_qtver=5.15.2
pkgver=${_qtver/-/}
_debver=$pkgver
_debrel=2
pkgrel=2
arch=('i686' 'x86_64')
url='https://www.qt.io'
license=('LGPL-3')
pkgdesc='Provides access to sensor hardware and motion gesture recognition'
depends=('qt-base')
makedepends=('qt-declarative' 'quilt')
optdepends=('qt-declarative: QML bindings')
groups=('qt')
replaces=('qt5-sensors')
conflicts=('qt5-sensors')
provides=('qt5-sensors')
_pkgfqn="${pkgname/-/}-everywhere-src-${_qtver}"
source=("https://download.qt.io/official_releases/qt/${pkgver%.*}/${_qtver}/submodules/${_pkgfqn}.tar.xz"
        "https://deb.debian.org/debian/pool/main/q/qtsensors-opensource-src/qtsensors-opensource-src_$_debver-$_debrel.debian.tar.xz")
sha512sums=('d0a34e0718cc727b1256601bc5f9a2975532d728fdf0cb7678824c7d36aa5049d9c2886053821ec93a238120441c980027306ac633677617867c7aee40bb560b'
            '8a691a46206521619515369cd73fd0b6ac4c6d256032e0a4975a745019900e4866e048b068462422c028a147789284f8e3a0b94c5300710ee76590c789174dac')

prepare() {
  mkdir -p build

  cd ${_pkgfqn}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd build

  qmake ../${_pkgfqn}
  make
}

package() {
  cd build
  make INSTALL_ROOT="$pkgdir" install

  # drop QMAKE_PRL_BUILD_DIR because reference the build dir
  find "$pkgdir/usr/lib" -type f -name '*.prl' \
    -exec sed -i -e '/^QMAKE_PRL_BUILD_DIR/d' {} \;

  # install license
  install -D -m644 "${srcdir}"/${_pkgfqn}/LICENSE.LGPL3 "${pkgdir}"/usr/share/licenses/${pkgname}/LICENSE.LGPL3
}
