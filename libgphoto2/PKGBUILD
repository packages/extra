# Maintainer (Arch): Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor (Arch): Jan de Groot <jgc@archlinux.org>
# Contributor (Arch): Tom Gundersen <teg@jklm.no>
# Contributor (Arch): Eduardo Romero <eduardo@archlinux.org>
# Contributor (Arch): Damir Perisa <damir.perisa@bluewin.ch>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=libgphoto2
pkgver=2.5.27
_debver=$pkgver
_debrel=1
pkgrel=2
pkgdesc="Digital camera access library"
url='https://gphoto.sourceforge.io/'
arch=('i686' 'x86_64')
license=('LGPL-2.1')
depends=('libexif' 'libjpeg-turbo' 'gd' 'libltdl' 'libusb' 'libxml2' 'curl')
makedepends=('autoconf-archive' 'quilt')
source=("https://downloads.sourceforge.net/gphoto/${pkgname}-${pkgver}.tar.bz2"{,.asc}
        "https://deb.debian.org/debian/pool/main/libg/libgphoto2/libgphoto2_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('7cc82977e62034c539c7e62bff62ee61a299d3adb0c42e88b8e5521e75eedbc22437b9520af8a4bbd7dc0639ce5a8a10b1f5e5e8075e9b73a566da9061c958f8'
            'SKIP'
            '0e95edf90d4389b8ab4170e8c2a946265dc2a647eff5549a39d9d9d45367e62d3e83e06f4c5d59c773abaf7bbe4a29f12cced707421ddeb49123852b67a53d51')
validpgpkeys=('7C4AFD61D8AAE7570796A5172209D6902F969C95') # Marcus Meißner (german spelling) <meissner@suse.de>

prepare() {
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  autoreconf -fvi
}

build() {
  cd $pkgname-$pkgver
  ./configure \
    --prefix=/usr \
    --without-hotplug-doc-dir \
    --without-hotplug-usermap-dir \
    --disable-rpath

  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0 /g' -e 's/    if test "$export_dynamic" = yes && test -n "$export_dynamic_flag_spec"; then/      func_append compile_command " -Wl,-O1,--as-needed"\n      func_append finalize_command " -Wl,-O1,--as-needed"\n\0/' libtool

  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install

  # remove udev helper
  rm -r "$pkgdir/usr/lib/udev"

  # install license
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}