# Maintainer (Arch): Eric Bélanger <eric@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: Jesús E.

_pkgoriginalname=pyasn1
pkgbase=python-$_pkgoriginalname
pkgname=('python-pyasn1' 'tauthon-pyasn1')
pkgver=0.4.8
_debver=$pkgver
_debrel=1
pkgrel=2
arch=('any')
url="https://github.com/etingof/pyasn1"
license=('Simplified-BSD')
makedepends=('python-setuptools' 'tauthon-setuptools' 'quilt')
replaces=('pyasn1')
provides=('pyasn1')
source=("https://pypi.io/packages/source/p/${_pkgoriginalname}/${_pkgoriginalname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/p/${_pkgoriginalname}/${_pkgoriginalname}_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('e64e70b325c8067f87ace7c0673149e82fe564aa4b0fa146d29b43cb588ecd6e81b1b82803b8cfa7a17d3d0489b6d88b4af5afb3aa0052bf92e8a1769fe8f7b0'
            'cb84a0a2df1efc68473f5ee226a432b60f47021ed93b306f95dfb655c8db6c7f58ace16515502e4f7f6212fb51ec5e5c730b3ef1ea0b245067e8627b3caee29d')

prepare() {
  cd "$_pkgoriginalname-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

package_python-pyasn1() {
  pkgdesc="ASN.1 library for Python"
  depends=('python')

  cd "$_pkgoriginalname-$pkgver"
  python setup.py install --root="$pkgdir"
  install -Dm644 LICENSE.rst -t "$pkgdir/usr/share/licenses/$pkgname"
}

package_tauthon-pyasn1() {
  pkgdesc="ASN.1 library for Tauthon"
  depends=('tauthon')

  cd "$_pkgoriginalname-$pkgver"
  tauthon setup.py install --root="$pkgdir"
  install -Dm644 LICENSE.rst -t "$pkgdir/usr/share/licenses/$pkgname"
}
