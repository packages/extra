# Maintainer (Arch): Evangelos Foutras <evangelos@foutrelis.com>
# Contributor (Arch): Tobias Powalowski <tpowa@archlinux.org>
# Contributor (Arch): Hokum <hokum_at_mail_dot_ru>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=gptfdisk
_debname=gdisk
pkgver=1.0.9
_debver=$pkgver
pkgrel=2
_debrel=2.1
pkgdesc="A text-mode partitioning tool that works on GUID Partition Table (GPT) disks"
arch=('i686' 'x86_64')
url="https://www.rodsbooks.com/gdisk/"
license=('GPL-2')
depends=('gcc-libs' 'popt' 'util-linux' 'ncurses')
makedepends=('quilt')
provides=('gdisk')
conflicts=('gdisk')
replaces=('gdisk')
source=(https://downloads.sourceforge.net/project/$pkgname/$pkgname/$pkgver/$pkgname-$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/g/$_debname/${_debname}_$_debver-$_debrel.debian.tar.xz
        partitions.patch)
sha512sums=('c2489ac7e196cb53b9fdb18d0e421571eca43c366de8922c5c7f550aadf192558e7af69b181c30747d6cf607c1670126223465eaa5e231cc589402d94a4e97a2'
            '280b1898b0e60059e3e5f7d3c0acc657f5f6b2f1e6b35aea2369f2e5dda612e6b611c72963945704e0a6f231348e73e36ee22662463eae45597215a98b3fe9ed'
            'a5b9cbb53519ae17645a240a304d69467c94a772fb6e1654abeddf3c2032f877037ecb517c84b66f687eb13fa2c248d29186e6f15b1776aa95094eec2f7608f7')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  # improve partitions
  patch -p1 -i ../partitions.patch

  # https://sourceforge.net/p/gptfdisk/discussion/939590/thread/bc29ca06f8/
  sed -i '/^#include /s:ncursesw/::' gptcurses.cc
}

build() {
  cd "$srcdir/$pkgname-$pkgver"
  make
}

package () {
  cd "$srcdir/$pkgname-$pkgver"

  install -d "$pkgdir"/usr/{bin,share/{doc/gdisk,man/man8}}
  install -t "$pkgdir/usr/bin" {,c,s}gdisk fixparts
  install -m644 -t "$pkgdir/usr/share/man/man8" {{,c,s}gdisk,fixparts}.8
  install -m644 -t "$pkgdir/usr/share/doc/gdisk" README NEWS
  install -Dm 644 COPYING -t "$pkgdir"/usr/share/licenses/$pkgname
}
