# Maintainer (Arch): Anatol Pomozov <anatol.pomozov@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad

_gemname=mustache
pkgname=ruby-$_gemname
pkgver=1.1.1
_debver=$pkgver
_debrel=2
pkgrel=2
pkgdesc="Mustache is a framework-agnostic way to render logic-free views."
arch=('any')
url='https://github.com/defunkt/mustache'
license=('Expat')
depends=('ruby')
makedepends=('quilt')
options=(!emptydirs)
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/${_gemname}/${_gemname}/archive/refs/tags/v${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/r/ruby-mustache/ruby-mustache_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('3bfe85698ffc95828cb2c6d7ee77049a7dab3a60313289cb0b19e9210d17e4de043df913784c5322a0d9d2073ee6be66f973df9b290e939a0eab153727c31fab'
            '54abbc15593a1cbc95596540c03e9c89ee23fa6c2f2434e81d4c3e50fa3b703722d231270db2ecea5d6b8d1cfc70598ebd995dd2ef33d8fa6e970d3e88bf6644')

prepare() {
  cd $_gemname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build () {
  cd $_gemname-$pkgver
  gem build
}

package() {
  cd $_gemname-$pkgver
  local _gemdir="$(ruby -e'puts Gem.default_dir')"
  gem install --ignore-dependencies --no-user-install -N -i "$pkgdir/$_gemdir" -n "$pkgdir/usr/bin" $_gemname-$pkgver.gem
  rm "$pkgdir/$_gemdir/cache/$_gemname-$pkgver.gem"
  install -D -m644 "$pkgdir/$_gemdir/gems/$_gemname-$pkgver/LICENSE" -t "$pkgdir/usr/share/licenses/$pkgname"
}
