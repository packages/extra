# Maintainer (Arch): David Runge <dvzrv@archlinux.org>
# Contributor (Arch): Eric Bélanger <eric@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=libsamplerate
pkgver=0.2.1
pkgrel=1
pkgdesc="An audio sample rate conversion library"
arch=('i686' 'x86_64')
url="https://libsndfile.github.io/libsamplerate/"
license=('Simplified-BSD')
depends=('glibc')
makedepends=('alsa-lib' 'cmake' 'libsndfile' 'opus' 'flac' 'libvorbis')
checkdepends=('fftw')
source=("https://github.com/libsndfile/${pkgname}/releases/download/${pkgver}/${pkgname}-${pkgver}.tar.bz2"{,.sig})
sha512sums=('f54f7f12c9536868d7a11fc9cbb86857505e7b75fe34cedaf0b9bfc864da6037296b3eae303a33d4c87b7fd20d96933b91ef59c8cc3d1313b9fc21654e5daa2d'
            'SKIP')
validpgpkeys=('31D95CAB6D80D262244A1750A47620E801E47E95') # David Seifert soap@gentoo.org

build() {
  cd "${pkgname}-${pkgver}"
  cmake -DCMAKE_INSTALL_PREFIX=/usr \
        -DCMAKE_BUILD_TYPE='None' \
        -DBUILD_SHARED_LIBS=ON \
        -Wno-dev \
        -B build \
        -S .
  make VERBOSE=1 -C build
}

check() {
  cd "${pkgname}-${pkgver}"
  make test -C build
}

package() {
  cd "${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install -C build
  install -vDm 644 {AUTHORS,NEWS,README.md,ChangeLog} \
    -t "${pkgdir}/usr/share/doc/${pkgname}"
  install -vDm 644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
