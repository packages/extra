# Maintainer (Arch): Jaroslav Lichtblau <svetlemodry@archlinux.org>
# Contributor (Arch): Mateusz Herych <heniekk@gmail.com>
# Contributor (Arch): dibblethewrecker <dibblethewrecker.at.jiwe.dot.org>
# Contributor (Arch): William Rea <sillywilly@gmail.com>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=proj
pkgver=7.2.1
pkgrel=1
_gridver=1.8
pkgdesc='Cartographic Projections library'
arch=('i686' 'x86_64')
url="https://trac.osgeo.org/proj/"
license=('Expat')
depends=('libtiff')
makedepends=('autoconf' 'sqlite')
source=(https://github.com/OSGeo/PROJ/releases/download/$pkgver/$pkgname-$pkgver.tar.gz
        https://download.osgeo.org/$pkgname/$pkgname-datumgrid-$_gridver.zip)
noextract=("$pkgname-datumgrid-$_gridver.zip")
sha512sums=('59b9b31b0183e620a2f4a25a08620c170773fe4f99e8eca59e9ed6815f43bb379ea21ef71e8f759dbd747855b982657d7503bac3acc542218e0d862105f25324'
            '991206f17348b3de484eb5364d773cd06577057228c2d1a0a1c1658308e2596ca13338a666fa71ddd76d538f23dd5bf21e178fd26a785717edd847a17e5c0cd1')

prepare() {
  cd "${srcdir}"/$pkgname-$pkgver

  autoreconf -vfi
}

build(){
  cd "${srcdir}"/$pkgname-$pkgver

  ./configure --prefix=/usr
  make
}

package() {
  cd "${srcdir}"/$pkgname-$pkgver

  make DESTDIR="${pkgdir}" install

  install -Dm644 COPYING -t "${pkgdir}"/usr/share/licenses/$pkgname
  bsdtar --no-same-owner -xzvf "${srcdir}"/$pkgname-datumgrid-$_gridver.zip -C "${pkgdir}"/usr/share/$pkgname
}
