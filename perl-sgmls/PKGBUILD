# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

_cpanname=SGMLSpm
pkgname=perl-sgmls
epoch=1
pkgver=1.1
_debver=1.03ii
_debrel=36
pkgrel=3
pkgdesc="A Post-Processor for SGMLS and NSGMLS"
arch=('any')
url="https://search.cpan.org/dist/SGMLSpm"
license=('GPL-2')
depends=('perl')
makedepends=('quilt')
options=('!emptydirs' 'docs')
source=(https://search.cpan.org/CPAN/authors/id/R/RA/RAAB/SGMLSpm-$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/libs/libsgmls-perl/libsgmls-perl_$_debver-$_debrel.debian.tar.xz)
sha512sums=('0ca3f0f3265f20661ae88ad4764fbf82f226382f79f62c01653265cc7372524dfdf30ce5abffc0344624f5e6138e483952471f8c18d59794b40035af0b7526bc'
            '1cbaff68aaafbb057455cb032845afd73be35d789e4199d7cc7d34ea4604f00906b8adec0bfd3e139bc227c06cf047e1de9bdfc8cdfcbd28d08cd04b92c3136f')

prepare() {
	cd "$srcdir/$_cpanname-$pkgver"
	find . -type f -exec chmod 0644 {} \;
	if [[ $pkgver = $_debver ]]; then
		# Debian patches
		export QUILT_PATCHES=debian/patches
		export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
		export QUILT_DIFF_ARGS='--no-timestamps'

		mv "$srcdir"/debian .

		quilt push -av
	fi
}

prepareEnvironment() {
	cd "$srcdir/$_cpanname-$pkgver"
	export \
		PERL_MM_USE_DEFAULT=1 \
		PERL_AUTOINSTALL=--skipdeps \
		PERL_MM_OPT="INSTALLDIRS=vendor DESTDIR='"$pkgdir"'" \
		PERL_MB_OPT="--installdirs vendor --destdir '"$pkgdir"'" \
		MODULEBUILDRC=/dev/null
}

build() {
	prepareEnvironment
	/usr/bin/perl Makefile.PL
	make
}

check() {
	prepareEnvironment
	make test
}

package() {
	prepareEnvironment
	make install
	find "$pkgdir" -name .packlist -o -name perllocal.pod -delete

	# FS#51874
	ln -sv /usr/bin/vendor_perl/sgmlspl.pl $pkgdir/usr/bin/sgmlspl

	install -Dm644 COPYING $pkgdir/usr/share/licenses/$pkgname/COPYING
}
