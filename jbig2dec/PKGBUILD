# Maintainer (Arch): Gaetan Bisson <bisson@archlinux.org>
# Contributor (Arch): Bartłomiej Piotrowski
# Contributor (Arch): Stefan Husmann <stefan-husmann@t-online.de>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=jbig2dec
pkgver=0.19
_gsver=gs9530
_debver=$pkgver
_debrel=2
pkgrel=2
pkgdesc="Decoder implementation of the JBIG2 image compression format"
url='https://jbig2dec.com/'
arch=('i686' 'x86_64')
license=('AGPL-3')
depends=('libpng')
makedepends=('quilt')
source=("https://github.com/ArtifexSoftware/ghostpdl-downloads/releases/download/${_gsver}/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/j/jbig2dec/jbig2dec_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('0d3ec8123b446b5b74846b189004c188e82cdf7b1c7ec3e02f98892aaa249fba0b0fe7f1db7036f2571b0ef2a5df22d1997d3d3f5479c9bef7851bf114134737'
            '60cf1810796e025f652b8c600ac977b1a133121653c5c68463c5e3032938e4f0dfa2be0506ae3ba7ac8f3b15cbde9a1f37363064ecc8a530a4da4ee2549784fc')

prepare() {
	cd ${pkgname}-${pkgver}
	if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
		# Debian patches
		export QUILT_PATCHES=debian/patches
		export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
		export QUILT_DIFF_ARGS='--no-timestamps'

		mv "$srcdir"/debian .

		# Doesn't apply and seems unimportant
		rm -v debian/patches/2* || true

		quilt push -av
	fi
	autoreconf -vfi
}

build() {
	cd ${pkgname}-${pkgver}
	./configure --prefix=/usr
	make
}

package() {
	cd ${pkgname}-${pkgver}
	make DESTDIR="${pkgdir}" install
	for i in COPYING LICENSE; do
		install -Dm644 ${i} ${pkgdir}/usr/share/licenses/${pkgname}/${i}
	done
}
