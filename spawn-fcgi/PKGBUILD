# Maintainer (Arch): Lukas Fleischer <lfleischer@archlinux.org>
# Contributor (Arch): Aaron Bull Schaefer <aaron@elasticdog.com>
# Contributor (Arch): Felix Hanley <felix@seconddrawer.com.au>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=spawn-fcgi
pkgver=1.6.4
_debver=$pkgver
_debrel=2
pkgrel=1
pkgdesc="A FastCGI process manager for web applications"
arch=('i686' 'x86_64')
url='https://redmine.lighttpd.net/projects/spawn-fcgi/'
license=('Modified-BSD')
depends=('glibc')
makedepends=('quilt')
source=("https://www.lighttpd.net/download/${pkgname}-${pkgver}.tar.bz2"
        "https://deb.debian.org/debian/pool/main/s/spawn-fcgi/spawn-fcgi_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('defe3b0e21952f6a852237f4677b446757bd0dbccc6609beb2094c59b12de7814a76525554731ad3b52d10060bb0b4060b4f5c4fc3e14cd35f59c30cc583b6b9'
            'cf776cf702700b35e41d172428bfbc650ea3047483b80beb24d5875d3466da8a2eebf711a8421468637e233accb6a18d7a3d52fbd25d2af5bb9e7b1a8c9c9d1b')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  autoreconf -vfi
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  ./configure \
    --prefix=/usr
  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
