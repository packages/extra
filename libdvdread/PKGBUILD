# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Contributor (Arch): Thomas Baechler <thomas@archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=libdvdread
pkgver=6.1.1
_debver=6.1.1
_debrel=2
pkgrel=1
pkgdesc="Provides a simple foundation for reading DVD video disks"
arch=(i686 x86_64)
url="https://www.videolan.org/developers/libdvdnav.html"
license=('GPL-2')
depends=('glibc')
makedepends=('libdvdcss' 'quilt')
optdepends=('libdvdcss: for decoding encrypted DVDs')
source=("https://download.videolan.org/pub/videolan/$pkgname/$pkgver/$pkgname-$pkgver.tar.bz2"{,.asc}
        "https://deb.debian.org/debian/pool/main/libd/libdvdread/libdvdread_$_debver-$_debrel.debian.tar.xz")
sha512sums=('f02c2d33ec3e79a4efd0b4ed9172935b092c9f827eb6810ed354328c34b5206ee4ffd646239ce50b9401cab1758f1a746113e004461b20df4f44bc7fa6bc2278'
            'SKIP'
            'd0c16855ce737d83157762dfe07a404dd84b4db4834db51ed68b407966bf41d5002de7b58345272dd5736068f91d2084a2cefae004acf8a3d17c4d1b221b5ec7')
validpgpkeys=('65F7C6B4206BD057A7EB73787180713BE58D1ADC')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  autoreconf -fi
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING $pkgdir/usr/share/licenses/$pkgname/COPYING
}
