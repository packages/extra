# Maintainer (Arch): David Runge <dvzrv@archlinux.org>
# Contributor (Arch): Ray Rashif <schiv@archlinux.org>
# Contributor (Arch): speps <speps at aur dot archlinux dot org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=suil
pkgver=0.10.10
pkgrel=2
pkgdesc='A lightweight C library for loading and wrapping LV2 plugin UIs'
arch=('i686' 'x86_64')
url='https://drobilla.net/software/suil/'
license=('ISC')
depends=('glibc')
makedepends=('gtk2' 'gtk' 'lv2' 'python' 'qt-base' 'qt-x11extras' 'waf')
optdepends=('atk: support for wrapping X11 and Qt in GTK2/3 and GTK2 in Qt'
            'cairo: support for wrapping X11 and Qt in GTK2/3'
            'fontconfig: support for wrapping X11 and Qt in GTK2 and GTK2 in Qt'
            'gcc-libs: support for wrapping GTK2 and X11 in Qt and Qt in GTK2/3'
            'gdk-pixbuf2: support for wrapping X11 and Qt in GTK2/3 and GTK2 in Qt'
            'glib2: support for wrapping X11 and Qt in GTK2/3 and GTK2 in Qt'
            'gtk2: support for wrapping X11 and Qt in GTK2 and GTK2 in Qt'
            'gtk: support for wrapping X11 and Qt in GTK3'
            'libx11: support for wrapping X11 directly and in GTK2/3'
            'qt-base: support for wrapping X11 in Qt, Qt in GTK2/3 and GTK2 in Qt'
            'qt-x11extras: support for wrapping X11 in Qt'
            'pango: support for wrapping X11 in GTK2/3, Qt in GTK2/3 and GTK2 in Qt'
            'zlib: support for wrapping X11 in GTK3 and Qt in GTK3')
source=("https://download.drobilla.net/${pkgname}-${pkgver}.tar.bz2"{,.asc})
sha512sums=('e442022096150b2f71d65bbd625e2e262cb3e3865c57f5a30f0472dac8ef90ad2fbbf67235823055019017e3751b46330f6e98b8e1745e5845eb02c4d2a4fc21'
            'SKIP')
validpgpkeys=('907D226E7E13FA337F014A083672782A9BF368F3') # David Robillard <d@drobilla.net>

prepare() {
  cd "${pkgname}-${pkgver}"
  # remove local call to ldconfig
  sed -i "/ldconfig/d" wscript
  # let wscript(s) find the custom waf scripts
  mkdir -pv tools
  touch __init__.py
  cp -v waflib/extras/{autoship,autowaf,lv2}.py tools/
  mkdir -pv plugins/tools/
  cp -v waflib/extras/{autoship,autowaf,lv2}.py plugins/tools/
  rm -rv waflib
  sed -e 's/waflib.extras/tools/g' \
      -e "s/load('autowaf'/load('autowaf', tooldir='tools'/g" \
      -e "s/load('lv2'/load('lv2', tooldir='tools'/g" \
      -i wscript
}

build() {
  cd "${pkgname}-${pkgver}"
  waf configure --prefix=/usr
  waf build
}

package() {
  cd "${pkgname}-${pkgver}"
  waf install --destdir="${pkgdir}"
  # license
  install -vDm 644 COPYING \
    -t "${pkgdir}/usr/share/licenses/${pkgname}"
  # docs
  install -vDm 644 {AUTHORS,NEWS,PACKAGING,README.md} \
    -t "$pkgdir/usr/share/doc/${pkgname}"
}
