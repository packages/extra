# Maintainer (Arch): Thomas Bächler <thomas@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=v4l-utils
pkgver=1.20.0
_debver=1.20.0
_debrel=2
pkgrel=1
pkgdesc="Userspace tools and conversion library for V4L"
arch=('i686' 'x86_64')
url="https://linuxtv.org/"
provides=("libv4l=$pkgver")
replaces=('libv4l')
conflicts=('libv4l')
backup=(etc/rc_maps.cfg)
license=('GPL-2' 'LGPL-2.1')
makedepends=('qt-base' 'alsa-lib' 'quilt')
optdepends=('qt-base: for qv4l2' 'alsa-lib: for qv4l2')
depends=('hicolor-icon-theme' 'gcc-libs' 'sysfsutils' 'libjpeg-turbo')
source=(https://linuxtv.org/downloads/v4l-utils/${pkgname}-${pkgver}.tar.bz2{,.asc}
        https://deb.debian.org/debian/pool/main/v/v4l-utils/v4l-utils_$_debver-$_debrel.debian.tar.xz)
sha512sums=('179ca8dbbf7af5fa4870b70f17645d7834fe6ba52670ae6b58473efa257db0cd812ce14f16574cc4491c0bcb218835e4c29f1354882a151687eecec97852fc63'
            'SKIP'
            'f314cff50b7f2a9a28dd0f68f94afc403d5cf89f7d452af2dda47bdf294334006f9df6dccc8d1897635a0114ffcb5edddee5975e52412d9d2205e6956588321b')
validpgpkeys=('05D0169C26E41593418129DF199A64FADFB500FF') # Gregor Jasny <gjasny@googlemail.com>

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  ./configure --prefix=/usr --sysconfdir=/etc
  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  MAKEFLAGS="-j1" make install DESTDIR="${pkgdir}/"

  for i in COPYING{,.libdvbv5,.libv4l}; do
    install -Dm644 ${i} ${pkgdir}/usr/share/licenses/${pkgname}/${i}
  done

  rm "${pkgdir}/usr/bin/ivtv-ctl"
}
