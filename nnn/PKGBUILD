# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org
# Maintainer (Arch): Alexander Epaneshnikov <alex19ep@archlinux.org>
# Contributor (Arch): Maxim Baz <$pkgname at maximbaz dot com>
# Contributor (Arch): Pablo Arias <pabloariasal@gmail.com>
# Contributor (Arch): John Jenkins <twodopeshaggy@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=nnn
pkgver=4.7
pkgrel=1
pkgdesc="Fast terminal file manager"
url='https://github.com/jarun/nnn'
arch=('i686' 'x86_64')
license=('Expat' 'Simplified-BSD')
depends=('bash' 'sed')
optdepends=('libarchive: for more archive formats'
            'zip: for zip archive format'
            'unzip: for zip archive format'
            'trash-cli: to trash files'
            'sshfs: mount remotes'
            'fuse2: unmount remotes'
            'xdg-utils: desktop opener')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/jarun/nnn/archive/v${pkgver}.tar.gz")
sha512sums=('28ec9b9ab93f979d77e80957df2a70ec687e56ada6d5e8555c121f2454b5c049466120e5eb3570174411a61e3855c869f96d306a43516cceff8c4cb7f1075875')

prepare() {
  sed -i 's/install: all/install:/' "${pkgname}-${pkgver}/Makefile"
}

build() {
  cd "${pkgname}-${pkgver}"
  make
}

package() {
  cd "${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" PREFIX=/usr install
  make DESTDIR="${pkgdir}" PREFIX=/usr install-desktop

  install -Dm644 misc/auto-completion/fish/nnn.fish "${pkgdir}/usr/share/fish/vendor_completions.d/nnn.fish"
  install -Dm644 misc/auto-completion/bash/nnn-completion.bash "${pkgdir}/usr/share/bash-completion/completions/nnn"
  install -Dm644 misc/auto-completion/zsh/_nnn "${pkgdir}/usr/share/zsh/site-functions/_nnn"

  install -Dm644 -t "${pkgdir}/usr/share/nnn/quitcd/" misc/quitcd/*

  # handle plugins
  cp -a plugins "${pkgdir}/usr/share/nnn/plugins/"
  rm "${pkgdir}/usr/share/nnn/plugins/"{boom,getplugs,gsconnect,imgur,imgresize,kdeconnect,moclyrics,mtpmount}

  install -Dm644 -t "${pkgdir}/usr/share/licenses/${pkgname}/" LICENSE
}
