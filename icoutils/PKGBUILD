# Maintainer (Arch): Giancarlo Razzolini <grazzolini@archlinux.org>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Geoffroy Carrier <geoffroy.carrier@aur.archlinux.org>
# Contributor (Arch): neodreams <yanbrodeur@videotron.ca>
# Contributor (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=icoutils
pkgver=0.32.3
_debver=0.32.3
_debrel=3
pkgrel=1
pkgdesc='Extracts and converts images in MS Windows(R) icon and cursor files.'
arch=('i686' 'x86_64')
license=('GPL-3')
url='https://www.nongnu.org/icoutils/'
depends=('libpng>=1.0.0' 'perl-libwww>=5.64')
makedepends=('quilt')
source=("https://savannah.nongnu.org/download/${pkgname}/${pkgname}-${pkgver}.tar.bz2"
        "https://deb.debian.org/debian/pool/main/i/icoutils/icoutils_$_debver-$_debrel.debian.tar.xz")
sha512sums=('982a051a5dc4a63bb2a9f23e78e5a88e481e5c7a9c25789253e1c396e40d4c093e5a9b399966d660e4f2da21ce15d539cb9d20bfd8126b1138f148b86baa6726'
            'c17cafabfc281a4698e27ea58cbcc4e0de06ebf72b87fc6d474b279dbca11bf24eaef8a40095ada60dffc46372b05164be37a2145b6651551201a0ce3133543b')

prepare() {
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver

  ./configure \
    --prefix=/usr \
    --mandir=/usr/share/man
  make
}

package() {
  cd $pkgname-$pkgver

  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
