# Maintainer (Arch): David Runge <dvzrv@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad

_name=sphinx-argparse
pkgname=python-sphinx-argparse
pkgver=0.2.5
_debver=$pkgver
_debrel=1
pkgrel=2
pkgdesc="Sphinx extension that automatically documents argparse commands and options"
arch=('any')
url="https://github.com/ribozz/sphinx-argparse"
license=('Expat')
depends=('python-sphinx')
makedepends=('python-setuptools' 'quilt')
optdepends=('python-commonmark: markdown support')
source=("https://files.pythonhosted.org/packages/source/${_name::1}/${_name}/${_name}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/s/$_name/${_name}_$_debver-$_debrel.debian.tar.xz")
sha512sums=('785e2b1ea04b39721d28aee712b87f7fccb56a25a2d5ccf7825b27673a7e53069265c89b32f7b63d4f788127fe815595913af699dba3cd60db37a7efdef6ea55'
            '7a14aa96d7f1d333ca39e455e1255383e7b2675211787319d721850bce4bd9b436287e8dd9cb9ac337158c22ac2c9da620cfd0f775b5c0e56449252ad6a559f9')

prepare() {
  mv -v "${_name}-${pkgver}" "${pkgname}-${pkgver}"

  # fix problematic file permissions: https://github.com/ribozz/sphinx-argparse/issues/117
  cd "${pkgname}-${pkgver}"
  chmod 644 sphinx_argparse.egg-info/*

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/0003-Remove-usage-of-Markdown-options-in-docs-markdown.rs.patch || true

    quilt push -av
  fi
}

build() {
  cd "${pkgname}-${pkgver}"
  python setup.py build
}

package() {
  cd "${pkgname}-${pkgver}"
  python setup.py install --skip-build \
    --optimize=1 \
    --prefix=/usr \
    --root="${pkgdir}"
  install -vDm 644 README.md -t "${pkgdir}/usr/share/doc/${pkgname}"
  install -Dm644 debian/copyright -t $pkgdir/usr/share/licenses/$pkgname/
}
