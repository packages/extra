# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=wizznic
pkgver=0.9.9
_debver=$pkgver
_debrel=1
pkgrel=1
pkgdesc="Free and libre implementation of the arcade classic Puzznic"
arch=('i686' 'x86_64')
url='http://wizznic.org/'
license=('GPL-3')
depends=('glu' 'sdl_image' 'sdl_mixer' 'sdl' 'glibc' 'zlib')
makedepends=('quilt')
groups=('games')
source=("${pkgname}-${pkgver}.tar.xz::https://deb.debian.org/debian/pool/main/w/wizznic/wizznic_${pkgver}+dfsg.orig.tar.xz"
        "https://deb.debian.org/debian/pool/main/w/wizznic/wizznic_${_debver}+dfsg-${_debrel}.debian.tar.xz"
        "$pkgname.png")
sha512sums=('827a65ccd1827528b419af73912db89b701d9b96d97403f80651e774f39d3862fbc2ef56c257fc931e21000fde5d048e67a71d42333f2519673b040746b7f7d5'
            'd4cd9492e1b32ef486859946bc86609cfb46abaf6c4a9026aa221c653531a4233f5e35bceb2055ada2bc6eb9bf5bdb99f86f3a1922cf37ba7273f150176c25b4'
            'abaeeeffa527bf06ac79113100b5b63a633bfff17ebeecd548ddeff83d63b43bd4a73a338b3af9d35bc10538af26a9bce7d5032b5412277bbdddd5e470735b45')

prepare() {
  mv "$pkgname-$pkgver-src" "$pkgname-$pkgver"
  cd "$pkgname-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$pkgname-$pkgver"
  export CFLAGS="${CLAGS} -std=gnu89"
  make DATADIR=/usr/share/games/$pkgname/ BINDIR=/usr/games/
}

package() {
  cd "$pkgname-$pkgver"

  # create directories and install data
  install -d "$pkgdir/usr/games/"
  install -d "$pkgdir/usr/share/games/$pkgname/"
  make DATADIR="$pkgdir/usr/share/games/$pkgname/" BINDIR="$pkgdir/usr/games/" install

  # desktop-shortcut, icon and license
  install -Dm644 $srcdir/$pkgname.png -t "$pkgdir/usr/share/pixmaps"
  install -Dm644 debian/$pkgname.desktop -t "$pkgdir/usr/share/applications"
  install -Dm644 debian/copyright -t "$pkgdir/usr/share/licenses/$pkgname"  
}
