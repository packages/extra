# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

# Based on xf86-video-vmware package

pkgname=xenocara-video-vmware
_openbsdver=6.9
pkgver=13.1.0
pkgrel=2
pkgdesc="Xenocara VMware video driver"
arch=('i686' 'x86_64')
url="https://www.xenocara.org"
license=('X11')
depends=('mesa')
makedepends=('xenocara-server-devel' 'X-ABI-VIDEODRV_VERSION=24.0' 'xenocara-util-macros')
provides=('xf86-video-vmware')
conflicts=('xf86-video-vmware' 'xenocara-server<1.20' 'X-ABI-VIDEODRV_VERSION<24' 'X-ABI-VIDEODRV_VERSION>=25')
replaces=('xf86-video-vmware' 'xorg-video-vmware')
groups=('xenocara-video-drivers' 'xenocara-video' 'xenocara-drivers'
        'xorg-video-drivers' 'xorg-video' 'xorg-drivers')
options=('!emptydirs')
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/driver/xf86-video-vmware-$pkgver.tar.lz{,.sig}
        gcc8.patch
        0001-Adapt-Block-WakeupHandler-signature-for-ABI-23.patch
        0002-vmware-Fix-build-warnings.patch
        0003-Fix-a-number-of-compilation-warnings.patch
        0004-saa-Build-compatibility-with-xserver-1.20.patch
        0005-vmwgfx-Limit-the-number-of-cliprects-in-a-drm-dirtyf.patch)
sha512sums=('678e33dd28603d113a47085f01d08083978ee6fc63a9a5c20df5dd391698f93fc09665a36930b061c89f73d7e077c79488868caa9dd3e6d880ef5e56ea48e26a'
            'SKIP'
            '8de12ceb62039ffb6528b6e6a8a5072846e85921141bde03b21496badc5e193af3b609ad2f50a1d0942b558a07c212ad82144641d5a9e9ff21929b5e20b5ae25'
            'bdb055f17379bf736dfe81ef049b65ef44297487a508178b34ace9fdb6b81eb69dafa47207780a193ea9b92d1577fc331c8d587367a09aee5f9657b1d093ceb3'
            '0cb791f517952c8a3bf8ce5c3c9e742b06967342e9282d2ac052c11c91adabea24af21ee71c7f9007cb8d5ecd8d168384527b657cffd0dfcd031d7ff543c0eea'
            'd13c39250fc83abcd18b348d0a7a26effa86002bddef589665a0691123013142817731236fcae1b3a9b57a7d6ef3eb3942f1c3e6c85fd16dc0c772030fcc7c07'
            '4077de6e43852e30f3e120e0b279d252f8388747a9acdadc7f5346bfd49fd088bc9337c05bec37bd40ab1790bd7d5a435ba908f0ac844ca5aee430312edef7c3'
            '63d878af65f7edafee686d422e4463f839674d30284e2b8b6fc60668829f4653c78d33d981f64d93c80526d2de63c02d38109eeb930eeab58cdc16dcbaedafcb')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/driver/xf86-video-vmware"
  patch -p1 -i "$srcdir/gcc8.patch"
  patch -p1 -i "$srcdir/0001-Adapt-Block-WakeupHandler-signature-for-ABI-23.patch"
  patch -p1 -i "$srcdir/0002-vmware-Fix-build-warnings.patch"
  patch -p1 -i "$srcdir/0003-Fix-a-number-of-compilation-warnings.patch"
  patch -p1 -i "$srcdir/0004-saa-Build-compatibility-with-xserver-1.20.patch"
  patch -p1 -i "$srcdir/0005-vmwgfx-Limit-the-number-of-cliprects-in-a-drm-dirtyf.patch"
  autoreconf -vfi
}

build() {
  cd "xenocara-$_openbsdver/driver/xf86-video-vmware"

  # Since pacman 5.0.2-2, hardened flags are now enabled in makepkg.conf
  # With them, module fail to load with undefined symbol.
  # See https://bugs.archlinux.org/task/55102 / https://bugs.archlinux.org/task/54845
  export CFLAGS=${CFLAGS/-fno-plt}
  export CXXFLAGS=${CXXFLAGS/-fno-plt}
  export LDFLAGS=${LDFLAGS/,-z,now}

  ./configure --prefix=/usr --enable-vmwarectrl-client
  make
}

package() {
  cd "xenocara-$_openbsdver/driver/xf86-video-vmware"
  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
