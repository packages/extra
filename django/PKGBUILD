# Maintainer (Parabola): Luke Shumaker <lukeshu@sbcglobal.net>
# Maintainer (Arch): Dan McGee <dan@archlinux.org>
# Contributor (Arch): Shahar Weiss <sweiss4@gmx.net>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=django
pkgname=('python-django' 'tauthon-django')
pkgver=1.7.11
pkgrel=1
arch=('any')
license=('Modified-BSD')
url='http://www.djangoproject.com/'
makedepends=('tauthon' 'tauthon-setuptools' 'python' 'python-setuptools')
source=("https://www.djangoproject.com/m/releases/${pkgver:0:3}/Django-$pkgver.tar.gz")
sha512sums=('2e3415295ee6b590e366b09adb261baccb750519b9bcef1bb09bd52a2db705c8082bfc13440c621a5f41dd4f9bd626792eba4a572e78de3b7caf93c951c406ee')

prepare() {
  cd "$srcdir/Django-$pkgver"

  cp -a "$srcdir/Django-$pkgver" "$srcdir/Django-$pkgver-python2"

  find "$srcdir/Django-$pkgver-python2" -name '*.py' | \
    xargs sed -i "s|#!/usr/bin/env python$|#!/usr/bin/env tauthon|"
}

build() {
  cd "$srcdir/Django-$pkgver"
  python setup.py build

  cd "$srcdir/Django-$pkgver-python2"
  tauthon setup.py build
}

package_python-django() {
  pkgdesc="A high-level Python Web framework that encourages rapid development and clean design"
  depends=('python' 'python-setuptools')
  optdepends=('python-psycopg2: for PostgreSQL backend')
  cd "$srcdir/Django-$pkgver"
  python setup.py install --root="$pkgdir" --optimize=1

  ln -s django-admin.py "$pkgdir"/usr/bin/django-admin3.py
  ln -s django-admin "$pkgdir"/usr/bin/django-admin3
  install -Dm644 extras/django_bash_completion \
    "$pkgdir"/usr/share/bash-completion/completions/django-admin.py
  ln -s django-admin.py \
    "$pkgdir"/usr/share/bash-completion/completions/django-admin
  ln -s django-admin.py \
    "$pkgdir"/usr/share/bash-completion/completions/manage.py

  install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

package_tauthon-django() {
  pkgdesc="A high-level Tauthon Web framework that encourages rapid development and clean design"
  depends=('tauthon' 'tauthon-setuptools')
  optdepends=('tauthon-psycopg2: for PostgreSQL backend')
  cd "$srcdir/Django-$pkgver-python2"
  tauthon setup.py install --root="$pkgdir" --optimize=1

  mv "$pkgdir"/usr/bin/django-admin.py "$pkgdir"/usr/bin/django-admin2.py
  mv "$pkgdir"/usr/bin/django-admin "$pkgdir"/usr/bin/django-admin2
  # TODO: this probably won't work due to the `complete` command within not
  # knowing about modified our exectuable names
  install -Dm644 extras/django_bash_completion \
    "$pkgdir"/usr/share/bash-completion/completions/django-admin2.py
  ln -s django-admin2.py \
    "$pkgdir"/usr/share/bash-completion/completions/django-admin2

  install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}