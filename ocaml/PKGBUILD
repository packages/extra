# Maintainer (Arch): Jürgen Hötzel <juergen@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=ocaml
pkgname=('ocaml' 'ocaml-compiler-libs')
pkgver=4.11.1
_debver=$pkgver
_debrel=4
pkgrel=2
pkgdesc="A functional language with OO extensions"
arch=('i686' 'x86_64')
license=('custom:LGPL-2.1+OCaml-Special-Exception')
url="https://caml.inria.fr/"
makedepends=('ncurses')
optdepends=('ncurses: advanced ncurses features'
            'tk: advanced tk features')
source=(https://caml.inria.fr/distrib/ocaml-${pkgver%.*}/${pkgname}-${pkgver}.tar.xz
        https://deb.debian.org/debian/pool/main/o/ocaml/ocaml_$_debver-$_debrel.debian.tar.xz)
sha512sums=('ca34655dc22257aa48aeb48a9cf56bde177aa0f0e7f3965f334939934801a206ed5667b7ab0cc291df9e6bf1bb3251f7384f078ea5562e647fde93167ecb1e84'
            '2f1a0422bc135d72a75b4830ccc0cd34736c1375c7d7d1c7375347bde3684cf2735082e364e5c02117c489296868337bc2af43e2dc849afb1eced34048be0a89')
options=('!makeflags' '!emptydirs' 'staticlibs')

prepare() {
  cd "${srcdir}/${pkgbase}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/0001-Don-t-use-rpath.patch || true
    rm -v debian/patches/0003-Use-CCLINKFLAGS-for-linking-all-executables-and-shar.patch || true
    rm -v debian/patches/0008-Statically-link-lib-bfd-iberty.patch || true

    quilt push -av
  fi
}

build() {
  cd "${srcdir}/${pkgbase}-${pkgver}"
  ./configure --prefix /usr --mandir /usr/share/man --disable-force-safe-string
  make --debug=v world.opt
}

package_ocaml() {
  cd "${srcdir}/${pkgbase}-${pkgver}"
  make DESTDIR="${pkgdir}" install

  # install license
  install -m755 -d "${pkgdir}/usr/share/licenses/${pkgname}"
  install -m644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/"

  # remove compiler libs
  rm -rf "${pkgdir}/usr/lib/ocaml/compiler-libs"
}

package_ocaml-compiler-libs() {
  pkgdesc="Several modules used internally by the OCaml compiler"
  depends=('ocaml')

  cd "${srcdir}/${pkgbase}-${pkgver}"
  make DESTDIR="${pkgdir}" install
  # Remove non-compiler-libs
  rm -rf   "${pkgdir}/usr/bin"  "${pkgdir}/usr/lib/ocaml/caml" \
     "${pkgdir}/usr/lib/ocaml/ocamldoc" "${pkgdir}/usr/lib/ocaml/stublibs" \
     "${pkgdir}/usr/lib/ocaml/threads" "${pkgdir}/usr/share"
  find "${pkgdir}/usr/lib/ocaml/" -maxdepth 1 -type f -delete

  install -m755 -d "${pkgdir}/usr/share/licenses/${pkgname}"
  install -m644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/"
}
