# Maintainer (Arch): Alad Wenter <alad@mailbox.org>
# Contributor (Arch): carstene1ns <arch carsten-teibes de>
# Contributor (Arch): Sam S. <smls75@gmail.com>
# Contributor (Arch): Daniel Scharrer <daniel@constexpr.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=innoextract
pkgver=1.8
_debver=1.8
_debrel=1.2
pkgrel=1
pkgdesc="A tool to extract installers created by Inno Setup"
url='https://constexpr.org/innoextract/'
arch=('i686' 'x86_64')
license=('zlib')
depends=('boost-libs' 'xz')
makedepends=('boost' 'cmake' 'quilt')
source=("https://constexpr.org/innoextract/files/$pkgname-$pkgver.tar.gz"{,.sig}
        "https://deb.debian.org/debian/pool/main/i/innoextract/innoextract_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('2c68009333f02a8a677c084e9c95c835d84a73e60c9b0c70fee5f23fd9a7a640cf2aa7e0476c55579774ac7079498fa24668f9388493bbc13415ff5a5b06ac9c'
            'SKIP'
            'b17c990948d8722da37386cae467372fadc39c685bb21e87e1e6b22779eb93ea04679e23b826e3ac78e7e60be239780d2eab44b3812434d3ab3368fb8c2a0747')
validpgpkeys=("ADE9653703D4ADE0E997758128555A66D7E1DEC9") # Daniel Scharrer <daniel@constexpr.org>

prepare() {
  cd "$pkgname-$pkgver"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$pkgname-$pkgver"

  # https://github.com/dscharrer/innoextract/issues/88
  cmake . -DCMAKE_INSTALL_PREFIX=/usr -DBoost_NO_BOOST_CMAKE=ON
  make
}

package() {
  cd "$pkgname-$pkgver"
  make DESTDIR="$pkgdir/" install

  install -Dm644 README.md "$pkgdir/usr/share/doc/$pkgname/README.md"
  install -m644  CHANGELOG "$pkgdir/usr/share/doc/$pkgname/CHANGELOG"
  install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}
