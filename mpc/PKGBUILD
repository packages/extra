# Maintainer (Arch): David Runge <dvzrv@archlinux.org>
# Contributor (Arch): Gaetan Bisson <bisson@archlinux.org>
# Contributor (Arch): Angel Velasquez <angvp@archlinux.org>
# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Contributor (Arch): Alexander Fehr <pizzapunk gmail com>
# Contributor (Arch): Link Dupont <link@subpop.net>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=mpc
pkgver=0.34
_debver=$pkgver
_debrel=1
pkgrel=1
pkgdesc="Minimalist command line interface to MPD"
url='https://www.musicpd.org/clients/mpc/'
license=('GPL-2')
arch=('i686' 'x86_64')
depends=('glibc')
makedepends=('libmpdclient' 'meson' 'python-sphinx' 'quilt')
options=('!emptydirs')
source=("https://www.musicpd.org/download/${pkgname}/${pkgver%.*}/${pkgname}-${pkgver}.tar.xz"{,.sig}
        "https://deb.debian.org/debian/pool/main/m/mpc/mpc_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('d43d5547134ffb24f2a2972882f6e264c1011a94d1033369d329487f59414d32d9842835afdd05da96fd8ed28f823d2ae2f46d8d8d24b68f3695badca2a9bbb2'
            'SKIP'
            '24ff1443685832b46e8e796b83bf1494260985f621b1e0989e0159c81fa2836263a1057676c362ec33a5be25b8ce5c150c7c1ca0237b7567061d3e0b2d2d182e')
validpgpkeys=('0392335A78083894A4301C43236E8A58C6DB4512') # Max Kellermann <max@blarg.de>

prepare() {
  cd "${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "${pkgname}-${pkgver}"
  meson --prefix /usr \
        --buildtype plain \
        --auto-features enabled \
        --wrap-mode nodownload \
        -D b_lto=true \
        -D b_pie=true \
        build
  ninja -C build
}

package() {
  depends+=('libmpdclient')

  cd "${pkgname}-${pkgver}"
  DESTDIR="${pkgdir}" ninja -C build install
  install -vDm 644 contrib/mpc-completion.bash "${pkgdir}/usr/share/bash-completion/completions/mpc"

  # the html documentation is not reproducible and only the man page is needed
  rm -rvf "${pkgdir}/usr/share/doc/${pkgname}/html"

  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
