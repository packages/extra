# Maintainer (Arch): Shinlun Hsieh <yngwiexx@yahoo.com.tw>
# Contributor (Arch): Cory Farmer <rbgkofi@ameritech.net>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=vorbisgain
pkgver=0.37
_debver=$pkgver
_debrel=2
pkgrel=1
pkgdesc="Free and libre utility that computes the ReplayGain values for Ogg Vorbis files."
arch=('i686' 'x86_64')
url='https://sjeng.org/vorbisgain.html'
license=('LGPL-2.1')
depends=('libvorbis')
makedepends=('quilt')
source=("https://sjeng.org/ftp/vorbis/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/v/vorbisgain/vorbisgain_${_debver}-${_debrel}.debian.tar.gz")
sha512sums=('67181834e1a0d7c6e1ccd984e2d30ebf1dcdff84caa77f75e5e771de8414904810e966a43852b7184e075d2428fd8b431124835efc370ce6504b8b2756746e04'
            '0d699affccfe11d58be1a363ac61be229580bfdb91c0021f5c8c1893bea3d8c3cfddf6bb7f3849f87d33ed410c79c3057480b54534373488f5fa852997568450')

prepare() {
  cd ${pkgname}-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}


build() {
  cd ${pkgname}-${pkgver}

  ./configure \
    --prefix=/usr \
    --enable-recursive \
    --mandir=/usr/share/man
  make
}

package() {
  cd ${pkgname}-${pkgver}

  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
