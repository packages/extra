# Maintainer (Arch): Balló György <ballogyor+arch at gmail dot com>
# Contributor (Arch): Alexander F Rødseth <xyproto@archlinux.org>
# Contributor (Arch): Bartłomiej Piotrowski <nospam@bpiotrowski.pl>
# Contributor (Arch): Robert Hollencamp <rhollencamp@gmail.com>
# Contributor (Arch): Daniel J Griffiths <griffithsdj@inbox.com>
# Contributor (Arch): Stefan Husmann <stefan-husmann@t-online.de>
# Contributor (Arch): Roman Mikhayloff <rimf@inbox.ru>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=codeblocks
pkgver=17.12
pkgrel=2
pkgdesc="Cross-platform C/C++ IDE"
arch=('i686' 'x86_64')
url='http://codeblocks.org/'
license=('GPL-3')
depends=('boost-libs' 'wxgtk3' 'hunspell')
makedepends=('boost' 'graphicsmagick-imagemagick-compat' 'zip')
source=("https://downloads.sourceforge.net/codeblocks/${pkgname}_$pkgver.tar.xz"
        "fix-crash.patch")
sha512sums=('f90e756ca6f532656eeed34c6259c6a96ae85d172fc11e39f696ded9189cab7171d0cead18524fbab50dd782f21adbc7d52e2ea90535a3ed0aef5946b8a476e1'
            '6417d54129e57c27d04982be808573c9e6cc30afa41a78549c7d99bbf36803ed06ca3169613f860ed91155adc1d3be2700207dd3bdaf8cdaeb0ed7da37bba161')

prepare() {
  cd $pkgname-$pkgver
  convert src/mime/codeblocks.png +set date:create +set date:modify -background none -extent 64x64 src/mime/codeblocks.png
  sed -i 's|$(datadir)/pixmaps|$(datadir)/icons/hicolor/64x64/apps|' src/mime/Makefile.{am,in}
  sed -i 's|$(datarootdir)/appdata|$(datarootdir)/metainfo|' Makefile.{am,in} src/plugins/contrib/appdata/Makefile.{am,in}

  # wx3-gtk3: Fix crash when doing Control-A, Control-V
  # https://sourceforge.net/p/codeblocks/code/11320/
  patch -Np2 -i $srcdir/fix-crash.patch
}

build() {
  cd $pkgname-$pkgver
  ./configure \
    --prefix=/usr \
    --with-contrib-plugins=all,-FileManager,-NassiShneiderman \
    --with-wx-config=/usr/bin/wx-config-gtk3
  sed -i 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}
