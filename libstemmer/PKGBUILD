# Maintainer (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgbase=snowball
pkgname=(snowball libstemmer)
pkgver=2.1.0
_debver=2.1.0
_debrel=1
pkgrel=1
pkgdesc='String processing language for creating stemming algorithms'
arch=('i686' 'x86_64')
url="https://snowballstem.org/"
license=('Modified-BSD')
depends=('glibc')
makedepends=('quilt')
source=("https://github.com/snowballstem/snowball/archive/refs/tags/v${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/s/snowball/snowball_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('1efd7d8ab58852987e83247048244882c517e32237c8cb3c0558b66ecfb075733ce8805ebb76041e6e7d6664c236054effe66838e7c524ee529ce869aa8134f0'
            '12980a38c302e8d8dedb70bbd91ebc2c7f50e5352f38c9d49796532d5fc8b13a24217963b2e53c7b2a7cd06803d39d4925acc16bbdbb587075c3657f7ac6dc81')

prepare() {
  cd $pkgbase-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgbase-$pkgver

  make
}

package_snowball() {
  cd $pkgbase-$pkgver

  install -d "$pkgdir/usr/bin"
  install -t "$pkgdir/usr/bin" snowball stemwords
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/snowball"
}

package_libstemmer() {
  pkgdesc='Stemming library supporting several languages'
  cd $pkgbase-$pkgver

  mv libstemmer.so.0d.0.0 libstemmer.so.0.0.0
  install -Dm644 {.,"$pkgdir"/usr}/include/libstemmer.h
  install -D {.,"$pkgdir"/usr/lib}/libstemmer.so.0.0.0
  ln -s libstemmer.so.0.0.0 "$pkgdir/usr/lib/libstemmer.so.0"
  ln -s libstemmer.so.0 "$pkgdir/usr/lib/libstemmer.so"
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/libstemmer"
}

