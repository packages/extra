# Maintainer (Arch): Anatol Pomozov <anatol.pomozov@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

_gemname=nokogiri
pkgname=ruby-$_gemname
pkgver=1.13.10
_debver=$pkgver
_debrel=2
pkgrel=1
pkgdesc="Nokogiri is an HTML, XML, SAX and Reader parser"
arch=('i686' 'x86_64')
url='https://nokogiri.org'
license=('Expat')
depends=('ruby' 'ruby-mini_portile2' 'libxslt')
makedepends=('quilt')
options=(!emptydirs)
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/sparklemotion/${_gemname}/archive/refs/tags/v${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/r/$pkgname/${pkgname}_${_debver}+dfsg-${_debrel}.debian.tar.xz")
sha512sums=('c9a4b14cb92c4920872048991c958036d5f09b8e231a6f6408e5d3c5df78b70e206cc12b8ed8aaae012e51f6f87adcbe46273cd1feca082ec17f42114f5a1172'
            '53626f6da49155ee3d08a4b883d38f86cf786b0aeaf98eb04d17844294241907b50bf7023aa4e0be1335639f117c118020c197b7ea50fe05e77f58f72a0ffdfd')

prepare() {
  cd $_gemname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build () {
  cd $_gemname-$pkgver
  gem build
}

package() {
  cd $_gemname-$pkgver
  local _gemdir="$(ruby -e'puts Gem.default_dir')"
  gem install --ignore-dependencies --no-user-install -i "$pkgdir/$_gemdir" -n "$pkgdir/usr/bin" $_gemname-$pkgver.gem -- --use-system-libraries
  sed -r 's|~>|>=|g' -i "$pkgdir/$_gemdir/specifications/$_gemname-$pkgver.gemspec"
  rm "$pkgdir/$_gemdir/cache/$_gemname-$pkgver.gem"
  rm "$pkgdir/$_gemdir/gems/$_gemname-$pkgver/ext/nokogiri"/{*.o,*.c,*.h}
  install -D -m644 "$pkgdir/$_gemdir/gems/$_gemname-$pkgver/LICENSE-DEPENDENCIES.md" -t "$pkgdir/usr/share/licenses/$pkgname"
  install -D -m644 "$pkgdir/$_gemdir/gems/$_gemname-$pkgver/LICENSE.md" -t "$pkgdir/usr/share/licenses/$pkgname"
}