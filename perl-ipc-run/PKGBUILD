# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Maintainer (Arch): Jan-Erik Rediger <badboy at archlinux dot us>
# Contributor (Arch): Gerardo Exequiel Pozzi <vmlinuz386@yahoo.com.ar>
# Contributor (Arch): Francois Charette <firmicus@gmx.net>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=perl-ipc-run
pkgver=20200505.0
_debver=$pkgver
_debrel=1
pkgrel=1
pkgdesc="IPC::Run - system() and background procs w/ piping, redirs, ptys"
arch=('any')
url='https://search.cpan.org/dist/IPC-Run'
license=('GPL-2')
# IPC::Run depends on IO::Pty which is provided by perl-io-tty
depends=('perl' 'perl-io-tty')
makedepends=('quilt')
options=('!emptydirs')
source=("https://search.cpan.org/CPAN/authors/id/T/TO/TODDR/IPC-Run-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/libi/libipc-run-perl/libipc-run-perl_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('6f2e0cdbd66741a5185aa6f79c5d23189f6527e319ead385a033e482abed439a7e7fb1bbb7fa83c91c7b2971b31313a99b2b6510cc6aad3516ae6524a196d57c'
            '8958d2e36d41c419ae978e9cb5429855f370bbed8de8d840940b29fa86c34b8fd7a64904e68700f8e9dff92ef422a05a0223620aec8e62bf94474e5f598acc21')

prepare() {
  cd "$srcdir"/IPC-Run-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$srcdir"/IPC-Run-$pkgver
  PERL_MM_USE_DEFAULT=1 perl Makefile.PL INSTALLDIRS=vendor
  make
}

package() {
  cd "$srcdir"/IPC-Run-$pkgver
  make install DESTDIR="$pkgdir"
  find "$pkgdir" -name perllocal.pod -delete
  find "$pkgdir" -name .packlist -delete
  install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}
