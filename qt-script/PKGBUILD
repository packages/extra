# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=qt-script
_pkgbasename=qtscript
_qtver=5.15.2
pkgver=${_qtver/-/}
_debver=5.15.2+dfsg
_debrel=2
pkgrel=2
arch=('i686' 'x86_64')
url='https://www.qt.io'
license=('LGPL-3')
pkgdesc='Classes for making Qt applications scriptable. Provided for Qt 4.x compatibility'
depends=('qt-base')
makedepends=('quilt')
groups=('qt')
_pkgfqn="${_pkgbasename/5-/}-everywhere-src-${_qtver}"
source=("https://download.qt.io/official_releases/qt/${pkgver%.*}/${_qtver}/submodules/${_pkgfqn}.tar.xz"
        "https://deb.debian.org/debian/pool/main/q/qtscript-opensource-src/qtscript-opensource-src_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('71c70b34f7d4a0742df64d20d7e9a685cc640b9cc6a3d22847c04f45a437b3f5537f40225a522ed82787c2744d9a4949dea5b43c2ee81961d5ed175cf10deb32'
            'f18416119bb7a677537906827d36870c69a380e9dcbcadf079bc806d569c43a74e4f8b3806013338c15774e3bf989532d32383e4abac3bbb77da5880ea088f9d')

prepare() {
  cd ${_pkgfqn}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  mkdir -p ../build
}

build() {
  cd build

  qmake ../${_pkgfqn}
  make
}

package() {
  cd build
  make INSTALL_ROOT="$pkgdir" install

  # Drop QMAKE_PRL_BUILD_DIR because reference the build dir
  find "$pkgdir/usr/lib" -type f -name '*.prl' \
    -exec sed -i -e '/^QMAKE_PRL_BUILD_DIR/d' {} \;

  # Install license
  install -D -m644 "${srcdir}"/${_pkgfqn}/LICENSE.LGPL3 "${pkgdir}"/usr/share/licenses/${pkgname}/LICENSE.LGPL3
}
