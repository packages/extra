# Maintainer (Arch): Jelle van der Waa <jelle@vdwaa.nl>
# Maintainer (Arch): Eli Schwartz <eschwartz@archlinux.org>
# Contributor (Arch): lilydjwg <lilydjwg@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=python-dateutil
pkgver=2.8.1
_debver=2.8.1
_debrel=6
pkgrel=1
pkgdesc='Provides powerful extensions to the standard datetime module'
arch=('any')
license=('Apache-2.0' 'Modified-BSD')
url='https://github.com/dateutil/dateutil'
depends=('python-six')
makedepends=('python-setuptools-scm' 'python-six' 'quilt')
source=("https://files.pythonhosted.org/packages/source/${pkgname:0:1}/$pkgname/$pkgname-$pkgver.tar.gz"{,.asc}
        "https://deb.debian.org/debian/pool/main/p/python-dateutil/python-dateutil_$_debver-$_debrel.debian.tar.xz")
sha512sums=('337000216e0f8ce32d6363768444144183ab9268f69082f20858f2b3322b1c449e53b2f2b5dcb3645be22294659ce7838f74ace2fd7a7c4f2adc6cf806a9fa2c'
            'SKIP'
            'f376c089d6f43f21d4cf602e251ba4537a23fb995d47b200b00158ff4c2396230fc36983e8bde1a2c5af97c1f69e769252467273a6552506832ccfcac80ffb55')
validpgpkeys=('6B49ACBADCF6BD1CA20667ABCD54FCE3D964BEFB') # Paul Ganssle <paul@ganssle.io>

prepare() {
  cd "$srcdir"/python-dateutil-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$srcdir"/python-dateutil-$pkgver
  export PYTHONHASHSEED=0
  python setup.py build
}

package() {
  cd "$srcdir"/python-dateutil-$pkgver
  python setup.py install --root="$pkgdir" --optimize=1 --skip-build
  install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}
