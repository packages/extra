# Maintainer (Arch): Jaroslav Lichtblau <svetlemodry@archlinux.org>
# Contributor (Arch): Geoffroy Carrier <geoffroy.carrier@aur.archlinux.org>
# Contributor (Arch): Marco Bartholomew <marco@marcobartholomew.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=monit
pkgver=5.32.0
_debver=$pkgver
_debrel=1
pkgrel=2
pkgdesc="Utility for managing and monitoring processes, files, directories and devices on a UNIX system"
arch=('i686' 'x86_64')
url='https://mmonit.com/monit/'
license=('custom:AGPL-3+OpenSSL-Linking-Exception')
depends=('libressl')
makedepends=('quilt')
backup=('etc/monit/monitrc')
source=("https://mmonit.com/$pkgname/dist/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/m/monit/monit_${_debver}-${_debrel}~bpo11+1.debian.tar.xz"
        "fix-invalid-use-of-vfork.patch"
        "fix-monitrc.patch"
        "fix-webinterface.patch"
        "$pkgname.initd"
        "$pkgname.run")
sha512sums=('dcb56bee23c5fef4abe08a2b1633e58b60c8863b335d651d26e87f5306b4f98bd4887d2bf6311c2fc11c9b99eb853226d836b9d212ab65c0f150872c7297e005'
            '83b2fbb0b3b741454fdadd02c1eb6ff162e7a1c1c989dfb6316bcfe33f55b43d58570617f542bed329dd32936271098fabfaa7411e4306814f04a875d7acf4b5'
            '549fda6dc92c2630152a29f5bb94d29a59abb06300e55eb93ed729758d73ed7dceaa2e9f491c9d33419920aef24c0360f865753be28af13a11216129529611ae'
            '89ea3b9405f9cbd1f6b28e2d97bc187ad32c8a8cd16d13ccfa6ecb3700272c936c5289bcba08e2b88035fe621c2b30e94cf8dfff1af94b13f8dbdf944790760f'
            'e6537bb48b8ec2e52203727c7483a020c1940ee649ac5a187914da1b72c174b01b8751565f22dbc47fe8f4c068d4c795d14af3056f1e7ff221a460d4cb33a9bd'
            'cd44fad84f65071e9f5f1d40c0169bae6fbe245aa106bbfe519f200971e2e7e2b868046ad2e603024d3ed264e45e98d93172f58c390aed883a729984458eb898'
            '5086acb1d1171540941219da87af2cd6072281111cddc4077b4ba1a33ee83e0e161adb9b1cc426257ab647fa32580da26b41a3e33eface8e93415410031156aa')

prepare() {
  cd "${srcdir}"/$pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/010_monitrc.patch || true
    rm -v debian/patches/020_enable-hurd.patch || true

    quilt push -av
  fi
  patch -Np1 -i ${srcdir}/fix-invalid-use-of-vfork.patch
  patch -Np1 -i ${srcdir}/fix-monitrc.patch
  patch -Np1 -i ${srcdir}/fix-webinterface.patch
}

build() {
  cd "${srcdir}"/$pkgname-$pkgver

  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --without-pam
  make
}

package() {
  cd "${srcdir}"/$pkgname-$pkgver

  make DESTDIR="${pkgdir}" install

  # initial configuration and data
  install -Dm700 monitrc "${pkgdir}"/etc/$pkgname/monitrc
  install -d "${pkgdir}"/var/lib/$pkgname

  # services
  install -Dm755 "${srcdir}"/$pkgname.initd "${pkgdir}"/etc/init.d/$pkgname
  install -Dm755 "${srcdir}"/$pkgname.run "${pkgdir}"/etc/sv/$pkgname/run

  # license
  install -Dm644 COPYING -t "${pkgdir}"/usr/share/licenses/$pkgname
}
