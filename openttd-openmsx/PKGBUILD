# Contributor (Arch): BluePeril <blueperil (at) blueperil _dot_ de>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=openttd-openmsx
pkgver=0.4.2
_debver=$pkgver
_debrel=1
pkgrel=3
pkgdesc="A free and libre music set for OpenTTD"
arch=('any')
url='https://github.com/OpenTTD/OpenMSX'
license=('GPL-2')
depends=('timidity++' 'freepats')
makedepends=('quilt' 'python')
groups=('games')
source=("${pkgname}-${pkgver}.tar.xz::https://deb.debian.org/debian/pool/main/o/openttd-openmsx/openttd-openmsx_${pkgver}.orig.tar.xz"
        "https://deb.debian.org/debian/pool/main/o/openttd-openmsx/openttd-openmsx_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('65a06792032d0a7f6db5c9495bf5b382fdc29ca84db0535b61ea95b57e8ef961820e75dad2f8dc74de0608502839bea7af28fd342756ebfcc12ee8ac1f5553ab'
            '8c6a9ece0bbc05c0d1b727b9d81d397e97bb30ec8ff5b730ef831b5d2a7d37d8254e5568303ee18ebb359489f5cacdc39ad35653b51fc291f94ebf684eab156d')

prepare() {
  mv "openmsx-$pkgver-source" "$pkgname-$pkgver"
  cd "$pkgname-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$pkgname-$pkgver"
  make INSTALL_DIR="$pkgdir/usr/share/games/openttd/gm"
}

package() {
  cd "$pkgname-$pkgver"
  make DESTDIR="$pkgdir" INSTALL_DIR="$pkgdir/usr/share/games/openttd/gm" install

  # cleaning structure
  mv "$pkgdir/usr/share/games/openttd/gm/openmsx-$pkgver/"* "$pkgdir/usr/share/games/openttd/gm"
  rm -rf "$pkgdir/usr/share/games/openttd/gm/openmsx-$pkgver/"
  rm "$pkgdir/usr/share/games/openttd/gm/"{changelog,license,readme}.txt

  install -Dm644 debian/copyright -t "$pkgdir/usr/share/licenses/$pkgname"
}