# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): lilydjwg <lilydjwg@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=python-cffi
pkgname=(python-cffi tauthon-cffi)
pkgver=1.14.3
pkgrel=2
arch=('i686' 'x86_64')
url="https://cffi.readthedocs.org/"
license=('Expat')
makedepends=('python-setuptools' 'tauthon-setuptools' 'python-pycparser' 'tauthon-pycparser')
source=("${pkgname}-${pkgver}.tar.bz2::https://foss.heptapod.net/pypy/cffi/-/archive/v$pkgver/cffi-v${pkgver}.tar.bz2")
sha512sums=('987fb818e945d77f03921a60b71ebb0cf93989a6f322be39f710f32f5faaf5406f70c203a004527bbf49e26220cfacaedc2ad4949f08b8218a1a90dcc6c73b17')

prepare() {
  mv cffi-v$pkgver cffi-$pkgver
  cp -a cffi-$pkgver{,-tauthon}
}

build() {
  cd "$srcdir"/cffi-$pkgver
  python setup.py build

  cd "$srcdir"/cffi-$pkgver-tauthon
  tauthon setup.py build
}

package_python-cffi() {
  pkgdesc="Foreign Function Interface for Python calling C code"
  depends=('python-pycparser')

  cd cffi-$pkgver
  python setup.py install --root="$pkgdir" --optimize=1
  install -Dm644 LICENSE -t "$pkgdir"/usr/share/licenses/$pkgname
}

package_tauthon-cffi() {
  pkgdesc="Foreign Function Interface for Tauthon calling C code"
  depends=('tauthon-pycparser')

  cd cffi-$pkgver-tauthon
  tauthon setup.py install --root="$pkgdir" --optimize=1
  install -Dm644 LICENSE -t "$pkgdir"/usr/share/licenses/$pkgname
}
