# Maintainer (Arch): Jonathan Steel <jsteel at archlinux.org>
# Contributor (Arch): Ido Rosen <ido@kernel.org>
# Contributor (Arch): Army
# Contributor (Arch): Jay Tanzman <jay at jt512 dot dynddns dot ort>
# Contributor (Arch): Ray Kohler <ataraxia937 at gmail dot com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=sc
pkgver=7.16
_debver=7.16
_debrel=4
pkgrel=1
pkgdesc='Ncurses-based spreadsheet program'
arch=('i686' 'x86_64')
url='https://ibiblio.org/pub/linux/apps/financial/spreadsheet/!INDEX.html'
license=('Public-Domain')
depends=('ncurses')
source=(https://ibiblio.org/pub/linux/apps/financial/spreadsheet/$pkgname-$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/s/sc/sc_$_debver-$_debrel.debian.tar.xz
        LICENSE)
sha512sums=('efa495ba21d01b578a53f09101ccc28b564437317e0860904ad59136a24df9619dfc8715339296ae1567eaa8d620cdabc9bc2784962bc1c93e3bbd086856407b'
            'b0b2ae41bb8f86be8c13f80dd37d223185fbb8984c9aca85ba24cee30ebc1ece114f50f0a4315748a96d9b9b91a5beeea667997c012090abd609cd82e7f50361'
            'bbdb7d33e444e1f3e34d5f1c73414da3a492fee797eae80e8868d82faed02e42198e1dcea807ed1c279b47bb5a6efd2cf2ce4f4c67254eb7a7dde782d3d51d11')

prepare() {
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}


build() {
  cd $pkgname-$pkgver

  make
  make sc.1 psc.1
}

package() {
  cd $pkgname-$pkgver

  install -d "$pkgdir"/usr/bin "$pkgdir"/usr/share/doc/$pkgname \
    "$pkgdir"/usr/share/man/man1 "$pkgdir"/usr/share/licenses/$pkgname

  make prefix="$pkgdir"/usr MANDIR="$pkgdir"/usr/share/man/man1 install

  # license
  install -Dm644 "$srcdir/LICENSE" -t "$pkgdir/usr/share/licenses/$pkgname"
}
