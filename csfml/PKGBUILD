# Maintainer (Arch): Sven-Hendrik Haase <svenstaro@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=csfml
pkgver=2.5
_debver=2.5
_debrel=1.1
pkgrel=1
pkgdesc='C bindings for sfml'
arch=('i686' 'x86_64')
url='http://www.sfml-dev.org/'
license=('zlib')
depends=('sfml')
makedepends=('cmake' 'doxygen' 'ninja' 'quilt')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/SFML/CSFML/archive/${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/libc/libcsfml/libcsfml_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('186ed87d8b925cfd51cc4aa0ba440407d18adc875da760576b5fafdf3e8c7e01ca0f594d9950495eb3e607c65bff12341a81df24af81043dce89d25d7cc24626'
            '806def1d069feae93d2613f9e4f13ed5e45925657ed0867b5f91fb323be829d3460daf02a047488c542cd9708031b6d5ecd6e1fb2bec8ca39bd858abfe8bf9f6')

prepare() {
  cd CSFML-*

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd CSFML-*

  cmake . \
      -Bbuild \
      -GNinja \
      -DCMAKE_INSTALL_PREFIX=/usr \
      -DCSFML_BUILD_DOC=true
  ninja -C build
  ninja -C build doc
}

package() {
  cd CSFML-*

  DESTDIR="$pkgdir/" ninja -C build install

  install -Dm644 license.txt -t "${pkgdir}/usr/share/licenses/${pkgname}"
}

