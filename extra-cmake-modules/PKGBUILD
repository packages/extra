# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Maintainer (Arch): Antonio Rojas <arojas@archlinux.org>
# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad

pkgname=extra-cmake-modules
pkgver=5.82.0
pkgrel=2
pkgdesc="Extra modules and scripts for CMake"
arch=('i686' 'x86_64')
url='https://community.kde.org/Frameworks'
license=('Modified-BSD' 'Simplified-BSD' 'Expat')
depends=('cmake')
makedepends=('python-sphinx' 'python-requests' 'qt-tools')
groups=('kf5')
source=("https://download.kde.org/stable/frameworks/${pkgver%.*}/$pkgname-$pkgver.tar.xz"
        "ECM-no-init.py.patch"
        "ecm-sphinx4.patch"
        "force-relative-paths.patch")
sha512sums=('a8c8d1d15718760de42238b121ed1294d12d36f1ef25acdd2542f12a56d5091a5cc0135884b6c97e62123d64fc80e2756dccd32c88b65caaf6e86418bae4217f'
            'e3d7cc9f969e6a88717cf5532b4c7e5a85ba30eddc31dda4167fbb0a9c2bf28ea652f0ca3af0adbcc583820ea5f23af93d71ca14c1edb3fe52da501755d1fee7'
            'df9c83cf8c5d8f541f49dad9ac883dabef5cef713ff1cf7b9ee5ff777b0e95aaa1ca01278a96ea6867ae9eb0f183ec7ba768713d71a329e6f145cf85f333aa2c'
            '421a2fd2b6abc9937ba9365c532e3dfb96ffe5e34b450931ebd8d1125b48fe0628711f1283026977d31a88b614cc1cc1fe36a06ba7fd2f06e9abca3dfac6519f')

prepare() {
  patch -d $pkgname-$pkgver -p1 < $srcdir/ECM-no-init.py.patch
  patch -d $pkgname-$pkgver -p1 < $srcdir/ecm-sphinx4.patch
  patch -d $pkgname-$pkgver -p1 < $srcdir/force-relative-paths.patch
}

build() {
  cmake \
    -B build \
    -S $pkgname-$pkgver \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DBUILD_HTML_DOCS=ON \
    -DBUILD_QTHELP_DOCS=ON
  cmake --build build
}

package() {
  DESTDIR="$pkgdir" cmake --install build
  cd $pkgname-$pkgver
  install -Dm644 LICENSES/* -t $pkgdir/usr/share/licenses/$pkgname/
}
