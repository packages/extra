# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

# Based on xf86-input-mouse package

pkgname=xenocara-input-joystick-debug
_openbsdver=6.9
pkgver=1.6.3
pkgrel=3
pkgdesc="Xenocara joystick input driver"
arch=(i686 x86_64)
license=('X11')
url="https://www.xenocara.org"
depends=('glibc')
makedepends=('xenocara-server-devel' 'X-ABI-XINPUT_VERSION=24.1' 'xenocara-proto' 'xenocara-util-macros')
provides=('xf86-input-joystick')
conflicts=('xf86-input-joystick' 'xenocara-server<1.20' 'X-ABI-XINPUT_VERSION<24.1' 'X-ABI-XINPUT_VERSION>=25')
replaces=('xf86-input-joystick')
groups=('xenocara-drivers-debug' 'xorg-drivers-debug')
options=(!strip) # It's required for debug packages
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/driver/xf86-input-joystick-$pkgver.tar.lz{,.sig})
sha512sums=('0a3a5bb1a57419a52cf7c7a914a5f80c3cf5774a2acfd531036c89be6c161e3424c2c1ae3706691d917495e5208a4dd31863e0a0e21b6cc113e0509044502e9f'
            'SKIP')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/driver/xf86-input-joystick"
  autoreconf -vfi
}

build() {
  cd "xenocara-$_openbsdver/driver/xf86-input-joystick"

  # It's required for debug packages
  export CFLAGS=${CFLAGS/-O2/-O0 -g3}
  export CXXFLAGS=${CXXFLAGS/-O2/-O0 -g3}

  ./configure --prefix=/usr
  make
}

package() {
  cd "xenocara-$_openbsdver/driver/xf86-input-joystick"
  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
