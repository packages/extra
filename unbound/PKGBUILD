# Maintainer (Arch): Gaetan Bisson <bisson@archlinux.org>
# Contributor (Arch): Hisato Tatekura <hisato_tatekura@excentrics.net>
# Contributor (Arch): Massimiliano Torromeo <massimiliano DOT torromeo AT google mail service>
# Contributor (Artix): artoo <artoo@cromnix.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Jesús E. <heckyel@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=unbound
pkgver=1.13.1
pkgrel=5
pkgdesc='Validating, recursive, and caching DNS resolver'
url='https://unbound.net/'
license=('Modified-BSD')
arch=('i686' 'x86_64')
depends=('libressl' 'libsodium' 'libevent' 'ldns' 'dnssec-anchors')
makedepends=('expat' 'gettext-tiny')
optdepends=('expat: unbound-anchor'
            'logger: message logging support')
backup=('etc/unbound/unbound.conf'
        'etc/conf.d/unbound')
install=unbound.install
source=("https://unbound.net/downloads/${pkgname}-${pkgver}.tar.gz"
        "hook"
        "unbound.conf"
        "unbound.confd"
        "unbound.initd"
        "unbound.run"
        "CVE-2024-43167.patch")
sha512sums=('f4d26dca28dbcc33a5e65a55147fa01077c331292e88b6a87798cb6c3d4edb0515015d131fd893c92b74d22d9998a640f0adce404e6192d61ebe69a6a599287c'
            'dab83cc253d8ce8fac52a1fc7abcdb31e183a7245a887d85cad2c83d8b69ca4e4cbbcfc0914e71d686653b7252e40b29574ca0889da7d9b06e668a3ba8cfb763'
            'f0f87cd414e7efef28a3df09d09deb4eaec49d6694e93f532c6ec3f23ee5ed1c950bed047c755b20d21e618bde31f56e210208fbcc2f35ded880f0cdba45b4bc'
            'e3e4c7f97c13d133724417a70e4f20dac6f1f4b5a4e2e573c410148059d9f722589249b3dd4668069d4e324abef60f5981f20b1797623f37db9c9422410dc13f'
            '24b973b7da783c7bf66307eb09a837e6fcd2299e2cdfc78dd5e0e48e6faba3b7f23155c14997da0533495a04f4115b711df5f25b9400dbb70cf4aeebfdd75fd5'
            'cb1b931f9628f75ea2a72f1fa0489baa2af392208f503bb2bcee3af2221c3283ea6e584d65722abe01c8420aaec94c8b4263cd9b6212f2e06c5d8eb5150ba985'
            '52dad70da916d3935f80f10399c3a86226349dcece4a8674b20f77ec36069961dfc6137526aa96ad428b38531958d96a1905a37b49a620df03b9e590c3ab62b5')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  patch -Np1 -i ${srcdir}/CVE-2024-43167.patch
  sed '/# trust-anchor-file:/c\\ttrust-anchor-file: /etc/unbound/trusted-key.key' -i doc/example.conf.in
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --disable-rpath \
    --disable-systemd \
    --disable-dnstap \
    --enable-dnscrypt \
    --enable-pie \
    --enable-relro-now \
    --enable-subnet \
    --enable-tfo-client \
    --enable-tfo-server \
    --enable-cachedb \
    --with-conf-file=/etc/unbound/unbound.conf \
    --with-pidfile=/run/unbound.pid \
    --with-rootkey-file=/etc/trusted-key.key \
    --with-libevent

  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install
  install -Dm644 doc/example.conf.in "${pkgdir}/etc/unbound/unbound.conf.example"
  install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
  install -Dm644 $srcdir/unbound.confd "${pkgdir}/etc/conf.d/unbound"
  install -Dm755 $srcdir/unbound.initd "${pkgdir}/etc/init.d/unbound"
  install -Dm644 $srcdir/unbound.conf "${pkgdir}/etc/unbound/unbound.conf"
  install -Dm755 $srcdir/unbound.run "${pkgdir}/etc/sv/unbound/run"
  install -Dm644 $srcdir/hook "${pkgdir}/usr/share/libalpm/hooks/unbound-key.hook"
}
