# Maintainer (Arch): Ray Rashif <schiv@archlinux.org>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=libconfig
pkgver=1.5
_debver=1.5
_debrel=0.4
pkgrel=4
pkgdesc="C/C++ Configuration File Library"
arch=('i686' 'x86_64')
url="https://hyperrealm.com/libconfig/libconfig.html"
license=('LGPL-2.1')
depends=('gcc-libs' 'texinfo')
makedepends=('quilt')
options=('!emptydirs' 'zipman')
source=("https://github.com/hyperrealm/libconfig/archive/v$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/libc/libconfig/libconfig_$_debver-$_debrel.debian.tar.xz")
sha512sums=('f3a0a969d56604c473b004fd15e541569a6b7639a23db896e391d6280060c278f0adbae1da92bc2a9a55431e74a9312d7581cc47dc27d043a9ac3b045ae2a8a0'
            'b7702c056aecc73d0f5d7f4c2603d4b9694b5f0c312df2cfc08fbe2265d68f452444a746f0c34254303a1e9467e2686a1b0b9fc377d8cde74a91e22a8f2fe7c0')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian/patches debian

    quilt push -av
  fi

  autoreconf -vfi
}

build() {
  cd "$srcdir/$pkgname-$pkgver"

  ./configure --prefix=/usr
  make
}

package() {
  cd "$srcdir/$pkgname-$pkgver"

  make DESTDIR="$pkgdir" install
  rm "$pkgdir/usr/share/info/dir"

  install -Dm644 COPYING.LIB "$pkgdir/usr/share/licenses/$pkgname/COPYING.LIB"
}

# vim:set ts=2 sw=2 et:
