# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Maintainer (Arch): Filipe Laíns <lains@archlinux.org>
# Contributor (Arch): Timothy Redaelli <timothy.redaelli@gmail.com>
# Contributor (Arch): Guillaume ALAUX <guillaume@archlinux.org>
# Contributor (Arch): Florian Pritz <bluewind at jabber dot ccc dot de>
# Contributor (Arch): Peter Wu <peter@lekensteyn.nl>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad

pkgbase=wireshark
pkgname=('wireshark-cli' 'wireshark-qt')
pkgver=4.0.17
_debver=$pkgver
_debrel=0
pkgrel=1
pkgdesc="Network traffic and protocol analyzer/sniffer"
url='https://www.wireshark.org/'
arch=('i686' 'x86_64')
license=('GPL-2' 'Public-Domain' 'LGPL-2' 'Modified-BSD' 'ISC' 'Simplified-BSD' 'GPL-3' 'custom:FSL-Kaz')
makedepends=('glibc' 'cmake' 'ninja' 'c-ares' 'qt-tools' 'qt-svg'
             'qt-multimedia' 'krb5' 'libpcap' 'libssh' 'libxml2' 'libnghttp2'
             'snappy' 'lz4' 'spandsp' 'gnutls' 'lua52' 'python' 'libcap' 'libnl'
             'glib2' 'libgcrypt' 'desktop-file-utils' 'libxslt'
             'hicolor-icon-theme' 'zlib' 'gcc-libs' 'asciidoctor'
             'doxygen' 'minizip' 'speexdsp' 'opus' 'quilt')
options=('!emptydirs')
source=("https://1.na.dl.wireshark.org/src/all-versions/${pkgbase}-${pkgver}.tar.xz"
        "https://deb.debian.org/debian/pool/main/w/${pkgbase}/${pkgbase}_${_debver}-${_debrel}+deb12u1.debian.tar.xz")
sha512sums=('c7eb6375df60009a6f2efd02385d959cffcfd565fc7254cd0d6aa595127266799d0d296894f8a2ff626103ecc64b763bc617543f7718b8788578711609e29797'
            'fd52503d352bf42d911a23ab5804f116ec828246689f3f018ec4f69b4f1937391316462173138dbac4d6621fbdc04cd18e3abb0158fd24f144331a179cfa4b78')

prepare() {
  cd ${pkgbase}-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd ${pkgbase}-${pkgver}
  cmake \
    -B build \
    -G Ninja \
    -DCMAKE_BUILD_TYPE=None \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBDIR=lib \
    -DCMAKE_INSTALL_RPATH= \
    -DCMAKE_SKIP_RPATH=ON \
    -DENABLE_ZSTD=OFF \
    -DENABLE_BROTLI=OFF \
    -DENABLE_BCG729=OFF \
    -Wno-dev
  ninja -C build -v
}

package_wireshark-cli() {
  pkgdesc+=" - CLI tools and data files"
  depends=('glibc' 'c-ares' 'krb5' 'libgcrypt' 'libcap' 'libpcap'
           'gnutls' 'glib2' 'lua52' 'libssh' 'libxml2' 'libnghttp2' 'snappy'
           'lz4' 'spandsp' 'zlib' 'speexdsp' 'opus')
  install=$pkgbase.install

  cd ${pkgbase}-${pkgver}
  DESTDIR="${pkgdir}" ninja -C build install

  install -Dm 644 "${srcdir}/${pkgbase}-${pkgver}/debian/copyright" "${srcdir}/${pkgbase}-${pkgver}/COPYING" -t $pkgdir/usr/share/licenses/$pkgname/

  # wireshark uid group is 150
  chgrp 150 "${pkgdir}/usr/bin/dumpcap"
  chmod 754 "${pkgdir}/usr/bin/dumpcap"

  cd "${pkgdir}"
  rm -r usr/share/mime \
    usr/share/icons \
    usr/share/man/man1/wireshark.1 \
    usr/bin/wireshark \
    usr/share/applications/org.wireshark.Wireshark.desktop \
    usr/share/metainfo/org.wireshark.Wireshark.metainfo.xml
}

package_wireshark-qt() {
  pkgdesc+=" - Qt GUI"
  depends=('glibc' 'desktop-file-utils' 'qt-multimedia' 'qt-svg' 'wireshark-cli'
           'shared-mime-info' 'hicolor-icon-theme' 'xdg-utils' 'gcc-libs'
           'zlib' 'libpcap' 'libgcrypt' 'libnl' 'minizip' 'speexdsp')

  cd ${pkgbase}-${pkgver}
  install -d "${srcdir}/staging"
  DESTDIR="${srcdir}/staging" ninja -C build install

  install -Dm 755 build/run/wireshark -t "${pkgdir}"/usr/bin
  install -Dm 644 build/doc/wireshark.1 -t "${pkgdir}"/usr/share/man/man1
  install -Dm 644 build/doc/wireshark.html -t "${pkgdir}"/usr/share/doc/wireshark

  cd "${srcdir}"/staging/usr/share
  install -Dm 644 applications/org.wireshark.Wireshark.desktop -t "${pkgdir}"/usr/share/applications
  install -Dm 644 mime/packages/org.wireshark.Wireshark.xml -t "${pkgdir}"/usr/share/mime/packages
  install -Dm 644 metainfo/org.wireshark.Wireshark.metainfo.xml -t "${pkgdir}"/usr/share/metainfo
  mv icons "${pkgdir}"/usr/share/icons

  install -Dm 644 "${srcdir}/${pkgbase}-${pkgver}/debian/copyright" "${srcdir}/${pkgbase}-${pkgver}/COPYING" -t $pkgdir/usr/share/licenses/$pkgname/
}
