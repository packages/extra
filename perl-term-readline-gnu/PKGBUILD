# Maintainer (Arch): Daniel Wallace <danielwallace at gtmanfred dot com>
# Contributor (Arch): jason ryan <jasonwryan@gmail.com>
# Contributor (Arch): AUR Perl <aurperl@juster.info>
# Contributor: Jesús E. <heckyel@hyperbola.info>

pkgname=perl-term-readline-gnu
pkgver=1.37
pkgrel=2
_debver=1.37
_debrel=1
pkgdesc="GNU Readline XS library wrapper"
arch=('i686' 'x86_64')
url='https://search.cpan.org/dist/Term-ReadLine-Gnu'
license=('GPL-1')
options=('!emptydirs')
depends=('perl')
makedepends=('quilt' 'gettext-tiny')
source=("https://search.cpan.org/CPAN/authors/id/H/HA/HAYASHI/Term-ReadLine-Gnu-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/libt/libterm-readline-gnu-perl/libterm-readline-gnu-perl_${_debver}-${_debrel}.debian.tar.xz"
        'COPYING')
sha512sums=('e2ada2104a9efef56dcda9999348674e0ba72bb5ece74961342a8d00fd66990fb471ef2f2af75cddce4c1d0210f9e219f017de08c1cc2c4ade7878b25b8c5915'
            '11b668e688a08e2c6849bb83a416dc0d9d4c425656564ca153fa99a3cdd83962a6e8a18c62e704b58a140b740fc2f62a36c7c64a6b1ef2698123776c009f7ebe'
            'e239b539f2c46c1ae3da87c4983a0fda67dc8ae622d607b507b5c523af3bdf99e7bea8931e3a7c6007910bfe9e21a761e03e8203de95b5aceea425a97d0a84c9')

prepare() {
  cd "Term-ReadLine-Gnu-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  export PERL_MM_USE_DEFAULT=1 PERL5LIB=""  \
         PERL_AUTOINSTALL=--skipdeps        \
         PERL_MM_OPT="INSTALLDIRS=vendor"   \
         PERL_MB_OPT="--installdirs vendor" \
         MODULEBUILDRC=/dev/null

  cd "Term-ReadLine-Gnu-${pkgver}"
  perl Makefile.PL
  make
}

check() {
  cd "Term-ReadLine-Gnu-${pkgver}"
  export PERL_MM_USE_DEFAULT=1 PERL5LIB=""
  make test
}

package() {
  cd "Term-ReadLine-Gnu-${pkgver}"
  make pure_install PERL_INSTALL_ROOT="${pkgdir}"
  find "${pkgdir}" -name .packlist -o -name perllocal.pod -delete
  install -D -m644 "${srcdir}/COPYING" "${pkgdir}/usr/share/licenses/${pkgname}/COPYING"
}

# vim:set ts=2 sw=2 et:
