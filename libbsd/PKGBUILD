# Maintainer (Arch): Lukas Fleischer <lfleischer@archlinux.org>
# Contributor (Arch): Sven-Hendrik Haase <sh@lutzhaase.com>
# Contributor (Arch): Gerardo Exequiel Pozzi <vmlinuz386@yahoo.com.ar>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=libbsd
pkgver=0.10.0
pkgrel=1
pkgdesc='Provides useful functions commonly found on BSD systems like strlcpy()'
arch=('i686' 'x86_64')
url="https://libbsd.freedesktop.org"
license=('Modified-BSD')
depends=('glibc')
options=('staticlibs')
source=("https://libbsd.freedesktop.org/releases/$pkgname-$pkgver.tar.xz"{,.asc})
sha512sums=('b75529785b16c93d31401187f8a58258fbebe565dac071c8311775c913af989f62cd29d5ce2651af3ea6221cffd31cf04826577d3e546ab9ca14340f297777b9'
            'SKIP')
validpgpkeys=('4F3E74F436050C10F5696574B972BF3EA4AE57A3') # Guillem Jover

build() {
  cd "$pkgname-$pkgver"

  ./configure --prefix=/usr
  make
}

check() {
  cd "$pkgname-$pkgver"
  make check
}

package() {
  cd "$pkgname-$pkgver"

  make DESTDIR="$pkgdir" install
  rm "${pkgdir}"/usr/lib/libbsd.a
  install -D -m644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"

  # included now in man-pages, FS#53351
  rm -f "${pkgdir}"/usr/share/man/man3/explicit_bzero.3
}
