#!/usr/bin/env bash

arch=$(uname -m)
if [[ $arch = "i686" ]]; then
  arch="x86"
fi
file=`basename "$0"`
if [ "$file" == 'openarena' ]; then
  file='openarena'
else
  file='oa_ded'
fi
cd '/usr/share/games/openarena'
"./${file}.${arch}" $@
