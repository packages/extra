# Maintainer (Arch): Juergen Hoetzel <juergen@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=clisp
pkgver=2.49.92
_debver=$pkgver
_debrel=3
pkgrel=2
pkgdesc='ANSI Common Lisp interpreter, compiler and debugger'
arch=('i686' 'x86_64')
license=('GPL-2')
url='https://clisp.sourceforge.io/'
depends=('readline' 'libsigsegv' 'ffcall' 'libxcrypt' 'libunistring')
makedepends=('quilt')
options=('!makeflags' '!emptydirs')
source=("${pkgname}-${pkgver}.tar.bz2::https://deb.debian.org/debian/pool/main/c/clisp/clisp_2.49.20180218+really${pkgver}.orig.tar.bz2"
        "https://deb.debian.org/debian/pool/main/c/clisp/clisp_2.49.20180218+really${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('07e4c920e9fbbe469ed40c3fa74f739344602c6ff3370b9e0ede1c0ad66fa2b723f093bfd589362340caf7fa48d64cf66d11a8caa39ed6bcd90e42a92f97d1a6'
            'd943b45704ad0a6654b59f149eac19fadbcd759e7d085c9a434ac3aeccc069a84aa41e0592d8f492b6c7243550d80cc8b95666174cb80168d6809095eae8d43e')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr --with-readline --with-ffcall src

  cd src
  ./makemake --prefix=/usr --with-readline --with-ffcall --with-dynamic-ffi > Makefile
  make
}

package() {
  cd $pkgname-$pkgver/src
  make DESTDIR="$pkgdir" install
  install -Dm644 "${srcdir}/${pkgname}-${pkgver}/COPYRIGHT" -t "${pkgdir}/usr/share/licenses/$pkgname"
}
