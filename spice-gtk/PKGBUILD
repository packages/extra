# Maintainer (Arch): Balló György <ballogyor+arch at gmail dot com>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Stefano Facchini <stefano.facchini@gmail.com>
# Contributor (Arch): Jonathan Lestrelin <zanko@daemontux.org>
# Contributor (Arch): Lucio Zara <pennega@gmail.com>
# Maintainer: Márcio Silva <coadde@hyperbola.info>
# Contributor: Jayvee Enaguas <harvettfox96@dismail.de>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=spice-gtk
pkgver=0.39
pkgrel=3
pkgdesc="GTK client library for SPICE"
arch=('i686' 'x86_64')
url='https://www.spice-space.org/'
license=('LGPL-2.1')
depends=('gtk' 'gst-plugins-base' 'gst-plugins-good' 'opus' 'usbredir' 'libjpeg-turbo'
         'libsasl' 'mesa-libgl')
makedepends=('gobject-introspection' 'meson' 'spice-protocol' 'usbutils' 'vala')
replaces=('spice-glib' 'spice-gtk3')
# libcap-ng is recommended for GNU/Linux in security isolation.
if [ $(uname -s) = HyperbolaBSD ]; then
  _libcap_ng=disabled
else
  _libcap_ng=enabled
  depends+=('libcap-ng')
fi
source=("https://www.spice-space.org/download/gtk/${pkgname}-${pkgver}.tar.xz"{,.sig})
sha512sums=('ff0f3ca6b10a2c415f2fa8d61464c5710aaa2a46c2c83909f146fa45f01151e756d9c3d79cb162dd3d0c1279b6ef55a67fc5c1266af2cb5b46ac1eaa0254c8d2'
            'SKIP')
validpgpkeys=('206D3B352F566F3B0E6572E997D9123DE37A484F') # Victor Toso de Carvalho <me@victortoso.com>

build() {
  cd ${pkgname}-${pkgver}
  # Partially uses parameters based on OpenBSD to build this package.
  # - https://cvsweb.openbsd.org/cgi-bin/cvsweb/ports/x11/spice-gtk/Makefile?rev=1.51
  hyperbola-meson build \
    -Dpolkit=disabled \
    -Dwebdav=disabled \
    -Dlibcap-ng=$_libcap_ng \
    -Dgtk_doc=disabled \
    -Dsmartcard=disabled
  ninja -v -C build
}

package() {
  cd ${pkgname}-${pkgver}
  DESTDIR=${pkgdir} meson install -C build
  install -Dvm644 COPYING -t ${pkgdir}/usr/share/licenses/${pkgname}
}
