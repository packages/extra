# Maintainer (Arch): Ronald van Haren <ronald.archlinux.org>
# Contributor (Arch): Jan de Groot <jgc@archlinux.org>
# Contributor (Arch): damir <damir@archlinux.org>
# Contributor (Arch): Jason Taylor <jftaylor21@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=lapack
pkgname=(lapack blas cblas lapacke lapack-doc)
pkgver=3.9.0
_debver=$pkgver
_debrel=3
pkgrel=2
url='https://www.netlib.org/lapack'
pkgdesc="Linear Algebra PACKage"
makedepends=(gcc-fortran cmake python doxygen quilt)
arch=(i686 x86_64)
license=(Modified-BSD)
source=($pkgbase-$pkgver.tar.gz::"https://github.com/Reference-LAPACK/lapack/archive/v$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/l/lapack/lapack_${_debver}-${_debrel}+deb11u1.debian.tar.xz"
        "lapacke-missing-symbols.patch")
sha512sums=('424956ad941a60a4b71e0d451ad48db12a692f8a71a90f3ca7f71d6ecc1922f392746ea84df1c47a46577ed2db32e9e47ec44ad248207c5ac7da179becb712ef'
            '7919ce67169ffae66af2baa1ab91186bb4e3ecaf3812bc6284b493d51f2abc688c5371fdb7b303535c52e38e0c9ed3f8ec6a643b524d6c027595601b08d7a6b1'
            '7964addb97716b24a0d9ea375c826925256a0efe82547921e0e5867cc12f2b611d626232c0e26d26cdaeea9fd36674ed5275407ad7efcc060c673cde0082c3a6')

prepare() {
  cd $pkgbase-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/python3.patch || true

    quilt push -av
  else
    # https://github.com/Reference-LAPACK/lapack/issues/365
    patch -p1 -i $srcdir/lapacke-missing-symbols.patch
  fi
}

build() {
  cmake \
    -S $pkgbase-$pkgver \
    -B build \
    -DCMAKE_SKIP_RPATH=ON \
    -DBUILD_SHARED_LIBS=ON \
    -DBUILD_TESTING=OFF \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_Fortran_COMPILER=gfortran \
    -DLAPACKE_WITH_TMG=ON \
    -DCBLAS=ON \
    -DBUILD_DEPRECATED=ON
  make -C build

  # build man pages
  cd $pkgname-$pkgver
  doxygen DOCS/Doxyfile_man
}

package_lapack() {
  depends=(blas)

  cd build
  make DESTDIR="$pkgdir" install

  rm -r "$pkgdir"/usr/lib/{libblas.*,libcblas.*,liblapacke.*}
  rm -r "$pkgdir"/usr/lib/pkgconfig/{blas.*,cblas.*,lapacke.*}
  rm -r "$pkgdir"/usr/lib/cmake/{cblas*,lapacke*}
  rm -r "$pkgdir"/usr/include

  install -Dm644 $srcdir/$pkgbase-$pkgver/LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}

package_blas() {
  pkgdesc="Basic Linear Algebra Subprograms"
  depends=(gcc-libs)

  cd build/BLAS
  make DESTDIR="$pkgdir" install

  install -Dm644 $srcdir/$pkgbase-$pkgver/LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}

package_cblas() {
  pkgdesc="C interface to BLAS"
  depends=(blas)

  cd build/CBLAS
  make DESTDIR="$pkgdir" install

  install -Dm644 $srcdir/$pkgbase-$pkgver/LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}

package_lapacke() {
  pkgdesc="C interface to LAPACK"
  depends=(lapack)

  cd build/LAPACKE
  make DESTDIR="$pkgdir" install

  install -Dm644 $srcdir/$pkgbase-$pkgver/LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}

package_lapack-doc() {
  pkgdesc="Man pages for BLAS/LAPACK"

  mkdir -p "$pkgdir"/usr/share
  cp -r lapack-$pkgver/DOCS/man "$pkgdir"/usr/share

  install -Dm644 $srcdir/$pkgbase-$pkgver/LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}
