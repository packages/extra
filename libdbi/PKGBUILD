# Maintainer (Arch): Balló György <ballogyor+arch at gmail dot com>
# Contributor (Arch): Thorsten Töpper <atsutane-tu@freethoughts.de>
# Contributor (Arch): Carl Flippin <carlf@photocarl.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=libdbi
pkgver=0.9.0
_debver=$pkgver
_debrel=6
pkgrel=2
pkgdesc="Database independant abstraction layer for C"
url='https://libdbi.sourceforge.net/'
license=('LGPL-2.1')
arch=('i686' 'x86_64')
depends=('glibc')
makedepends=('quilt')
options=(!emptydirs)
source=("https://downloads.sourceforge.net/${pkgname}/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/libd/libdbi/libdbi_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('ee8777195af43057409d051a6055ec0467cd926d48da076458b09f91d2f0995a1cc4bc071762e401b7bdcd8a4173fd8ea3472db3a1518e34b4c5b5ed24e4e2ce'
            '7053df9ed2319e081d22d5df218a65d74119bf6fb71899a233434081800ffea786ae51ae2c90365fb91ae54682f7b8774dd7cd6817c26d815f79e6300380ce27')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  ./configure \
    --prefix=/usr
  make
}

package() {
  cd $pkgbase-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
