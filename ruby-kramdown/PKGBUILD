# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad

_gemname=kramdown
pkgname=ruby-kramdown
pkgver=2.3.0
_debver=$pkgver
_debrel=5
pkgrel=1
pkgdesc="Fast, pure Ruby Markdown superset converter, using a strict syntax definition"
url='https://kramdown.gettalong.org/'
arch=('any')
license=('Expat')
depends=('ruby' 'ruby-rexml')
makedepends=('quilt')
options=('!emptydirs')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/gettalong/kramdown/archive/REL_${pkgver//./_}.tar.gz"
        "https://deb.debian.org/debian/pool/main/r/$pkgname/${pkgname}_$_debver-$_debrel.debian.tar.xz")
sha512sums=('9bce4aae940a31e975e1582c47edce046d1fcd61e8dd4a56e5088761b2a3cf4c72cb322c55af4d9b17db14d1e0db56602f8d56e6724e2f9be36307de4249bb84'
            'c0b8e1a08365be965438ac1df31efd772cd331ffdd00df3e30511d4a9e16f1d4316f69081af7a12716d7c0b9422b3225385dfebff863dbf14e54841410db98e1')

prepare() {
  cd ${_gemname}-REL_${pkgver//./_}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .
    rm -v debian/patches/fix_manpage_warnings.patch || true

    quilt push -av
  fi
  rake gemspec
  sed -r 's|~>|>=|g' -i ${_gemname}.gemspec
}

build() {
  cd ${_gemname}-REL_${pkgver//./_}
  gem build ${_gemname}.gemspec
}

package() {
  cd ${_gemname}-REL_${pkgver//./_}
  local _gemdir="$(gem env gemdir)"
  gem install --ignore-dependencies --no-user-install -i "${pkgdir}${_gemdir}" -n "${pkgdir}/usr/bin" ${_gemname}-${pkgver}.gem
  install -Dm 644 README.md -t "${pkgdir}/usr/share/doc/${pkgname}"
  install -Dm 644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
  install -d "${pkgdir}/usr/share/man/man1"
  mv "${pkgdir}/${_gemdir}/gems/kramdown-${pkgver}/man/man1/kramdown.1" "${pkgdir}/usr/share/man/man1"
  rm "${pkgdir}/${_gemdir}/cache/${_gemname}-${pkgver}.gem"
  rm -r "${pkgdir}/${_gemdir}/gems/kramdown-${pkgver}/test"
}
