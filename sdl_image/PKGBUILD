# Maintainer (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor (Arch): Allan McRae <allan@archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=sdl_image
pkgver=1.2.12
_debver=1.2.12
_debrel=12
pkgrel=1
pkgdesc='A simple library to load images of various formats as SDL surfaces'
url='https://www.libsdl.org/projects/SDL_image/'
arch=('i686' 'x86_64')
license=('zlib')
depends=('sdl>=1.2.13' libpng libjpeg-turbo libtiff zlib)
makedepends=('quilt')
source=(https://www.libsdl.org/projects/SDL_image/release/SDL_image-$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/s/sdl-image1.2/sdl-image1.2_$_debver-$_debrel.debian.tar.xz)
sha512sums=('0e71b280abc2a7f15755e4480a3c1b52d41f9f8b0c9216a6f5bd9fc0e939456fb5d6c10419e1d1904785783f9a1891ead278c03e88b0466fecc6871c3ca40136'
            'cb6e8b720aa1f821ea462fa70389a43b7358b79e4df186209f1edc2857c92627f4b92321fc4445535be01affe14df0fd23cf27e7efb50531f1986e6d80913d63')

prepare() {
  cd SDL_image-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd SDL_image-$pkgver
  ./configure --prefix=/usr --disable-static
  make
}

package() {
  cd SDL_image-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dt "$pkgdir/usr/share/licenses/$pkgname" -m644 COPYING
}
