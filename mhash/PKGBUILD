# Maintainer (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Contributor (Arch): gregor <gregor@archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=mhash
pkgver=0.9.9.9
_debver=0.9.9.9
_debrel=9
pkgrel=1
pkgdesc="A thread-safe hash library which provides a uniform interface to hash algorithms (MD5, SHA1, HAVAL, etc)"
arch=('i686' 'x86_64')
url="http://mhash.sourceforge.net/"
license=('LGPL-2')
depends=('glibc')
makedepends=('quilt')
source=(https://downloads.sourceforge.net/sourceforge/mhash/${pkgname}-${pkgver}.tar.bz2
        https://deb.debian.org/debian/pool/main/m/mhash/mhash_${_debver}-${_debrel}.debian.tar.xz)
sha512sums=('3b063d258cb0e7c2fa21ed30abae97bd6f3630ecd1cb4698afb826aa747555f3cf884828f24ac5e2b203730d0c7c0ecc9ef1e724ad9d85769a2f66128f3072eb'
            '0cf2458e58a9cc0c274c29ce48f40f66fd557286675b477a885295628358bb1ceb0796f24920764c3cd5dd0b5bbb5808c913a07042dedc33a8415c471b36bc6c')

prepare() {
  cd ${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd ${pkgname}-${pkgver}
  ./configure --prefix=/usr
  make
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}
