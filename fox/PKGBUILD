# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Ben <ben@benmazer.net>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=fox
epoch=1
pkgver=1.6.57
_debver=1.6.57
_debrel=1
pkgrel=1
pkgdesc='Free Objects for X: GUI Toolkit for C++'
arch=('i686' 'x86_64')
url='http://www.fox-toolkit.org/'
license=('LGPL-2.1')
depends=('bzip2' 'libxcursor' 'libxft' 'libxrandr' 'libxi' 'libpng' 'libtiff' 'glu')
makedepends=('mesa' 'graphicsmagick' 'quilt' 'automake-1.15')
optdepends=('perl')
source=(http://fox-toolkit.org/ftp/fox-$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/f/fox1.6/fox1.6_$_debver-$_debrel.debian.tar.xz
        adie.desktop
        calculator.desktop
        ControlPanel.desktop
        PathFinder.desktop)
sha512sums=('e7b91054a9ea674818803415284cb61572935d22466e446cb37ef68344e2684e8acd567f7c8a6912f939dcbcc37455119ad6a01ac30513f460d0b3937b7e6087'
            'eae83d0065bd349427971b5174357af5d32d97fe786819e7978b2a7d03bfe87e5afd6b3dda2329dd38f48eca038606b1d30038a39807d40684df0cd7723d83d2'
            'ffda52709989093a377f265748941bd1da249c6fe4514ed0a969fe0d203b485ae618049279376988589716401132c9e10af82ff770bb81c0d771bfdfeaefdc24'
            'bff9b22e244830271d284b1c3e49936e9f4596eaad97a9ba510b6f82f3ac13210b354e8997cbe487aab6e2e5ed0eec90a9a739aa400f5d7a4953888d14368aaa'
            '90a718497698d6938775d48abd1b018dc19e3a450b9702a045c26d61494464a47a940eb8c239afb339bc394f074254223f194fd313320bf91a76291d938180dd'
            '57f9ef683b0d5427453cb8268adcfd6c306a6769218ce004c1f1387d48811d625791229eba9aea69f77984148e40b16f10e64b0ebab09f4716b89bb4d4011428')

prepare() {
  cd "$srcdir"/${pkgname}-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/12_build-reproducibly-without-date.patch || true

    quilt push -av
  fi
}

build() {
  cd "$srcdir"/${pkgname}-${pkgver}
  gm convert +set date:create +set date:modify adie/big_gif.gif adie.png
  gm convert +set date:create +set date:modify calculator/bigcalc.gif calculator.png
  gm convert +set date:create +set date:modify pathfinder/file_gif.gif PathFinder.png

  export CPPFLAGS="$CPPFLAGS -I/usr/include/freetype2"
  ./configure   --prefix=/usr \
                --enable-release \
                --with-xft=yes \
                --with-opengl=yes \
                --with-xim \
                --with-xshm \
                --with-shape \
                --with-xcursor \
                --with-xrender \
                --with-xrandr \
                --with-xfixes \
                --with-xinput
  make
}

package() {
  cd "$srcdir"/${pkgname}-${pkgver}
  make DESTDIR="$pkgdir" install

  pushd tests
  make ControlPanel
  install -m755 .libs/ControlPanel "$pkgdir"/usr/bin/
  popd

  install -Dm644 LICENSE -t "$pkgdir"/usr/share/licenses/$pkgname

  install -dm755 "$pkgdir"/usr/share/{applications,pixmaps}
  install -m644 *.png "$pkgdir"/usr/share/pixmaps/
  install -m644 "$srcdir"/*.desktop "$pkgdir"/usr/share/applications/
}
