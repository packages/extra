# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Maintainer (Arch): Eric Bélanger <eric@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=wget
pkgver=1.21.3
_debver=$pkgver
_debrel=1
pkgrel=2
pkgdesc="Network utility to retrieve files from the Web"
url='https://www.gnu.org/software/wget/wget.html'
arch=('i686' 'x86_64')
license=('custom:GPL-3+OpenSSL-Linking-Exception')
depends=('glibc' 'libressl' 'libidn2' 'libutil-linux' 'libpsl' 'pcre2')
makedepends=('quilt')
checkdepends=('perl-http-daemon' 'perl-io-socket-ssl' 'python')
optdepends=('ca-certificates: HTTPS downloads')
backup=('etc/wgetrc')
source=("https://ftp.gnu.org/gnu/${pkgname}/${pkgname}-${pkgver}.tar.lz"
        "https://deb.debian.org/debian/pool/main/w/wget/wget_${_debver}-${_debrel}.debian.tar.xz"
        "CVE-2024-38428.patch")
sha512sums=('489b9beba237df4555ee3b22bf3ae1f654d448e29f5772a52690f7b7cd7e63679e295bdadb6d55d28d2e4f9ccf9a85a04a6b189e1b5333e9133613685d6cfc66'
            '7e1241311d04a1f111da59ca3daaa500d3baa0b8c30edcb9d2a1f6a15fe63dca3d6fc0aea81293baf2e2a24bde096c765eecb18555ebae89f60c5e1e5472cab4'
            '330468d28f2121960b36c80fc91b02005594affab9f0d20a887f749fbf00541f0fc31d7eed683956b97ef321297e3e25bf65870a74b6d484c8529be89adac887')

prepare() {
  cd ${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  patch -Np1 -i ${srcdir}/CVE-2024-38428.patch

  cat >> doc/sample.wgetrc <<EOF

# default root certs location
ca_certificate=/etc/ssl/certs/ca-certificates.crt
EOF
}

build() {
  cd ${pkgname}-${pkgver}
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --disable-rpath \
    --enable-nls \
    --with-ssl=openssl
  make
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install
  for i in COPYING README; do
    install -Dm644 ${i} ${pkgdir}/usr/share/licenses/${pkgname}/${i}
  done
}