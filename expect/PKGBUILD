# Maintainer (Arch): Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor (Arch): Allan McRae <allan@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=expect
pkgver=5.45.4
_debver=5.45.4
_debrel=2
pkgrel=2
pkgdesc='A tool for automating interactive applications'
arch=(i686 x86_64)
url='http://www.nist.gov/el/msid/expect.cfm'
license=(Public-Domain)
depends=(tcl)
makedepends=(quilt)
source=(https://downloads.sourceforge.net/project/expect/Expect/$pkgver/expect${pkgver}.tar.gz
        https://deb.debian.org/debian/pool/main/e/expect/expect_$_debver-$_debrel.debian.tar.xz
        public-domain.txt)
sha512sums=('a8dc25e8175f67e029e15cbcfca1705165c1c4cb2dd37eaaaebffb61e3ba132d9519cd73ca5add4c3358a2b0b7a91e878279e8d0b72143ff2c287fce07e4659a'
            '33b6d60e96ff4faabe663480d9e2a650db049151d7780ab7d322b9303b6c1d06e59384d232cec2ada62a81881e013413795170040a5f62f96980f7e05e145326'
            '2d6eae51298801dae8dda6575a67d0403b28f21b5b4d8dae63f39bc9d0109c956f1d2ff5047794fa481e4630b0a448aebc057dc1f4ec80ad07dbfc566c7d0dc3')

prepare() {
  cd expect${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/02-example-cryptdir.patch || true
    rm -v debian/patches/05-makefile-soname.patch || true
    rm -v debian/patches/06-pkgindex.patch || true
    rm -v debian/patches/29-suggest-tk.patch || true

    quilt push -av
  fi
}

build() {
  cd expect${pkgver}
  ./configure --prefix=/usr --mandir=/usr/share/man
  make
}

check() {
  cd expect${pkgver}
  make test
}

package() {
  cd expect${pkgver}
  make DESTDIR="$pkgdir" install
  install -Dm644 README "$pkgdir/usr/share/licenses/$pkgname/README"
  install -Dm644 ../public-domain.txt "$pkgdir/usr/share/licenses/$pkgname/public-domain.txt"
}
