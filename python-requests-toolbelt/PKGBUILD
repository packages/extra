# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): eolianoe <eolianoe At GoogleMAIL DoT com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: Jesús E. <heckyel@hyperbola.info>

pkgname=python-requests-toolbelt
pkgver=0.9.1
_debver=$pkgver
_debrel=1
pkgrel=2
pkgdesc="A toolbelt of useful classes and functions to be used with python-requests."
arch=('any')
url="https://github.com/requests/toolbelt"
license=('Apache-2.0')
depends=('python-requests')
makedepends=('python-setuptools' 'python-requests' 'quilt')
source=("$pkgname-$pkgver.tar.gz::https://github.com/requests/toolbelt/archive/$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/p/python-requests-toolbelt/python-requests-toolbelt_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('12667056c225ce0bc88a5959660103feed23810890abd3890ef15581aa64c09c0552c3974473e1742cbe6200bd37d6475ad34ec051e83d4fbf0a33f320dbc0cc'
            'b240ba6cb092a47e9252a9e9b86ac94240f1519c8cbf1294b42f8370f8229766251d028ed18417c535295a8cf4da2fcd0f62ae16411f691c70b1252e0dc7ea45')

prepare() {
  cd toolbelt-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "${srcdir}"/debian .

    quilt push -av
  fi
  # Disable some failing tests
  sed -e 's:test_stream_response:_&:' \
      -i tests/test_downloadutils.py
  sed -e 's:test_no_content_length_header:_&:' \
      -e 's:test_read_file:_&:' \
      -e 's:test_reads_file_from_url_wrapper:_&:' \
      -i tests/test_multipart_encoder.py
  sed -e 's:test_auth_generator_is_called:_&:' \
      -e 's:test_initializer_is_called:_&:' \
      -e 's:test_number_of_processes_can_be_arbitrary:_&:' \
      -e 's:test_session_is_called:_&:' \
      -i  tests/threaded/test_pool.py
}

build() {
  cd toolbelt-$pkgver
  python setup.py build
}

package() {
  cd toolbelt-$pkgver
  python setup.py install --root="$pkgdir" --optimize=1
  install -D -m644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
