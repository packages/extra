# Maintainer (Arch): Evangelos Foutras <evangelos@foutrelis.com>
# Contributor (Arch): Gaetan Bisson <bisson@archlinux.org>
# Contributor (Arch): Alexander Fehr <pizzapunk gmail com>
# Contributor (Arch): Giovanni Scafora <giovanni@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

_realpkgname=audacious
pkgname=$_realpkgname-legacy
pkgver=4.2
_debver=$pkgver
_debrel=1
pkgrel=1
pkgdesc="Lightweight, advanced audio player focused on audio quality (legacy version)"
arch=('i686' 'x86_64')
url='https://audacious-media-player.org/'
license=('Simplified-BSD')
depends=('gtk2' 'qt-base' 'glib2' 'libsm' 'hicolor-icon-theme' 'desktop-file-utils')
makedepends=('quilt')
optdepends=('unzip: zipped skins support'
            'audacious-plugins-legacy: additional plugins')
source=("https://distfiles.audacious-media-player.org/$_realpkgname-$pkgver.tar.bz2"
        "https://deb.debian.org/debian/pool/main/a/audacious/audacious_$_debver-$_debrel.debian.tar.xz")
sha512sums=('814bd865e87cf9bc39df07c9f9898eca1dd94d8b8299b2709f77046669ce12e9c31067d55084371f6797724e79872f7571b575cc766093ee2124b38b0409a853'
            '2e8ed42d95dc4aa052826b2053e39c6be0ef913448fb1f9672ead88bb74c83ec6cce25c8d85d76db2b4da5d7a13e57602a0bcaaf04e0c80abe327a46f363b566')

prepare() {
  cd "$srcdir/$_realpkgname-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$srcdir/$_realpkgname-$pkgver"
  ./configure \
    --prefix=/usr \
    --enable-dbus=no \
    --enable-gtk=yes \
    --enable-qt=yes \
    --with-buildstamp='Hyperbola'
  make
}

package() {
  cd "$srcdir/$_realpkgname-$pkgver"
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}
