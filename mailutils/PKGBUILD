# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=mailutils
pkgver=3.10
_debver=$pkgver
_debrel=3
pkgrel=1
pkgdesc="MUA command line tool (mailx)"
arch=('i686' 'x86_64')
url="https://www.gnu.org/software/mailutils/"
license=('GPL-3')
depends=('libldap' 'gnutls' 'gsasl')
makedepends=('quilt' 'automake-1.15')
options=(!emptydirs)
source=("https://ftp.gnu.org/gnu/mailutils/mailutils-${pkgver}.tar.bz2"{,.sig}
        "https://deb.debian.org/debian/pool/main/m/mailutils/mailutils_${_debver}-${_debrel}.debian.tar.xz"
        "fix-build-mb-len-max.patch")
sha512sums=('012fd10d580cf5f90365f92f9bf5a8d8f587d0eb34dda86561298ec38cb942b05bf53a364bf3f51687f35aa611baf9364349aeacd81ac2f278bb56498e99aadc'
            'SKIP'
            '53f5bc9a266fe2f6074003c6c2ac89a80d130eadea7923a1632237066ba8100ce7c004f27ec07e8794bdc2e45775f2aa4999b1488b98cdc991fe2b3ee21613e0'
            '48248bfae1cc3b80c3decfcb99f803b65fd0d068f6c09625eb5e3763b3184090d7bd97fa3bc916b3e2e8c737b2bd501aa25482a96aceb555c08307b54c2a24a8')
validpgpkeys=('325F650C4C2B6AD58807327A3602B07F55D0C732')

prepare() {
  cd "$srcdir/mailutils-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  # additional fixes
  patch -Np1 -i $srcdir/fix-build-mb-len-max.patch

  autoreconf -vfi
}

build() {
  cd "$srcdir/mailutils-$pkgver"
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --with-gdbm \
    --with-gnutls \
    --disable-python \
    --disable-pam \
    --without-guile
  make CFLAGS="$CFLAGS -O2" || make CFLAGS="$CFLAGS -O2" LDFLAGS="-L`pwd`/lib -lmutcpwrap -ltinfo"
}

package() {
  cd "$srcdir/mailutils-$pkgver"
  make -j1 DESTDIR="$pkgdir" install
  rm -f "$pkgdir"/usr/share/info/dir
  mv "$pkgdir"/usr/bin/mail "$pkgdir"/usr/bin/gnu-mail
  mv "$pkgdir"/usr/share/man/man1/mail.1 "$pkgdir"/usr/share/man/man1/gnu-mail.1
  chown -R root:root "$pkgdir"
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}
