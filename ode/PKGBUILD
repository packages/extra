# Maintainer (Arch): Santiago Torres-Arias <santiago@archlinux.org>
# Contributor (Arch): Alexander F. Rødseth <xyproto@archlinux.org>
# Contributor (Arch): Giovanni Scafora <giovanni@archlinux.org>
# Contributor (Arch): Adam Griffiths <adam_griffithsAATTdart.net.au>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=ode
pkgver=0.16.2
pkgrel=1
pkgdesc='High performance library for simulating rigid body dynamics'
arch=('i686' 'x86_64')
url='https://bitbucket.org/odedevs/ode/'
license=('Original-BSD')
source=("https://bitbucket.org/odedevs/$pkgname/downloads/$pkgname-$pkgver.tar.gz")
sha512sums=('801bc80c3e14e82c355316071d130dea2eb30428a957fcc1e3e2702e974387b327621a6f9349b72ed2f84b0e20426756b34d41c94b63a1b6b8e690c5de598911')

prepare() {
  cd $pkgname-$pkgver
  ./bootstrap
}

build() {
  cd $pkgname-$pkgver
  export CXXFLAGS='-w -Ofast -fpermissive'
  ./configure \
    --prefix=/usr \
    --enable-shared \
    --enable-double-precision \
    --enable-libccd \
    --enable-ou \
    --enable-silent-rules
  make
}

package() {
  cd $pkgname-$pkgver
  DESTDIR="$pkgdir" make install
  install -Dm644 LICENSE-BSD.TXT -t "$pkgdir/usr/share/licenses/$pkgname"
}
