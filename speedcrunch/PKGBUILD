# Maintainer (Arch): Antonio Rojas <arojas@archlinux.org>
# Contributor (Arch): Ronald van Haren <ronald.archlinux.org>
# Contributor (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=speedcrunch
pkgver=0.12.0
_debver=$pkgver
_debrel=6
pkgrel=1
pkgdesc="Simple but powerful, free and libre calculator using Qt"
arch=('i686' 'x86_64')
url='https://speedcrunch.org/'
license=('GPL-2' 'CC0-1.0')
depends=('qt-tools')
makedepends=('cmake' 'quilt')
source=("${pkgname}-${pkgver}.tar.bz2::https://bitbucket.org/heldercorreia/speedcrunch/get/release-${pkgver}.tar.bz2"
        "http://deb.debian.org/debian/pool/main/s/speedcrunch/speedcrunch_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('b41f147f2530735af2fad0ecef6047459baed03904ec6d8097a3f21d7d200c2d89beda77676ea1ff762c5c164cd7b639cae84942f61c869acd77aa380c80f141'
            '8b11c046c30dbf1ffd645161788a084d027340da4e6a6ac81f35bc38afb3c7fb7fdbccc46e2b9deca68f7868c7681b9bf63b1748d31b8d9b43ee2315aeb3c92c')

prepare() {
  mv "heldercorreia-speedcrunch-ea93b21f9498" "$pkgname-$pkgver"
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cmake \
    -B build \
    -S $pkgname-$pkgver/src \
    -DCMAKE_INSTALL_PREFIX=/usr
  make -C build
}

package() {
  make DESTDIR="$pkgdir" -C build install
  install -Dm644 "$srcdir/$pkgname-$pkgver/debian/copyright" -t "$pkgdir/usr/share/licenses/$pkgname"
}