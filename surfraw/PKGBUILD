# Maintainer (Arch): Jaroslav Lichtblau <svetlemodry@archlinux.org>
# Contributor (Arch): Jeff Mickey <jeff@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=surfraw
pkgver=2.3.0
_debver=2.3.0
_debrel=0.3
pkgrel=1
pkgdesc="Shell Users' Revolutionary Front Rage Against the Web"
arch=('any')
url='https://gitlab.com/surfraw/Surfraw/'
license=('Public-Domain')
depends=('perl')
makedepends=('quilt')
install=$pkgname.install
backup=('etc/xdg/surfraw/conf'
        'etc/xdg/surfraw/bookmarks')
source=($pkgname-$pkgver.tar.gz::https://gitlab.com/surfraw/Surfraw/-/archive/$pkgname-$pkgver/Surfraw-$pkgname-$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/s/surfraw/surfraw_$_debver-$_debrel.debian.tar.xz)
sha512sums=('bb2ac06f2c9f1d14a1ddb9134fe82854a99bb9a5f1bd84093df7071d5c2662928234762cdc75d4530a788d618c3e7adbe962b3520a9de4c2b092ea435d1a9b27'
            '1db17084492896679e8a41a8eae07e18af2bd52325f63208e935d435d4761c518768b57b461c5317d0ee6b7a2b3e3f2bbd8422b602b7b9b8946cc17fa639913c')

prepare() {
  mv Surfraw-$pkgname-$pkgver $pkgname-$pkgver
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    rm -rf ./debian
    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver

  ./prebuild
  ./configure --prefix=/usr --sysconfdir=/etc
  make
}

package () {
  cd $pkgname-$pkgver

  make DESTDIR="${pkgdir}" install

  # license
  install -Dm644 COPYING -t "${pkgdir}"/usr/share/licenses/$pkgname
}
