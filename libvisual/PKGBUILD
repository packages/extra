# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=libvisual
pkgver=0.4.0
_debver=0.4.0
_debrel=17
pkgrel=7
pkgdesc="Abstraction library that comes between applications and audio visualisation plugins"
arch=('i686' 'x86_64')
url="https://sourceforge.net/projects/libvisual/"
license=('LGPL-2.1')
depends=('glibc')
makedepends=('quilt')
source=("https://downloads.sourceforge.net/sourceforge/libvisual/libvisual-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/libv/libvisual/libvisual_$_debver-$_debrel.debian.tar.xz"
        "libvisual-0.4.0-inlinedefineconflict.patch")
sha512sums=('ab2286de30d33582a92f16e46436fcbc44b74649952df6e94d96aedc2cabb18d3361496c0b8ab6f52f7178214bf735673c8f1820c3d149304787ba8407201b95'
            'bb36c664ee0575475dfcee8c64e9930f1850a00f670a44fc7ce8fd0bb0b0980828bca5096bfe12e0f208d3a0c7e81374362115a965e36f4f063a39d4b2d88dec'
            '13872adb2f99bd989b954499f80d2bd2646369d3fdb190a07e02cada1c42de7a7c36ca368b70f024484eb621f4320649a83107d4874a01f37ab2e10806dca89d')

prepare() {
  cd ${pkgname}-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  else
    # libvisual's definition of "inline" causes issues when compiling with the
    # new C++11 ABI (patch from Fedora)
    patch -Np1 -i ../libvisual-0.4.0-inlinedefineconflict.patch
  fi
}

build() {
  cd ${pkgname}-${pkgver}
  ./configure --prefix=/usr
  make
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING ${pkgdir}/usr/share/licenses/${pkgname}/COPYING
}
