# Maintainer (Arch): Christian Hesse <mail@eworm.de>
# Maintainer (Arch): Angel Velasquez <angvp@archlinux.org>
# Contributor (Arch): Eric Belanger <eric@archlinux.org>
# Contributor (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=htop
pkgver=3.3.0
_debver=$pkgver
_debrel=4
pkgrel=2
pkgdesc="Interactive process viewer"
arch=('i686' 'x86_64')
url='https://htop.dev/'
license=('GPL-2')
depends=('ncurses' 'libnl')
makedepends=('quilt')
optdepends=('lsof: show files opened by a process'
            'strace: attach to a running process')
options=('!emptydirs')
source=("https://github.com/htop-dev/htop/archive/${pkgver}/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/h/htop/htop_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('4c03bd183d97ec84010472ea52ff6e210e6d211c813d0ab52bacef16d7e4eef9483e65279fb0a846dcdb532ad19eb1c1c39bad9dd8b103d36aeb51cb5d28e23a'
            '283d461ce833928edca3fb98c03fa57be0e74ea4da14f6a4effc96165a33ec3901fd041f8f4d1c471f9707f272a5fddfd195714e3571223c3e71a48e40aa31b6')

prepare() {
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  autoreconf -fi
}

build() {
  cd $pkgname-$pkgver

  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --enable-affinity \
    --enable-capabilities \
    --enable-delayacct \
    --enable-openvz \
    --enable-unicode \
    --enable-vserver

  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
