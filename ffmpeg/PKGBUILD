# Maintainer (Arch): Maxime Gauduin <alucryd@archlinux.org>
# Contributor (Arch): Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor (Arch): Ionut Biru <ibiru@archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Contributor (Arch): Paul Mattal <paul@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=ffmpeg
pkgver=4.4.5
pkgrel=5
pkgdesc="Free and libre software to record, convert and stream audio and video"
arch=('i686' 'x86_64')
url='https://ffmpeg.org/'
license=('GPL-3')
depends=('alsa-lib' 'bzip2' 'fontconfig' 'fribidi' 'gmp' 'gnutls' 'jack' 'lame' 'libass'
         'libdrm' 'freetype2' 'libmodplug' 'librsvg-legacy' 'libsoxr' 'libtheora' 'vid.stab'
         'libvorbis' 'libx11' 'x264' 'libxcb' 'libxext' 'libxml2' 'libxv' 'xvidcore' 'zimg'
         'openjpeg2' 'opus' 'sdl2' 'speex' 'v4l-utils' 'xz' 'zlib')
makedepends=('clang' 'libjack' 'ladspa' 'nasm')
optdepends=('ladspa: LADSPA filters')
source=("https://ffmpeg.org/releases/ffmpeg-${pkgver}.tar.xz"{,.asc}
        "0001-libavutil-clean-up-unused-FF_SYMVER-macro.patch"
        "0001-ffbuild-libversion.sh-add-shebang.patch")
validpgpkeys=('FCF986EA15E6E293A5644F10B4322F04D67658D8') # FFmpeg release signing key <ffmpeg-devel@ffmpeg.org>
sha512sums=('70df4e63ef507a7ec76da34438142499139769728fd5130d9cf48d56c110ec82c3d6a7e6d1622da03c70167fa861d901d016bbe52c21d2b284b8a0d9f30811dc'
            'SKIP'
            '1047a23eda51b576ac200d5106a1cd318d1d5291643b3a69e025c0a7b6f3dbc9f6eb0e1e6faa231b7e38c8dd4e49a54f7431f87a93664da35825cc2e9e8aedf4'
            '38443b570cf32b2ba8ffa9ecc5480425c7da9f93f9773fbb3f9ec5f603b369b6225f4329a1b1f34d0ef30b2c9730f25ff9cff31315ed0a89a600df8e53bd54bf')

prepare() {
  cd ffmpeg-${pkgver}

  patch -Np1 -i "${srcdir}"/0001-libavutil-clean-up-unused-FF_SYMVER-macro.patch
  patch -Np1 -i "${srcdir}"/0001-ffbuild-libversion.sh-add-shebang.patch
}

build() {
  cd ffmpeg-${pkgver}

  if [[ $CARCH = i686 ]]; then
    EXTRAOPTS="--disable-lto --enable-pic --disable-asm"
  else
    EXTRAOPTS="--enable-lto --enable-asm"
  fi

  ./configure \
    --prefix=/usr \
    --disable-debug \
    --disable-static \
    --disable-stripping \
    --disable-librtmp \
    --disable-amf \
    --disable-avisynth \
    --disable-cuda-llvm \
    --enable-fontconfig \
    --enable-gmp \
    --enable-gnutls \
    --enable-gpl \
    --enable-ladspa \
    --disable-libaom \
    --enable-libass \
    --disable-libbluray \
    --disable-libdav1d \
    --enable-libdrm \
    --enable-libfreetype \
    --enable-libfribidi \
    --disable-libgsm \
    --disable-libiec61883 \
    --enable-libjack \
    --disable-libmfx \
    --enable-libmodplug \
    --enable-libmp3lame \
    --disable-libopencore_amrnb \
    --disable-libopencore_amrwb \
    --enable-libopenjpeg \
    --enable-libopus \
    --disable-libpulse \
    --disable-librav1e \
    --enable-librsvg \
    --enable-libsoxr \
    --enable-libspeex \
    --disable-libsrt \
    --disable-libssh \
    --disable-libsvtav1 \
    --enable-libtheora \
    --enable-libv4l2 \
    --enable-libvidstab \
    --disable-libvmaf \
    --enable-libvorbis \
    --disable-libvpx \
    --disable-libwebp \
    --enable-libx264 \
    --disable-libx265 \
    --enable-libxcb \
    --enable-libxml2 \
    --enable-libxvid \
    --enable-libzimg \
    --disable-nvdec \
    --disable-nvenc \
    --disable-vaapi \
    --disable-vdpau \
    --enable-shared \
    --enable-version3 \
    $EXTRAOPTS \

  make
  make tools/qt-faststart
  make doc/ff{mpeg,play}.1
}

package() {
  cd ffmpeg-${pkgver}

  make DESTDIR="${pkgdir}" install install-man
  install -Dm 755 tools/qt-faststart "${pkgdir}"/usr/bin/
  install -Dm 644 COPYING.GPLv3 "${pkgdir}"/usr/share/licenses/${pkgname}/COPYING.GPLv3
}
