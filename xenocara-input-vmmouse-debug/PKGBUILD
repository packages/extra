# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

# Based on xf86-input-vmmouse package

pkgname=xenocara-input-vmmouse-debug
_openbsdver=6.9
pkgver=13.1.0
pkgrel=4
pkgdesc="Xenocara VMWare Mouse input driver"
arch=(i686 x86_64)
license=('X11')
url="https://www.xenocara.org"
depends=('libeudev')
makedepends=('xenocara-server-devel' 'X-ABI-XINPUT_VERSION=24.1' 'xenocara-proto' 'xenocara-util-macros')
provides=('xf86-input-vmmouse')
conflicts=('xf86-input-vmmouse' 'xenocara-server<1.20' 'X-ABI-XINPUT_VERSION<24.1' 'X-ABI-XINPUT_VERSION>=25')
replaces=('xf86-input-vmmouse')
groups=('xenocara-drivers-debug' 'xorg-drivers-debug')
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/driver/xf86-input-vmmouse-$pkgver.tar.lz{,.sig})
options=(!strip) # It's required for debug packages
sha512sums=('f7ab83577762c951cfcfffa2c406a5a3c170957d467dec802185b75af9cb2cec9b09dde917ed85291274aa1b00b99c01c557ba2ca2537a2c15df407fd19d7472'
            'SKIP')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/driver/xf86-input-vmmouse"
  autoreconf -vfi
}

build() {
  cd "xenocara-$_openbsdver/driver/xf86-input-vmmouse"
  ./configure --prefix=/usr \
      --with-udev-rules-dir=/lib/udev/rules.d

  # It's required for debug packages
  export CFLAGS=${CFLAGS/-O2/-O0 -g3}
  export CXXFLAGS=${CXXFLAGS/-O2/-O0 -g3}

  make
}

package() {
  cd "xenocara-$_openbsdver/driver/xf86-input-vmmouse"
  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
  rm -rfv $pkgdir/usr/{lib,share}/hal
}
