# Maintainer (Arch): Jaroslav Lichtblau <svetlemodry@archlinux.org>
# Contributor (Arch): Giovanni Scafora <giovanni@archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=lbreakout2
pkgver=2.6.5
_debver=2.6.5
_debrel=2
pkgrel=1
pkgdesc="A breakout game with nice effects, graphics, and sounds"
arch=('i686' 'x86_64')
url="http://lgames.sourceforge.net"
license=('GPL-2')
depends=('sdl_mixer' 'libpng')
makedepends=('quilt')
backup=('var/games/lbreakout2/lbreakout2.hscr')
groups=('games')
source=(https://downloads.sourceforge.net/lgames/${pkgname}-${pkgver/_/-}.tar.gz
        https://deb.debian.org/debian/pool/main/l/lbreakout2/lbreakout2_${_debver}-${_debrel}.debian.tar.xz
        lbreakout2.desktop
        lbreakout2.png)
sha512sums=('45c54bc9401131c96eba5fdcc08ca1324904fb50d3967acf7f29034045cbcd4c1d0b65f38eb33d8aace4cfe35f2a2e7a6c2319e4a6d6a1b41274dbe293a1747b'
            'fa85b83141118c6bf0d52a69f9c8f538147e81638d9824fe2ec4b2e91de8f1f2a19284436bfc17a260616ad9501b9811f405b2817634b0fafa0d3834a94a1bd8'
            '3f626522ed6ae48bc14f94e02ccb498cbc6154c2261ab7148fc8d640fd88bfc550f83f5bdd834771c8235a5d71afdda804aecae5e5a912b68f423bdc50e23968'
            '10894aac98b87eb586d7ee5dd9ca935742d091426a293d504e6ea7d1fa7781b855df728345531d883eaf3dffdf2b983fa21a544494f11f5472d16cb3a511472a')

prepare() {
  cd ${pkgname}-${pkgver/_/-}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd ${pkgname}-${pkgver/_/-}

  ./configure \
    --prefix=/usr \
    --bindir=/usr/games \
    --datadir=/usr/share/games \
    --localstatedir=/var/games/lbreakout2 
  make
}

package() {
  cd ${pkgname}-${pkgver/_/-}

  make DESTDIR="${pkgdir}"/ doc_dir=/usr/share/doc install

  mv "${pkgdir}/usr/share/games/locale" "${pkgdir}/usr/share"
  rm -rf "${pkgdir}/usr/share/games/"{applications,icons}
  
  chmod -R 755 "${pkgdir}"/usr
  chown root:games "${pkgdir}"/var/games
  chmod 775 "${pkgdir}"/var/games
  chown root:games "${pkgdir}"/var/games/lbreakout2
  chmod 755 "${pkgdir}"/var/games/lbreakout2
  chown root:games "${pkgdir}"/var/games/lbreakout2/lbreakout2.hscr
  chmod 775 "${pkgdir}"/var/games/lbreakout2/lbreakout2.hscr

  install -Dm644 "${srcdir}"/lbreakout2.png "${pkgdir}"/usr/share/pixmaps/lbreakout2.png
  install -Dm644 "${srcdir}"/lbreakout2.desktop "${pkgdir}"/usr/share/applications/lbreakout2.desktop
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
