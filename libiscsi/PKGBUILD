# Maintainer: Tobias Powalowski <tpowa@archlinux.org>
# Contributor: Patryk Kowalczyk <patryk@kowalczyk.ws>

pkgname=libiscsi
pkgver=1.19.0
_debver=1.19.0
_debrel=3
pkgrel=1
makedepends=('quilt')
depends=('glibc' 'popt' 'libgcrypt')
pkgdesc="Clientside library to implement the iSCSI protocol"
arch=(i686 x86_64)
url="https://github.com/sahlberg/libiscsi"
license=('LGPL-2.1')
source=(https://github.com/sahlberg/${pkgname}/archive/refs/tags/${pkgver}.tar.gz
        https://deb.debian.org/debian/pool/main/libi/libiscsi/libiscsi_$_debver-$_debrel.debian.tar.xz)
sha512sums=('2c0e70b7ecf66204524e57edf557a014b5141a8819e5d663b6e50cb2e756fd634ae1db0b1ca6915444d80a9babd54a82619bdd98726d16d4eeaf7b3c89cf3d53'
            'e5313614a4c80cbfb962c490653cb9c936e37d4a031fed6aba8ed793e01511fea397b9b823a9059f8c56077b2a59dd4772b45115541a15dc2cd8a72aab9cc685')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/soversion.diff || true

    quilt push -av
  fi
  ./autogen.sh
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  # Disabled werror due to GCC 8 warnings: https://github.com/sahlberg/libiscsi/issues/266
  ./configure --prefix=/usr --disable-static --libdir=/usr/lib --disable-werror 
  make
}

check() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make check
}

package () {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install

  for i in COPYING LICENCE-LGPL-2.1.txt; do
    install -Dm644 $i -t "${pkgdir}/usr/share/licenses/${pkgname}"
  done
}
