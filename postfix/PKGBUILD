# Contributor (Arch): Jeff Brodnax <tullyarcher@bellsouth.net>
# Contributor (Arch): Paul Mattal <paul@archlinux.org>
# Maintainer (Arch): Gaetan Bisson <bisson@archlinux.org>
# Contributor (Artix): artoo <artoo@cromnix.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Jesús E.
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=postfix
pkgver=3.5.13
_debver=$pkgver
_debrel=0
pkgrel=3
pkgdesc="Fast, easy to administer, secure mail server"
license=('IPL-1.0')
arch=('i686' 'x86_64')
url='http://www.postfix.org/'
depends=('libressl' 'pcre' 'icu' 'libsasl' 'libldap' 'db' 'libnsl' 'postgresql-libs' 'sqlite' 'tinycdb')
makedepends=('quilt')
optdepends=('logger: message logging support')
source=("https://archive.mgm51.com/mirrors/postfix-source/official/${pkgname}-${pkgver}.tar.gz"
        "https://repo.hyperbola.info:50000/sources/${pkgname}/postfix_${_debver}-${_debrel}+deb11u1.debian.tar.xz"{,.sig}
        "aliases.patch"
        "libressl.patch"
        "postfix.initd"
        "postfix.run")
sha512sums=('47aee05f92fca387533132f5227b82fbab4700f9bc3512a17abee1c3aeb2e54ce5a964189774c14489a99c5b53ef606d8d3f5aac44f32739265c1be47bd4a24c'
            'bac745840ce5cffe9243aba71df4506e54910377025aeeb29db9c6b32ff88cee96caeace110c1eb69859c4888865929b73f11416227abf08c546bcc48945f7c4'
            'SKIP'
            '630cd85be341fbd82c5202fdfd6c3b1df906c0b77cff43ae44f665451d7ddc99e6a20a755ed970fff60d93ccbb3139ea28495f0b281232e8a97218319156a57f'
            '1acedf7558b39f78943368c88ae551988a228ded81d37fd58160b6cbe665e1cc94a2abebad80fb0f15ca4a9cbbb8a5e0d880e36103a400789586095855a96b0c'
            '2ad1b053bbb51d4f480dbf5a01252e8e99a82ec14a75ff49336b2b190ea55bc02400542b109fa0cc4ebfe6b42eaabbc45f70f2ea581f1eb9c4189c439b825592'
            '8a7afca85aa864eeff052751a72cd81e094db188c689f5361d6acbf429bdbb2fbdcec95a743de9054dbe13e101b66e5d1047e6393bd3bafcbd40a4cffaa30563')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

backup=('etc/postfix/'{access,aliases,canonical,generic,header_checks,main.cf,master.cf,relocated,transport,virtual})

conflicts=('smtp-server' 'smtp-forwarder' 'postfix-tools')
provides=('smtp-server' 'smtp-forwarder' 'postfix-tools')
replaces=('postfix-tools')

install=postfix.install

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/02_kfreebsd_support.diff || true
    rm -v debian/patches/03_ldap3_by_default.diff || true
    rm -v debian/patches/05_debian_defaults.diff || true
    rm -v debian/patches/05_debian_manpage_differences.diff || true
    rm -v debian/patches/05_debian_readme_differences.diff || true
    rm -v debian/patches/06_debian_paths.diff || true
    rm -v debian/patches/07_sasl_config.diff || true
    rm -v debian/patches/10_openssl_version_check.diff || true
    rm -v debian/patches/30_shared_libs.diff || true

    quilt push -av
  fi

  patch -Np1 -i "${srcdir}/libressl.patch"
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  CCARGS='
          -DUSE_SASL_AUTH -DUSE_CYRUS_SASL -I/usr/include/sasl
          -DHAS_LDAP
          -DUSE_TLS
          -DHAS_PGSQL -I/usr/include/postgresql
          -DHAS_SQLITE
          -DHAS_CDB
          -DDEF_COMMAND_DIR=\"/usr/sbin\"
          -DDEF_DAEMON_DIR=\"/usr/libexec/postfix\"
          -DDEF_SENDMAIL_PATH=\"/usr/sbin/sendmail\"
          -DDEF_README_DIR=\"/usr/share/doc/postfix\"
          -DDEF_MANPAGE_DIR=\"/usr/share/man\"
         '
  AUXLIBS='
          -lsasl2
          -lldap -llber
          -lssl -lcrypto
          -lz -lm
          -lpq
          -lsqlite3 -lpthread
          -lcdb
          '
  make makefiles \
       DEBUG='' \
       pie=yes \
       shared=yes \
       CCARGS="${CCARGS//$'\n'/}" \
       AUXLIBS="${AUXLIBS//$'\n'/}" \
       OPT="${CFLAGS} ${LDFLAGS}"

  make
}

package() {
  cd "${srcdir}/postfix-${pkgver}"

  sed 's:bin/postconf -dhx:bin/postconf -c conf -dhx:g' -i postfix-install
  LD_LIBRARY_PATH=lib:$LD_LIBRARY_PATH \
                 sh postfix-install -non-interactive install_root="${pkgdir}"

  install -Dm644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"
  install -Dm755 "${srcdir}/${pkgname}.initd" "${pkgdir}/etc/init.d/${pkgname}"
  install -Dm755 "${srcdir}/${pkgname}.run" "${pkgdir}/etc/sv/${pkgname}/run"

  for name in posttls-finger {smtp,qmqp}-{sink,source}; do
    install -Dm644 "man/man1/${name}.1" "${pkgdir}/usr/share/man/man1/${name}.1"
    install -Dm755 "bin/${name}" "${pkgdir}/usr/sbin/${name}"
  done

  # now set permissions right
  sed -r \
      -e '/override=1/d' \
      -e '/absolute path name/d' \
      -e 's/\$POSTCONF -/$POSTCONF -c .\/etc\/postfix -/g' \
      -e '/(if \[|chmod|chown|find|mkdir|test)/s/\$path/.\/$path/g' \
      libexec/post-install > ../hyperbola-post-install
  cd "${pkgdir}"
  LD_LIBRARY_PATH=usr/lib/postfix:$LD_LIBRARY_PATH \
                 sh "${srcdir}/hyperbola-post-install" \
                 command_directory=usr/sbin \
                 config_directory=etc/postfix \
                 meta_directory=etc/postfix \
                 setgid_group=75 \
                 mail_owner=73 \
                 set-permissions

  patch --no-backup-if-mismatch -p1 -i "${srcdir}"/aliases.patch
  sed 's/^\(\$manpage[^:]*\):/\1.gz:/' -i "etc/postfix/postfix-files"
}
