# Maintainer: Levente Polyak <anthraxx[at]archlinux[dot]org>

pkgname=python-recommonmark
pkgver=0.6.0
_debver=0.6.0+ds
_debrel=1
pkgrel=0
pkgdesc='Markdown parser for docutils'
url='https://recommonmark.readthedocs.io/'
arch=('any')
license=('Expat')
depends=('python-docutils' 'python-commonmark' 'python-setuptools' 'python-sphinx')
makedepends=('quilt')
#checkdepends=('python-pytest')
source=(${pkgname}-${pkgver}.tar.gz::https://github.com/readthedocs/recommonmark/archive/${pkgver}.tar.gz
        https://deb.debian.org/debian/pool/main/r/recommonmark/recommonmark_$_debver-$_debrel.debian.tar.xz
        recommonmark-disable-math-on-builds.patch
        recommonmark-sphinx-2.patch)
sha512sums=('44005b3fd0052cd8d4fce8a64f9d66a1ac75dc3041a28c115e922254956b2143296cbfbc0a2396b9f95691145645c4242e3be68e695b2c62ae37964014511679'
            '59fd43653e2ccacac53b36571a6ff1e1b68862e0d024b543038ac39cfd0273c33ef501492efc849d985085a153f07256f32fddb736408a9e701c7fdbf3a5d2a0'
            '66ef07ac21643b913c8ee551c7c71753fcb6442712e4fb11b84ea5f34265715471759b2c5a1badc1e299ca6aa831713b12284144801616349b2db61738607e8a'
            '317b7139819bb183184e0a5abf4f74b955746779c841cf3b9e7e8032cc23ac3ce5f54a1ebd2d4c012038041c58ab849a09271d806f9fc3c5c0dadd1ad92eddae')

prepare() {
  cd recommonmark-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  else
    patch -p1 -i "${srcdir}/recommonmark-sphinx-2.patch" # Fix tests with Sphinx 2
  fi
  patch -p1 -i "${srcdir}/recommonmark-disable-math-on-builds.patch"
}

build() {
  cd recommonmark-${pkgver}
  python setup.py build
  make -j1 -C docs text man SPHINXBUILD=sphinx-build
}

#check() {
#  cd recommonmark-${pkgver}
#  py.test
#}

package() {
  cd recommonmark-${pkgver}
  python setup.py install --root="${pkgdir}" --skip-build -O1
  install -Dm 644 license.md -t "${pkgdir}/usr/share/licenses/${pkgname}"
  install -Dm 644 README.md CHANGELOG.md -t "${pkgdir}/usr/share/doc/${pkgname}"
  install -Dm 644 docs/_build/text/*.txt -t "${pkgdir}/usr/share/doc/${pkgname}"
  install -Dm 644 docs/_build/man/recommonmark.1 "${pkgdir}/usr/share/man/man1/recommonmark.1"
}

# vim: ts=2 sw=2 et:
