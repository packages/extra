# Maintainer (Arch): Antonio Rojas <arojas@archlinux.org>
# Contributor (Arch): Tom Gundersen <teg@jklm.no>
# Contributor (Arch): Jan de Groot <jgc@archlinux.org>
# Contributor (Arch): Sean Middleditch <elanthis@awesomeplay.com>
# Contributor (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=docbook-xsl
pkgver=1.79.2
_debver=$pkgver
_debrel=1
pkgrel=4
pkgdesc="XML stylesheets for Docbook-xml transformations"
url='https://docbook.org/'
arch=('any')
license=('Expat')
depends=('libxml2' 'libxslt' 'docbook-xml')
makedepends=('quilt')
install=$pkgname.install
source=(https://github.com/docbook/xslt10-stylesheets/releases/download/release%2F$pkgver/docbook-xsl{,-nons}-$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/d/docbook-xsl/docbook-xsl_${_debver}+dfsg-${_debrel}.debian.tar.xz)
sha512sums=('cd375e7a2339bcd9d24936b370c4371b351cab7c8a79044e785d0300dab75bed5a5c9b9076d4fba32d39b22535edfc6f449a7b1086139ad8bf54eab844b9434e'
            '8764c4176abd9662f9005e7d76b1cfcceb18882404c11256a8c19bf3b44cbe4e5ff9c73b1902c4d6611708cc9ae483fe21e6185a19a3b9aeb2abcb9c19e73862'
            'ff4514570b1a5cb034ec9d0b333d74eb674861407d3be9df3dfd075597b3eadec7c8123e090c3c0b1a784da9d53b47a6102ffc8e7d8ab44779296c7184a4f2d0')

prepare() {
  mv $pkgname{-$pkgver,}
  mv $pkgname-nons{-$pkgver,}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    # Doesn't apply and seems unimportant
    rm -v debian/patches/0005-catalog.xml-Compatibility-with-1.79.1-or-earlier.patch || true
    rm -v debian/patches/681116_multiarch_manpage_template.patch || true

    quilt push -av
  fi

  cd $pkgname-nons
  patch -Np2 -i ${srcdir}/debian/patches/765567_non-recursive_string_subst.patch
}

package() {
  local pkgroot ns dir
  for ns in -nons ''; do
    pkgroot="$pkgdir/usr/share/xml/docbook/xsl-stylesheets-$pkgver$ns"
    dir=$pkgname$ns

    install -Dt "$pkgroot" -m644 $dir/VERSION{,.xsl}

    (
      shopt -s nullglob  # ignore missing files
      for fn in assembly common eclipse epub epub3 fo highlighting html \
        htmlhelp javahelp lib manpages params profiling roundtrip template \
        website xhtml xhtml-1_1 xhtml5
      do
        install -Dt "$pkgroot/$fn" -m644 $dir/$fn/*.{xml,xsl,dtd,ent}
      done
    )
  done

  install -d "$pkgdir/etc/xml"
  install -Dt "$pkgdir/usr/share/licenses/$pkgname" -m644 $dir/COPYING
}