# Maintainer (Arch): Alexander F. Rødseth <xyproto@archlinux.org>
# Contributor (Arch): Mattias Andrée <`base64 -d`(bWFhbmRyZWUK)@member.fsf.org>
# Contributor (Arch): Jonathan Frawley <jonathanfrawley@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=xcftools
pkgver=1.0.7
pkgrel=1
pkgdesc="Command line tools for use with the free and libre XCF image format (includes xcf2png)"
url='http://henning.makholm.net/software'
arch=('i686' 'x86_64')
license=('Public-Domain')
depends=('libpng' 'perl')
makedepends=('gettext-tiny')
source=("https://security.debian.org/debian-security/pool/updates/main/x/xcftools/xcftools_${pkgver}.orig.tar.gz"
        "xcftools.patch"
        "security.patch"
        "fix-build.patch")
sha512sums=('88af4791e18650562db259bd96bfd122364c21b7ea7a5692d4679e619667bdbcc179040a1d912c8fd623bc2d2735461da237ccde4646553b15d6072cc4493203'
            'd85529e77666b5453f61c6e48eaf5569b05ae6ab9eab5e549062385dcae67981a10efbbe4a69b9dc344daea34c5b424d6b4ac44430cf79268433da4be676bd10'
            'af952c9b1ac8b9e0a62d7329de56d63e8a3a4719adc3ac7d9c1d45fea5473705a3e0e4d883a6c9f0291a0554b1d72bcb87c45167f50e029469a81e9eceb44533'
            'cbfabf241a074bb08b2c1ec445dd79e17b8216a55def3953100c133c2e6e8b4e52e3d873f250a95488d884b5d4c4ed4b2548717d95d6b988e83fa98073f475f1')

prepare() {
  grep -A16 License "$pkgname-$pkgver/README" > LICENSE
  cd $pkgname-$pkgver
  patch -i "$srcdir/xcftools.patch"
  patch -i "$srcdir/security.patch"
  patch -i "$srcdir/fix-build.patch"
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr
  make
}

package() {
  make DESTDIR="$pkgdir" -C $pkgname-$pkgver install
  install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}
