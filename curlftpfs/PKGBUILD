# Maintainer (Arch): Chris Brannon <cmbrannon79@gmail.com>
# Contributor (Arch): Allan McRae <mcrae_allan@hotmail.com>
# Contributor (Arch): Philip Nilsson <leffeman@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=curlftpfs
pkgver=0.9.2
_debver=0.9.2
_debrel=9
pkgrel=1
pkgdesc='A filesystem for acessing FTP hosts based on FUSE and libcurl.'
url='http://curlftpfs.sourceforge.net/'
license=('GPL-2')
arch=('i686' 'x86_64')
depends=('curl' 'fuse2' 'glib2')
makedepends=('quilt')
source=(https://downloads.sourceforge.net/sourceforge/curlftpfs/$pkgname-$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/c/curlftpfs/curlftpfs_$_debver-$_debrel.debian.tar.xz)
sha512sums=('df07c418d175f766c89525017fc56e79726061eee0c3a6607ded0e1bf24f64f1475ba0e546157b65892194e3c4414c120822bf8fb175437e68366f82de216067'
            '651939796a518e13cbc04e2ba276b57a58970f8d53bbba8b363b7d5bacb55734a21f78490484b2abec5eb707f4deb603938e6d0a5263a0d536a41c475c2684af')

prepare() {
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
