# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Contributor (Arch): Ben <ben@benmazer.net>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=gmime3
pkgver=3.2.7
pkgrel=1
pkgdesc="A C/C++ MIME creation and parser library with support for S/MIME, PGP, and Unix mbox spools"
arch=('i686' 'x86_64')
license=('LGPL-2.1')
url='https://github.com/jstedfast/gmime'
depends=('glib2' 'gpgme' 'zlib' 'libidn2')
makedepends=('gobject-introspection' 'vala')
source=("${pkgname}-${pkgver}.tar.xz::https://deb.debian.org/debian/pool/main/g/gmime/gmime_${pkgver}.orig.tar.xz")
sha512sums=('2c8fecd6eb74c9e956c419ce111bef6280916ea3756a534b6b800a273f0fff260717cda7fefd4bd4c6ddb27973112ec6e73fa971fa74bb9aa04cfe587d45f10b')

build() {
  cd gmime-$pkgver
  ./configure \
    --prefix=/usr \
    --program-prefix=$pkgname \
    --enable-smime \
    --disable-gtk-doc \
    --disable-static
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

package() {
  cd gmime-$pkgver
  make DESTDIR="$pkgdir" install

  # cleanup
  rm -rf "$pkgdir/usr/share/gtk-doc"

  # license
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}
