# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

# Based on pinentry package

pkgbase=pinentry-extra
_realpkgname=pinentry
pkgname=('pinentry-curses' 'pinentry-gtk' 'pinentry-gtk2' 'pinentry-qt' 'pinentry-fltk')
pkgver=1.1.0
_debver=$pkgver
_debrel=4
pkgrel=8
url='https://gnupg.org/related_software/pinentry/'
arch=('i686' 'x86_64')
license=('GPL-2')
makedepends=('libassuan' 'libcap' 'gcr' 'gtk2' 'qt-base' 'fltk' 'quilt')
source=("https://gnupg.org/ftp/gcrypt/pinentry/pinentry-${pkgver}.tar.bz2"
        "https://deb.debian.org/debian/pool/main/p/pinentry/pinentry_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('5012672925bcb5f683358c259e55e4b87c67cf063ad52c759308933733025c33f7ce08e5b8019ffc101cbf7ef30499040ef2fd34a7611698e65e1593f80948cd'
            '0797c9a317a31d2a1b9c6dd5f89c1af41c8c126f33c5606be26683f8b1870f50b0e2875f601a38a0a36d351a1f0b772dbc499bb078fd37a39c3b1972098dd66d')

prepare() {
  cd "${srcdir}/pinentry-${pkgver}"
  if [ ${pkgver%.*} = ${_debver%.*} ]; then
    # Debian patches
    export QUILT_PATCHES='debian/patches'
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "${srcdir}/pinentry-${pkgver}"
  ./configure \
    --prefix=/usr \
    --enable-pinentry-curses \
    --enable-pinentry-gtk2 \
    --enable-pinentry-gnome3 \
    --enable-pinentry-qt \
    --enable-pinentry-qt5 \
    --enable-pinentry-fltk \
    --enable-fallback-curses \
    --disable-pinentry-tty \
    --disable-pinentry-tqt \
    --disable-pinentry-emacs \
    --disable-inside-emacs \
    --disable-libsecret
  make V=1
}

package_pinentry-curses() {
  pkgdesc='Curses-based PIN or pass-phrase entry dialog for GnuPG'
  depends=('libassuan' 'libcap' 'ncurses')
  groups=('pinentry-extra')

  cd "${srcdir}/${_realpkgname}-${pkgver}"

  install -Dm755 curses/${pkgname} -t "${pkgdir}/usr/bin"
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
}

package_pinentry-gtk() {
  pkgdesc='GTK-based PIN or pass-phrase entry dialog for GnuPG'
  depends=('libassuan' 'libcap' 'gcr')
  groups=('pinentry-extra')

  cd "${srcdir}/${_realpkgname}-${pkgver}"

  install -Dm755 gnome3/${_realpkgname}-gnome3 "${pkgdir}/usr/bin/${pkgname}"
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
}

package_pinentry-gtk2() {
  pkgdesc='GTK+2-based PIN or pass-phrase entry dialog for GnuPG'
  depends=('libassuan' 'libcap' 'gtk')
  groups=('pinentry-extra')

  cd "${srcdir}/${_realpkgname}-${pkgver}"

  install -Dm755 gtk+-2/${_realpkgname}-gtk-2 "${pkgdir}/usr/bin/${pkgname}"
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
}

package_pinentry-qt() {
  pkgdesc='Qt-based PIN or pass-phrase entry dialog for GnuPG'
  depends=('libassuan' 'libcap' 'qt-base')
  groups=('pinentry-extra')

  cd "${srcdir}/${_realpkgname}-${pkgver}"

  install -Dm755 qt/${pkgname} -t "${pkgdir}/usr/bin"
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
}

package_pinentry-fltk() {
  pkgdesc='FLTK-based PIN or pass-phrase entry dialog for GnuPG'
  depends=('libassuan' 'libcap' 'fltk')
  groups=('pinentry-extra')

  cd "${srcdir}/${_realpkgname}-${pkgver}"

  install -Dm755 fltk/${pkgname} -t "${pkgdir}/usr/bin"
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
}