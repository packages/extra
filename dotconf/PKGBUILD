# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Contributor (Arch): Chris Brannon <cmbrannon@cox.net>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=dotconf
pkgver=1.3
_debver=$pkgver
_debrel=0.3
pkgrel=1
pkgdesc="A C library for parsing configuration files"
arch=('i686' 'x86_64')
url='https://github.com/williamh/dotconf'
license=('LGPL-2.1')
depends=('glibc')
makedepends=('quilt')
source=("https://deb.debian.org/debian/pool/main/d/dotconf/dotconf_$pkgver.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/d/dotconf/dotconf_$_debver-$_debrel.debian.tar.xz")
sha512sums=('6a8fa52ea498def8387568c4033fd31b6f27a89b0dc3ce7641d389e7db8dea5c0c402fdbdd9fc7a63abd6b052e5c82f6e21357f980afd5ed89cd8752a09fe8ac'
            '47854ca9dbfab2756f843443f16b8685cd5605e8ae6d03834457e63853d466ba0e11428f772d07421916a2669baa8347f34a21ae60de55048aabf0e3005588e2')

prepare() {
  mv "williamh-${pkgname}-4cd7b3a" "$pkgname-$pkgver"
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  autoreconf -i
  ./configure --prefix=/usr
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
