#!/sbin/openrc-run
# Copyright 2023 Hyperbola Project
# Distributed under the terms of the GNU General Public License v2

depend() {
	need net localmount
	after logger firewall postgresql
}

checkconfig() {
	if ! grep -q -E '^use_keyd=*(true|yes|1)$' /etc/onak.ini; then
		eerror "You need a to enable keyd-support within your configuration"
		return 1
	fi
}

start_pre() {
	checkconfig || return 1

	# create needed directory if necessary
	if [ ! -d  ${ONAKKEYD_SOCKDIR} ] ; then
		checkpath -q -d -o ${ONAKKEYD_DAEMON_UID}:${ONAKKEYD_DAEMON_GID} -m 0775 ${ONAKKEYD_SOCKDIR} || return 1
	fi
}

start() {
	ebegin "Starting onak-keyserver"
	start-stop-daemon \
		--start \
		--user "${ONAKKEYD_DAEMON_UID}:${ONAKKEYD_DAEMON_GID}" \
		--exec ${ONAKKEYD_DAEMON_CMD}
	eend $?
}

stop() {
	ebegin "Stopping onak-keyserver"
	keydctl quit
	eend $?
}