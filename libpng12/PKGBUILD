# Maintainer (Arch): Maxime Gauduin <alucryd@archlinux.org>
# Contributor (Arch): Arthur Zamarin <arthurzam@gmail.com>
# Contributor (Arch): trya <tryagainprod@gmail.com>
# Contributor (Arch): Jan de Groot <jgc@archlinux.org>
# Contributor (Arch): dorphell <dorphell@archlinux.org>
# Contributor (Arch): Travis Willard <travis@archlinux.org>
# Contributor (Arch): Douglas Soares de Andrade <douglas@archlinux.org>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=libpng12
pkgver=1.2.59
pkgrel=1
pkgdesc="A collection of routines used to create PNG format graphics files"
arch=('i686' 'x86_64')
url='http://www.libpng.org/pub/png/libpng.html'
license=('custom:libpng')
depends=('glibc' 'zlib')
source=("https://sourceforge.net/projects/libpng/files/libpng-${pkgver}.tar.xz"
        "https://sourceforge.net/projects/libpng-apng/files/libpng12/${pkgver}/libpng-${pkgver}-apng.patch.gz")
sha512sums=('bfdc51eca72a76697f1396611a08aa4ce6a169837197699c55d845fdef17850e8f7665b7b81ba815c277453737f12eeb41409ff9c7eca1ac0c0d134c44492a6e'
            '5e405e8c444956b5f30cb2bc8c55f8050c038287afa4b478d237a02084428c9ecda60bc0015c4ca915bf9784492ad3d854a627f165fa62040817cd19f32234c0')

prepare() {
  cd libpng-${pkgver}

  patch -Np1 -i ../libpng-${pkgver}-apng.patch

  libtoolize --force --copy
  aclocal
  autoconf
  automake --add-missing
}

build() {
  cd libpng-${pkgver}

  ./configure \
    --prefix='/usr'
  make
}

package() {
  cd libpng-${pkgver}

  make DESTDIR="${pkgdir}" install
  rm -rf "${pkgdir}"/usr/{bin,include/*.h,lib/{libpng.{a,so},pkgconfig},share}
  install -Dm 644 LICENSE -t "${pkgdir}"/usr/share/licenses/$pkgname/
}
