# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=qt-x11extras
_qtver=5.15.2
pkgver=${_qtver/-/}
pkgrel=1
arch=('i686' 'x86_64')
url='https://www.qt.io'
license=('LGPL-3')
pkgdesc='Provides platform-specific APIs for X11'
depends=('qt-base')
groups=('qt')
replaces=('qt5-x11extras')
conflicts=('qt5-x11extras')
provides=('qt5-x11extras')
_pkgfqn="${pkgname/-/}-everywhere-src-${_qtver}"
source=("https://download.qt.io/official_releases/qt/${pkgver%.*}/${_qtver}/submodules/${_pkgfqn}.tar.xz")
sha512sums=('beaefc865299f4e7c637baa96eb3a69adbe1a41fc7735b46cfec0768c9243391600e69078630fffb4aceba106b7512fd802e745abc38ddab5253233301c93ed9')

prepare() {
  mkdir -p build
}

build() {
  cd build

  qmake ../${_pkgfqn}
  make
}

package() {
  cd build
  make INSTALL_ROOT="$pkgdir" install

  # Drop QMAKE_PRL_BUILD_DIR because reference the build dir
  find "$pkgdir/usr/lib" -type f -name '*.prl' \
    -exec sed -i -e '/^QMAKE_PRL_BUILD_DIR/d' {} \;

  # Install license
  install -D -m644 "${srcdir}"/${_pkgfqn}/LICENSE.LGPL3 "${pkgdir}"/usr/share/licenses/${pkgname}/LICENSE.LGPL3
}
