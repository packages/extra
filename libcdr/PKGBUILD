# Maintainer (Arch): AndyRTR <andyrtr@archlinux.org>
# Contributor (Arch): megadriver <megadriver at gmx dot com>
# Contributor (Arch): Luca Bennati <lucak3 AT gmail DOT com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=libcdr
pkgver=0.1.6
_debver=$pkgver
_debrel=2
pkgrel=2
pkgdesc="Vector file format importer library"
arch=('i686' 'x86_64')
url='https://wiki.documentfoundation.org/DLP/Libraries/libcdr'
license=('MPL-2.0')
depends=('libwpd' 'lcms2' 'icu' 'librevenge')
makedepends=('libwpg' 'boost' 'doxygen' 'cppunit' 'quilt')
source=(https://dev-www.libreoffice.org/src/$pkgname/$pkgname-$pkgver.tar.xz
        https://deb.debian.org/debian/pool/main/libc/libcdr/libcdr_$_debver-$_debrel.debian.tar.xz)
sha512sums=('629d55da71c7333f41f60a32e2880deffcf80088096af1bbc8c572b80ef21d851102fdebce56f77245ed60822ca98e02c0867b192abef496a2313fde54a97bb6'
            '0eac391edd020590b906ebed6466b8b6164fb77bcec506b416022b3336a1a0ddbe51c81ff2335a782ebf827e086e62fbe02ec233d75062f76d28c41678ef1ed6')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING.MPL -t "${pkgdir}/usr/share/licenses/$pkgname"
}
