# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Contributor (Arch): Pierre Schmitz <pierre@archlinux.de>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=cmake
pkgver=3.25.1
_debver=$pkgver
_debrel=1
pkgrel=5
pkgdesc="A cross-platform free software make system"
arch=('i686' 'x86_64')
url='https://www.cmake.org/'
license=('Modified-BSD')
depends=('curl' 'libarchive' 'shared-mime-info' 'jsoncpp' 'libuv' 'rhash')
makedepends=('python-sphinx' 'quilt')
source=("https://www.cmake.org/files/v${pkgver%.*}/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/c/cmake/cmake_${_debver}-${_debrel}~bpo11+1.debian.tar.xz"
        "byacc.patch")
sha512sums=('ec4203cac569e3c340bf6535d193d9ccff9c4e4d59a7a7ae5b9156172f647d9f9212bdc37b3c12cbd676b1351b9a64364c563aaa968a2f41e0f402410ed78d57'
            'e15a12081df435739b57087769f98bf5876280c20b6dfd929e456b69218672ea1060d3a2f07e3308052a85cab33fb46b8300ea7500b3bf865a9b7a8cc73c27aa'
            'd137a8261dac9bd0cf123427a61581f5b31982a31a30e488c6852e3781cc9e3672c32accfc1a7310bcc3e8c9d43f0e7edb259b4e130b416d4a5d00d23e133f1a')

prepare() {
  cd ${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  patch -p1 -i "$srcdir"/byacc.patch
}

build() {
  cd ${pkgname}-${pkgver}
  ./bootstrap --prefix=/usr \
    --mandir=/share/man \
    --docdir=/share/doc/cmake \
    --datadir=/share/cmake \
    --sphinx-man \
    --sphinx-html \
    --system-libs \
    --parallel=$(/usr/bin/getconf _NPROCESSORS_ONLN)

  make
}

package() {
  cd ${pkgname}-${pkgver}

  make DESTDIR="${pkgdir}" install

  rm -r "$pkgdir"/usr/share/doc/cmake/html/_sources
  rm -rf "$pkgdir"/usr/share/emacs
  install -Dm644 Copyright.txt -t "${pkgdir}"/usr/share/licenses/${pkgname}
}
