# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Maintainer (Arch): Anatol Pomozov <anatol.pomozov@gmail.com>
# Contributor (Arch): Allan McRae <allan@archlinux.org>
# Contributor (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgbase=gdb
pkgname=(gdb gdb-common)
pkgver=10.1
_debver=10.1
_debrel=1.7
pkgrel=1
pkgdesc='The GNU Debugger'
arch=('i686' 'x86_64')
url='https://www.gnu.org/software/gdb/'
license=('GPL-3' 'LGPL-3')
makedepends=('glibc' 'gcc-libs' 'texinfo' 'python' 'guile' 'ncurses' 'expat' 'xz' 'mpfr' 'source-highlight'
             'readline' 'quilt')
source=(https://ftp.gnu.org/gnu/gdb/${pkgname}-${pkgver}.tar.xz{,.sig}
        https://deb.debian.org/debian/pool/main/g/gdb/gdb_${_debver}-${_debrel}.debian.tar.xz)
sha512sums=('0dc54380435c6853db60f1e388b94836d294dfa9ad7f518385a27db4edd03cb970f8717d5f1e9c9a0d4a33d7fcf91bc2e5d6c9cf9e4b561dcc74e65b806c1537'
            'SKIP'
            '2a589aace275cb45a8d4c5a8680c7bd69b1f88f7920dd6ddefae3f5e8dfa03d27955119f7fc9fb2a0e57b00e5144a36cb70a9a942f2e7f82160bcd487059f8b8')
validpgpkeys=('F40ADB902B24264AA42E50BF92EDB04BFF325CF3') # Joel Brobecker

prepare() {
  cd gdb-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  # hack! - libiberty configure tests for header files using "$CPP $CPPFLAGS"
  sed -i "/ac_cpp=/s/\$CPPFLAGS/\$CPPFLAGS -O2/" libiberty/configure
}

build() {
  cd gdb-$pkgver

  mkdir -p build && cd build
  ../configure \
    --prefix=/usr \
    --disable-nls \
    --enable-source-highlight \
    --enable-tui \
    --with-system-readline \
    --with-python=/usr/bin/python \
    --with-guile=guile-2.2 \
    --with-system-gdbinit=/etc/gdb/gdbinit
  make
}

package_gdb-common() {
  depends=('python' 'guile')

  cd gdb-$pkgver/build
  make -C gdb/data-directory DESTDIR="$pkgdir" install
  install -Dm644 "${srcdir}/gdb-$pkgver/"{COPYING3,COPYING3.LIB} -t "${pkgdir}"/usr/share/licenses/${pkgname}
}

package_gdb() {
  depends=('glibc' 'ncurses' 'gcc-libs' 'expat' 'xz' 'mpfr' 'source-highlight' "gdb-common=${pkgver}"
           'readline' 'guile' 'python' 'libelf')
  backup=(etc/gdb/gdbinit)

  cd gdb-$pkgver/build
  make -C gdb DESTDIR="$pkgdir" install
  make -C gdbserver DESTDIR="$pkgdir" install

  # install "custom" system gdbinit
  install -dm 755 "$pkgdir/etc/gdb"
  touch "$pkgdir/etc/gdb/gdbinit"

  # comes from gdb-common
  rm -r "$pkgdir/usr/share/gdb/"

  install -Dm644 "${srcdir}/gdb-$pkgver/"{COPYING3,COPYING3.LIB} -t "${pkgdir}"/usr/share/licenses/${pkgname}
}
