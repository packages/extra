# Maintainer (Arch): David Runge <dvzrv@archlinux.org>
# Contributor (Arch): Federico Cinelli <cinelli@aur.archlinux.org>
# Contributor (Arch): Mateusz Herych <heniekk@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Jorge Lopez <jorginho@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=libquicktime
pkgver=1.2.4
_debver=$pkgver
_debrel=12
pkgrel=17
pkgdesc="A library for reading and writing quicktime files"
arch=('i686' 'x86_64')
license=('LGPL-2.1')
url='https://libquicktime.sourceforge.net/'
depends=('glib2' 'glibc' 'zlib' 'alsa-lib' 'faad2' 'ffmpeg' 'gtk2' 'lame'
         'mesa-libgl' 'libjpeg-turbo' 'libvorbis' 'libx11' 'libxaw'
         'libxext' 'libxt' 'libxv' 'libsndio' 'x264')
makedepends=('quilt')
optdepends=('alsa-lib: for lqtplay'
            'ffmpeg: for lqt_ffmpeg plugin'
            'gtk2: for libquicktime_config'
            'lame: for lqt_lame plugin'
            'libjpeg-turbo: for lqt_mjpeg plugin'
            'libpng: for lqt_png plugin'
            'libvorbis: for lqt_lame and lqt_vorbis plugin'
            'libx11: for lqtplay'
            'libxaw: for lqtplay'
            'libxext: for lqtplay'
            'libxt: for lqtplay'
            'libxv: for lqtplay'
            'sndio: for lqtplay'
            'x264: for lqt_x264 plugin')
source=("https://downloads.sourceforge.net/sourceforge/$pkgname/$pkgname-$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/libq/libquicktime/libquicktime_$_debver-$_debrel.debian.tar.xz"
        "${pkgname}-1.2.4-faad2.patch")
sha512sums=('6ab6cd62ae2361bb3ad73725e9e8baa2b03361bc23e9e34b24f90c0e2bec4337de950ed7f30a6a1d2e67ab33e8f0c43538369fd07e417f54b84583a3c331f8dd'
            '714f941a47b46cb308c6d84f2a3689f9b80c7282bc6934c149e1616c4c61eb57ce1053c8523831519b8bd21f36116261c9a81d97ee870811ac7dfbcde2c1b215'
            '3e751330ebbc4026363efd45b21c60c313ea169a57a5cdfa46125e126aab34c2dea3629ce92e7b0b5f14693db4f11723177fd28611e2e979cc8fcd506077509e')

prepare() {
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/02-install-in-libquicktime2.patch || true

    quilt push -av
  fi

  # checks for faad2 are broken in configure.ac
  patch -Np1 -i "${srcdir}/${pkgname}-1.2.4-faad2.patch"

  autoreconf -vfi
}

build() {
  cd $pkgname-$pkgver
  ./configure \
    --prefix=/usr \
    --enable-gpl \
    --with-ffmpeg \
    --with-x264 \
    --without-doxygen \
    --without-schroedinger \
    --without-libdv \
    --without-faac
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -vDm 644 {AUTHORS,ChangeLog,README,TODO} -t "${pkgdir}/usr/share/doc/${pkgname}"
  install -Dm644 COPYING -t ${pkgdir}/usr/share/licenses/${pkgname}/
}
