# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad

_name=defusedxml
pkgname=python-defusedxml
pkgver=0.6.0
_debver=$pkgver
_debrel=2
pkgrel=2
pkgdesc="XML bomb protection for Python stdlib modules"
arch=('any')
url='https://bitbucket.org/tiran/defusedxml'
license=('Python')
depends=('python')
makedepends=('python-setuptools' 'quilt')
source=("https://pypi.io/packages/source/d/${_name}/${_name}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/d/${_name}/${_name}_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('dd1313802cf8565fd319ef10af04075019d00cba557e6414b2a2762bd4302437a32c05cd8c9e4cec5310c68c6a95d2951acf1360244e7974d40bb3fe963b7226'
            '4a3959996c0da19a0926ee0161d6a0920a8359796b3c5ba69b5182b998b4e1705ca26ba94547ebe7c7e4199e1cf1356f546441381290c88cf4ff99b5cbf8d6ba')

prepare() {
  cd $_name-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $_name-$pkgver
  python setup.py build
}

package() {
  cd $_name-$pkgver
  python setup.py install --root="$pkgdir" --optimize=1
  install -Dm644 LICENSE -t "$pkgdir"/usr/share/licenses/$pkgname/
}
