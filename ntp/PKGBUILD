# Maintainer (Arch): Lukas Fleischer <lfleischer@archlinux.org>
# Contributor (Arch): Gaetan Bisson <bisson@archlinux.org>
# Contributor (Arch): kevin <kevin@archlinux.org>
# Contributor (Artix): artoo <artoo@cromnix.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=ntp
_pkgname=ntp #-dev
_pkgver=4.2.8p15
pkgver=${_pkgver/p/.p}
_debver=4.2.8p15
_debrel=1
pkgrel=1
pkgdesc='Network Time Protocol reference implementation'
url='http://www.ntp.org/'
license=('Simplified-BSD')
arch=('i686' 'x86_64')
depends=('libressl' 'perl' 'libcap' 'libedit')
makedepends=('quilt')
optdepends=('logger: message logging support')
backup=('etc/ntp.conf'
        'etc/conf.d/ntpd'
        'etc/conf.d/ntp-client'
        'etc/init.d/sntp')
source=("https://www.eecis.udel.edu/~ntp/ntp_spool/ntp4/ntp-4.2/${_pkgname}-${_pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/n/ntp/ntp_$_debver+dfsg-$_debrel.debian.tar.xz"
        'ntp.conf'
        'libressl-2.8.patch'
        'ntpd.confd'
        'ntp-client.confd'
        'sntp.confd'
        'ntpd.initd'
        'ntp-client.initd'
        'sntp.initd')
sha512sums=('f5ad765e45fc302263dd40e94c287698fd235b94f3684e49f1d5d09d7d8bdd6b8c0fb96ecdabffea3d233e1e79b3c9687b76dc204ba76bad3f554682f4a97794'
            '6fe6603f2c699d65ffa4e49f1c406af9d8b50a97154b157e5111d332ea2d25374fb15813fbbc0d9788b7fa93469c6b768d1371ebc5b6235dd47bbb408b6b55af'
            '94e801d7a04620bc473f387e20a250b53e7788e287e42081812d47ea47be21a17edd2d3471bd0ad416ce80dccf3788cf93025ced37a1c5984fe072662f427855'
            '00ff0ce8e824190b12d914518be9653c79d72137e45e10e5e62f835e8d38c7cbb740acc669a0062e9d300f18257169823acd535023f524d77a147a73b5c284ca'
            'a9e96a214d3c21a3f4863709c48a11f9dedc73e2d83a9b20761021df44a9acb56757e77f113aa0314f5f3923e9cb26bc6e0147658d20e8a74d078d415a171d91'
            '2d6b9d96c55787cf4556332054a61399368dcf37cf76a14aaba3f8e2e4fdd47ce25808f9babda2ca1f2a6e7387c043e023251ba2b99786426c875bd7cb6592a6'
            '28c8f1fd3a69bbdfacc2379a3ce01f254ae21fd8b91d893600086d27e5fb9cccf05d1bf62022ab26fcb53fa2080d4178961619811fd7a56f4e7502893f8ea13b'
            'd51856ebff9160a01e1331114c5a8ea7337f9c8b570da75f36bbb29dcaf1924a40cdbbbf56e03148fc0f540103846e58d15175acf2e9d83c6b68d55c1e199734'
            '37f4937423cf3f39e7e801bd794ffb6f2a52e995a104e8969a6c373414c05aa31aaa59c2f4d181a4ef00cb22115584131c6e0dce4385b7b652f9e002c585e9fb'
            '8359d290b90fe9797a15305947a47e1a7731eb30f0d353bc2199c971ccdb190ed5a990df5c04263229791763d4a9e029ebb5af3a19cd8de21c13a67b2ecd9ac1')

options=('!emptydirs')
install=ntp.install

prepare() {
	cd "${srcdir}/${_pkgname}-${_pkgver}"

	if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
		# Debian patches
		export QUILT_PATCHES=debian/patches
		export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
		export QUILT_DIFF_ARGS='--no-timestamps'

		mv "$srcdir"/debian .

		# Doesn't apply and seems unimportant
		rm -v debian/patches/debian-locfile.patch || true
		rm -v debian/patches/reproducible-build.patch || true

		quilt push -av
	fi

	patch -p1 -i ../libressl-2.8.patch
}

build() {
	cd "${srcdir}/${_pkgname}-${_pkgver}"

	./configure --prefix=/usr --enable-linuxcaps --enable-ntp-signd
	make
}

package() {
	cd "${srcdir}/${_pkgname}-${_pkgver}"

	make DESTDIR="${pkgdir}" install

	install -d -m755 "${pkgdir}"/usr/sbin
	mv "${pkgdir}"/usr/bin/{ntp-keygen,ntp-wait,ntpd,ntpdate,ntptime} "${pkgdir}"/usr/sbin

	install -Dm644 ../ntp.conf "${pkgdir}"/etc/ntp.conf
	for f in ntpd ntp-client sntp; do
	  install -Dm644 ../$f.confd "${pkgdir}"/etc/conf.d/$f
	  install -Dm755 ../$f.initd "${pkgdir}"/etc/init.d/$f
	done
	install -Dm644 COPYRIGHT "${pkgdir}/usr/share/licenses/${pkgname}/COPYRIGHT"

	install -d -o 87 "${pkgdir}"/var/lib/ntp
	echo > "${pkgdir}/var/lib/ntp/.placeholder"
}
