# Maintainer (Arch): Andreas Radke <andyrtr@archlinux.org>
# Contributor (Arch): Aaron Griffin <aaron@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=lftp
pkgver=4.8.4
_debver=4.8.4
_debrel=2
pkgrel=1
pkgdesc="Sophisticated command line based FTP client"
arch=('i686' 'x86_64')
license=('GPL-3')
depends=('gcc-libs' 'readline' 'gnutls' 'expat' 'sh' 'hicolor-icon-theme')
makedepends=('quilt')
optdepends=('perl: needed for convert-netscape-cookies and verify-file')
url='https://lftp.yar.ru/'
backup=('etc/lftp.conf')
source=(https://lftp.yar.ru/ftp/${pkgname}-${pkgver}.tar.xz{,.asc}
        https://deb.debian.org/debian/pool/main/l/lftp/lftp_${_debver}-${_debrel}.debian.tar.xz)
sha512sums=('2528e2cbfe132068dc75036d6334372f79f8063d44e46c724a28ea1f5b40d8de0d1e36cde3124fbcc27e5ba8be04b893eb1a7546346253768346a80bc54c02d1'
            'SKIP'
            '603ad4f1a6791f41df85b2143344647583d95b8ad7cada59f7d65deb8b5fe4b3e002ac5b96128e0af88ba3eaffe55c98f90bac4e41fb949f4c2a66a08e2b4a7d')
validpgpkeys=('C027FA3148652A5513036413A824BB69F2A99A18') # "Alexander V. Lukyanov <lav@yars.free.net>"

prepare() {
  cd "${pkgname}"-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "${pkgname}"-${pkgver}
  ./configure --prefix=/usr \
    --with-gnutls \
    --without-openssl \
    --without-included-regex \
    --disable-static
  make
}

package() {
  cd "${pkgname}"-${pkgver}
  make DESTDIR="${pkgdir}" install
  rm -rf "${pkgdir}"/usr/lib
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
