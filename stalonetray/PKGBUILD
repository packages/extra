# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=stalonetray
pkgver=0.8.1
_debver=$pkgver
_debrel=1
pkgrel=1
pkgdesc="STAnd-aLONE sysTRAY. It has minimal build and run-time dependencies: the Xlib only."
arch=('i686' 'x86_64')
url='http://stalonetray.sourceforge.net'
license=('GPL-2')
depends=('libx11' 'libxpm')
makedepends=('quilt')
source=("https://downloads.sourceforge.net/$pkgname/$pkgname-$pkgver.tar.bz2"
        "https://deb.debian.org/debian/pool/main/s/stalonetray/stalonetray_$_debver-$_debrel.debian.tar.gz")
sha512sums=('e8b15f3f1e4a9073ba92ca628e79e9add26965ad0686a30c63cf14eb03de1d234550dfbd50a22f05771bded8f7c64da093cda5b44c38e91f2a837c50c901144b'
            'ac298fc2d11d367e3ce59ca8eeda1b79f5d1292fe7645b7057eaaf2fbf3495074a18d5b2ce42fa9f7442ccf50152aaed318993c35b03e0049441cf5a59af4a00')

prepare() {
  cd $srcdir/$pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $srcdir/$pkgname-$pkgver
  ./configure \
    --prefix=/usr \
    --disable-native-kde
  make
}

package() {
  cd $srcdir/$pkgname-$pkgver
  make DESTDIR=$pkgdir install
  install -Dm644 stalonetrayrc.sample "$pkgdir/etc/stalonetrayrc"
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}
