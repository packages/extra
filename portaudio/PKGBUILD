# Maintainer (Arch): David Runge <dave@sleepmap.de>
# Contributor (Arch): Lukas Fleischer <lfleischer@archlinux.org>
# Contributor (Arch): Eric Belanger <eric@archlinux.org>
# Contributor (Arch): Darwin Bautista <djclue917@gmail.com>
# Contributor (Arch): Bob Finch <w9ya@qrparci.net>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=portaudio
# versioning is very bizarre:
# https://app.assembla.com/wiki/show/portaudio/ReleaseNotes
_version=190600_20161030
pkgver=19.6.0
_debver=$pkgver
_debrel=1.1
pkgrel=4
epoch=1
pkgdesc='A free, cross-platform, audio I/O library.'
arch=('i686' 'x86_64')
url="http://www.portaudio.com/"
license=('Expat')
depends=('gcc-libs')
makedepends=('jack' 'alsa-lib' 'libsndio' 'quilt')
source=("${pkgname}-${pkgver}.tgz::http://www.portaudio.com/archives/pa_stable_v${_version}.tgz"
        "https://deb.debian.org/debian/pool/main/p/portaudio19/portaudio19_$_debver-$_debrel.debian.tar.xz"
        "sndio.patch")
sha512sums=('7ec692cbd8c23878b029fad9d9fd63a021f57e60c4921f602995a2fca070c29f17a280c7f2da5966c4aad29d28434538452f4c822eacf3a60af59a6dc8e9704c'
            'a0c49f92be9a41fec1e58f78b8fbbdb77ce35cfb283628b4518670ddefa7c5304554858d9895c9124c97993d76446013ac9a27270d8e15bf99125ecfb4a470c5'
            'da0aa902e0895d68236811b470832e5a38d0d41967fcff6c1ca4b916ff1fb84dad936518f1337200732075b66ecc5c9ff2368f75fde9ee8c66ee37964a6953aa')

prepare() {
  mv -v "${pkgname}" "${pkgname}-${pkgver}"
  cd "${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  patch -p0 -i "${srcdir}/sndio.patch"

  # depcomp is required when building the bindings/cpp extension
  # but will be removed by autoreconf + libtool >= 2.4.6
  # Protect it from removal
  mv depcomp{,~}
  autoreconf -fi
  # Restore depcomp
  mv depcomp{~,}
}

build() {
  cd "${pkgname}-${pkgver}"
  ./configure --prefix=/usr \
              --enable-cxx \
              --with-alsa \
              --with-sndio \
              --with-jack

  # build crashes somewhere above > 8 jobs
  make -j1
}

package() {
  depends+=('jack' 'alsa-lib' 'libsndio')
  cd "${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install
  install -vDm 644 LICENSE.txt -t "${pkgdir}/usr/share/licenses/${pkgname}"
  install -vDm 644 README.txt -t "${pkgdir}/usr/share/doc/${pkgname}"
}
