From 1bb5900a8aa64ae332dcd49a7ce82e43063b0244 Mon Sep 17 00:00:00 2001
From: Christian Hesse <mail@eworm.de>
Date: Wed, 24 Feb 2016 09:16:51 +0100
Subject: [PATCH] allow to build ISO image with EFI support (ipxe.eiso)

---
 src/arch/x86/Makefile.pcbios |  7 +++++
 src/util/geniso              | 52 +++++++++++++++++++++++++++---------
 2 files changed, 47 insertions(+), 12 deletions(-)

diff --git a/src/arch/x86/Makefile.pcbios b/src/arch/x86/Makefile.pcbios
index c44eefc1fa..bad076298d 100644
--- a/src/arch/x86/Makefile.pcbios
+++ b/src/arch/x86/Makefile.pcbios
@@ -86,6 +86,13 @@ NON_AUTO_MEDIA	+= iso
 	$(Q)ISOLINUX_BIN=$(ISOLINUX_BIN) LDLINUX_C32=$(LDLINUX_C32) \
 	    VERSION="$(VERSION)" bash util/geniso -o $@ $<
 
+# rule to make a non-emulation ISO boot image with EFI support
+NON_AUTO_MEDIA += eiso
+%eiso: %lkrn bin-i386-efi/ipxe.efi bin-x86_64-efi/ipxe.efi util/geniso
+	$(QM)$(ECHO) "  [GENISO] $@"
+	$(Q)ISOLINUX_BIN=$(ISOLINUX_BIN) LDLINUX_C32=$(LDLINUX_C32) \
+	    VERSION="$(VERSION)" bash util/geniso -e -o $@ $<
+
 # rule to make a floppy emulation ISO boot image
 NON_AUTO_MEDIA	+= liso
 %liso:	%lkrn util/geniso
diff --git a/src/util/geniso b/src/util/geniso
index ff090d4a08..7694036862 100755
--- a/src/util/geniso
+++ b/src/util/geniso
@@ -6,16 +6,21 @@ function help() {
 	echo "usage: ${0} [OPTIONS] foo.lkrn [bar.lkrn,...]"
 	echo
 	echo "where OPTIONS are:"
+	echo " -e       build image with EFI support"
 	echo " -h       show this help"
 	echo " -l       build legacy image with floppy emulation"
 	echo " -o FILE  save iso image to file"
 }
 
+EFI=0
 LEGACY=0
 FIRST=""
 
-while getopts "hlo:" opt; do
+while getopts "ehlo:" opt; do
 	case ${opt} in
+		e)
+			EFI=1
+			;;
 		h)
 			help
 			exit 0
@@ -37,17 +42,25 @@ if [ -z "${OUT}" ]; then
 	exit 1
 fi
 
-# There should either be mkisofs or the compatible genisoimage program
-for command in genisoimage mkisofs; do
-	if ${command} --version >/dev/null 2>/dev/null; then
-		mkisofs=(${command})
-		break
-	fi
-done
-
-if [ -z "${mkisofs}" ]; then
-	echo "${0}: mkisofs or genisoimage not found, please install or set PATH" >&2
+# We need xorriso (from libisoburn) for EFI support, so try that first.
+if xorriso --version >/dev/null 2>/dev/null; then
+	mkisofs=(xorriso -as mkisofs)
+elif [ ${EFI} -eq 1 ]; then
+	echo "${0}: xorriso not found, but required for EFI support. Please install." >&2
 	exit 1
+else
+	# fall back to mkisofs or the compatible genisoimage program
+	for command in genisoimage mkisofs; do
+		if ${command} --version >/dev/null 2>/dev/null; then
+			mkisofs=(${command})
+			break
+		fi
+	done
+
+	if [ -z "${mkisofs}" ]; then
+		echo "${0}: mkisofs or genisoimage not found, please install or set PATH" >&2
+		exit 1
+	fi
 fi
 
 dir=$(mktemp -d bin/iso.dir.XXXXXX)
@@ -122,13 +135,28 @@ case "${LEGACY}" in
 		# copy isolinux bootloader
 		cp ${ISOLINUX_BIN} ${dir}
 
+		mkisofs+=(-b isolinux.bin -no-emul-boot -boot-load-size 4 -boot-info-table)
+
+		if [ "${EFI}" -eq 1 ]; then
+			# generate EFI image
+			img=${dir}/efiboot.img
+
+			mformat -f 2880 -C -i ${img} ::
+			mmd -i ${img} "::/EFI"
+			mmd -i ${img} "::/EFI/BOOT"
+			mcopy -m -i ${img} bin-x86_64-efi/ipxe.efi "::EFI/BOOT/BOOTX64.EFI"
+			mcopy -m -i ${img} bin-i386-efi/ipxe.efi "::EFI/BOOT/BOOTIA32.EFI"
+
+			mkisofs+=(-eltorito-alt-boot -e efiboot.img -isohybrid-gpt-basdat -no-emul-boot)
+		fi
+
 		# syslinux 6.x needs a file called ldlinux.c32
 		if [ -n "${LDLINUX_C32}" -a -s "${LDLINUX_C32}" ]; then
 			cp ${LDLINUX_C32} ${dir}
 		fi
 
 		# generate the iso image
-		"${mkisofs[@]}" -b isolinux.bin -no-emul-boot -boot-load-size 4 -boot-info-table -output ${OUT} ${dir}
+		"${mkisofs[@]}" -output ${OUT} ${dir}
 
 		# isohybrid will be used if available
 		if isohybrid --version >/dev/null 2>/dev/null; then
