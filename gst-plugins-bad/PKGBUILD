# Maintainer (Arch): Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Maintainer: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=gst-plugins-bad
pkgname=('gst-plugins-bad-libs' 'gst-plugins-bad')
pkgver=1.18.4
_debver=$pkgver
_debrel=3
pkgrel=12
pkgdesc="Multimedia graph framework - bad plugins"
url='https://gstreamer.freedesktop.org/'
arch=(i686 x86_64)
license=(LGPL-2.1)
depends=(gst-plugins-base-libs orc libdrm libx11 libusb libxkbcommon-x11)
makedepends=(mjpegtools curl libmms faad2 libdca libdvdnav
             libmodplug nettle libkate openjpeg2 libass
             libexif libdvdread wildmidi ladspa openal
             soundtouch spandsp neon libmpcdec libbs2b
             lcms2 glu liblrdf libde265 zbar
             librsvg-legacy fluidsynth lilv lv2 gst-plugins-good python
             gobject-introspection gtk meson libmicrodns zxing-cpp serd sord sratom quilt)
options=(!emptydirs)
source=("https://gstreamer.freedesktop.org/src/gst-plugins-bad/gst-plugins-bad-${pkgver}.tar.xz"{,.asc}
        "https://security.debian.org/debian-security/pool/updates/main/g/gst-plugins-bad1.0/gst-plugins-bad1.0_${_debver}-${_debrel}+deb11u4.debian.tar.xz"
        "libressl.patch"
        "remove-enforced-deps.patch")
sha512sums=('02123e054419f5ce6ca2ebe0bdf23572269ea4120bf463b4d672efbe5d9e5d417d4d711cc80094be974c4569f754d8b2ad93f59b827e3b018d450582834cb125'
            'SKIP'
            '9b7d7313aea6bde29801b39439bd1d2e82887df48e95c82e59f7dbc721d41f607a1504691c1cc4a7f78b088f59e1504053ab7f05b5c8bdd648cb3e9376d5c286'
            'c52db9fc7e5a72fe638fd9a1a509787f9b1a7a8d493792da5dbf8dc3514572fae2cb75413f73dd4ef30af3a46afcf4edcb718e06b39be0e86a77ddf3d41a50db'
            '7f49d275053c8d7b04c0a7516e6f0d6a65a0f3d6b1a4039356dc72e3ed2a7ee3caac014a69b346cc3d70e31ac5c6c1c2e33bb2b6add477904f32c660b95540ce')
validpgpkeys=('D637032E45B8C6585B9456565D2EEE6F6F349D7C') # Tim Müller <tim@gstreamer-foundation.org>

prepare() {
  cd $pkgbase-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/02_opencv-data-path.patch || true

    quilt push -av
  fi

  patch -p0 -i $srcdir/libressl.patch
  patch -p1 -i $srcdir/remove-enforced-deps.patch
}

build() {
  hyperbola-meson $pkgbase-$pkgver build \
    -D package-name="GStreamer Bad Plugins (Hyperbola GNU/Linux-libre)" \
    -D package-origin="https://www.hyperbola.info/" \
    -D directfb=disabled \
    -D doc=disabled \
    -D flite=disabled \
    -D gsm=disabled \
    -D iqa=disabled \
    -D magicleap=disabled \
    -D msdk=disabled \
    -D openh264=disabled \
    -D openmpt=disabled \
    -D openni2=disabled \
    -D opensles=disabled \
    -D tinyalsa=disabled \
    -D voaacenc=disabled \
    -D voamrwbenc=disabled \
    -D wasapi2=disabled \
    -D wasapi=disabled \
    -D gobject-cast-checks=disabled \
    -D wayland=disabled \
    -D bluez=disabled \
    -D fdkaac=disabled \
    -D faac=disabled \
    -D svthevcenc=disabled \
    -D webrtc=disabled \
    -D webrtcdsp=disabled \
    -D sbc=disabled \
    -D wpe=disabled \
    -D webp=disabled \
    -D v4l2codecs=disabled \
    -D uvch264=disabled \
    -D va=disabled \
    -D vulkan=disabled \
    -D aom=disabled \
    -D srt=disabled \
    -D srtp=disabled \
    -D rtmp=disabled \
    -D rtmp2=disabled \
    -D opencv=disabled \
    -D avtp=disabled \
    -D androidmedia=disabled \
    -D applemedia=disabled \
    -D openexr=disabled \
    -D dc1394=disabled \
    -D gme=disabled \
    -D ofa=disabled \
    -D chromaprint=disabled \
    -D teletext=disabled \
    -D x265=disabled
  meson compile -C build
}

package_gst-plugins-bad-libs() {
  pkgdesc="${pkgdesc% plugins}"
  provides=("gst-transcoder=$pkgver")
  conflicts=("gst-transcoder<1.18.0")
  replaces=("gst-transcoder<1.18.0")

  DESTDIR="$pkgdir" meson install -C build

  install -Dm644 $pkgbase-$pkgver/COPYING -t $pkgdir/usr/share/licenses/$pkgname

  mkdir -p ext/lib/gstreamer-1.0
  for _x in assrender bs2b bz2 closedcaption colormanagement \
            curl de265 dtls dtsdec faad fluidsynthmidi \
            hls kate ladspa lv2 mms modplug mpeg2enc mplex musepack \
            neonhttpsrc openal openjpeg opusparse resindvd rsvg \
            sctp smoothstreaming sndfile soundtouch spandsp \
            ttmlsubs wildmidi zbar zxing microdns; do
    _x="lib/gstreamer-1.0/libgst${_x}.so"
    mv "$pkgdir/usr/$_x" "ext/$_x"
  done
}

package_gst-plugins-bad() {
  depends=("gst-plugins-bad-libs=$pkgver" gst-plugins-good
           libass libbs2b bzip2 pango lcms2 curl libxml2
           libde265 libressl libdca faad2 fluidsynth
           nettle libkate liblrdf lilv libmms libmodplug mjpegtools
           libmpcdec neon openal openjpeg2 opus libdvdnav
           libdvdread librsvg-legacy libsndfile soundtouch spandsp
           libxcb wildmidi zbar libmicrodns
           zxing-cpp serd sord sratom)

  mv ext "$pkgdir/usr"

  install -Dm644 $pkgbase-$pkgver/COPYING -t $pkgdir/usr/share/licenses/$pkgname
}
