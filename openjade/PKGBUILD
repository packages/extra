# Maintainer (Arch): AndyRTR <andyrtr@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=openjade
pkgver=1.3.2
pkgrel=1
_debpatch=13
pkgdesc='Implementation of the DSSSL language'
arch=('i686' 'x86_64')
url='http://openjade.sourceforge.net/' # no https
license=('Expat')
depends=('opensp' 'sgml-common')
makedepends=('perl-perl4-corelibs')
install=${pkgname}.install
conflicts=('jade')
provides=('jade')
source=(https://downloads.sourceforge.net/project/${pkgname}/${pkgname}/${pkgver}/${pkgname}-${pkgver}.tar.gz
        https://deb.debian.org/debian/pool/main/o/${pkgname}1.3/${pkgname}1.3_${pkgver}-${_debpatch}.diff.gz)
sha512sums=('c39f70ec8020bd7a2b3e125cbd146b49ddb57910a042bd212da02722617ed5681f32dab60acc26ab89ac658997c6f604911d7be3be391d6278267481f3bcf2f1'
            '9f9d9da2c64a4656ea0ead38b6fa614907e6c759394ec4a5f7235c0a51f733fe1c4417d78f01d146a4d353896f2935ccb509a48f3fbe48f29e223f3d17bb71a4')

prepare() {
  cd ${pkgname}-$pkgver
  patch -Np1 -i "$srcdir"/${pkgname}1.3_${pkgver}-${_debpatch}.diff
  # https://gcc.gnu.org/bugzilla/show_bug.cgi?id=69534#c9
  export CXXFLAGS+=' -fno-lifetime-dse'
}

build() {
  cd ${pkgname}-$pkgver
  ./configure --prefix=/usr \
    --mandir=/usr/share/man \
    --enable-default-catalog=/etc/sgml/catalog \
    --datadir=/usr/share/sgml/openjade-${pkgver} \
    --enable-html \
    --enable-http \
    --enable-mif
  make
}

package() {
  cd ${pkgname}-$pkgver
  make DESTDIR="$pkgdir/" install install-man 
  
  # add unversioned symlink
  ln -svf openjade-${pkgver} "${pkgdir}"/usr/share/sgml/openjade
  
  # openjade -> jade compat symlinks
  ln -svf /usr/bin/openjade "$pkgdir"/usr/bin/jade
  ln -svf /usr/lib/libogrove.so "$pkgdir"/usr/lib/libgrove.so
  ln -svf /usr/lib/libospgrove.so "$pkgdir"/usr/lib/libspgrove.so
  ln -svf /usr/lib/libostyle.so "$pkgdir"/usr/lib/libstyle.so
  ln -svf /usr/share/man/man1/openjade.1.gz "$pkgdir"/usr/share/man/man1/jade.1.gz
  
  # license
  install -m755 -d "${pkgdir}/usr/share/licenses/${pkgname}"
  install -m644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/"

  install -dm755 "$pkgdir"/usr/share/sgml/openjade-${pkgver}
  install -m644 dsssl/builtins.dsl dsssl/dsssl.dtd \
    dsssl/style-sheet.dtd dsssl/fot.dtd \
    dsssl/catalog "$pkgdir"/usr/share/sgml/openjade-${pkgver}
}
