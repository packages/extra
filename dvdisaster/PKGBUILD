# Maintainer (Arch): fitu996 <fitu996 _at_ gmail _dot_ com>
# Maintainer (Arch): flan_suse <windows2linux_AT_zoho_DOT_com>
# Contributor (Arch): Eric Bélanger <eric@archlinux.org>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=dvdisaster
pkgver=0.79.5
_debver=$pkgver
_debrel=10
pkgrel=1
pkgdesc="Provides a margin of safety against data loss on newly created ISO, CD, DVD, and BDR media caused by aging or scratches"
arch=('i686' 'x86_64')
url='https://dvdisaster.jcea.es'
license=('GPL-3')
depends=('gtk2')
makedepends=('quilt')
options=('!makeflags')
source=("https://deb.debian.org/debian/pool/main/d/dvdisaster/dvdisaster_${pkgver}.orig.tar.bz2"
        "https://deb.debian.org/debian/pool/main/d/dvdisaster/dvdisaster_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('8630d4389983307afc3faa5b4256f531ded370f84ec35d4a363853dc7d79e5e15ee0b81bf64f4b88f57fe6176537df8195f83acf23e770d991c700b7408dd1f5'
            'e5fcf51c92628be8084a6ba8c72bb846036400c8e549c7c916963cd2477caf9c434f229953931cde22d16295032c39f8d0251fa8d3192876896f2840fd854ec1')

prepare() {
  cd ${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  # Modify provided .desktop shortcut to point to dvdisaster.png rather than dvdisaster48.png
  sed -i 's/dvdisaster48/dvdisaster/' contrib/dvdisaster.desktop
}

build() {
  export CFLAGS="$CFLAGS -fcommon"
  cd ${pkgname}-${pkgver}
  ./configure --prefix=/usr \
    --mandir=/usr/share/man \
    --docdir=/usr/share/doc \
    --localedir=/usr/share/locale \
    --with-nls=yes
  make
}

package() {
  cd ${pkgname}-${pkgver}
  make BUILDROOT="${pkgdir}" install

  # Remove unnecessary uninstaller script 
  rm -f "${pkgdir}/usr/bin/dvdisaster-uninstall.sh"
  install -D -m 644 contrib/dvdisaster.desktop "${pkgdir}/usr/share/applications/dvdisaster.desktop"

  # Rename all .png icons to strip their resolution from their names, and place them in the proper standard directories
  for i in 16 24 32 48 64 ; do
    install -D -m 644 contrib/dvdisaster${i}.png "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/dvdisaster.png"
  done

  # Install license
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
