# Maintainer (Arch): Alexander F. Rødseth <xyproto@archlinux.org>
# Contributor (Arch): Ionut Biru <ibiru@archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Contributor (Arch): arjan <arjan@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=allegro
pkgver=5.2.8.0
_debver=$pkgver
_debrel=1
pkgrel=2
pkgdesc="Portable library mainly aimed at video game and multimedia programming"
arch=('i686' 'x86_64')
url='https://liballeg.org/'
license=('GPL-2' 'Modified-BSD' 'zlib')
depends=('gtk' 'jack' 'glu' 'libtheora' 'libvorbis' 'libxcursor'
         'libxpm' 'opusfile' 'flac' 'enet' 'openal' 'physfs')
makedepends=('cmake' 'xenocara-proto' 'quilt' 'alsa-lib')
source=("https://github.com/liballeg/allegro5/releases/download/${pkgver}/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/a/allegro5/allegro5_${_debver}+dfsg-${_debrel}.debian.tar.xz"
        "fix-build.patch")
sha512sums=('e7dcfe92646a571929d47c268f0f3844afd67a0396ea0d532908e33e39661c9c5dc7929cc01d622804c068c3aa0e4c394972d128072617457fcabe5ef6a8e5f3'
            '98bcd732d1e03bb0b693655c8256579270e56da9e299a4c9c243b0955010bfe5863d8d10f65cd1c03a246c0d42f4b13074bbb12e34be2ca5c98991830e342441'
            '46d3e267289792b1d8f71c2a369d8d832f2195621e19b1793cb5ff6c4ccf38970ff1043a8f9eb6d62e46184f50d63f0d4238320cb855fa0fd37d0a7816c4d372')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/use-debians-dejavu-font.patch || true

    quilt push -av
  fi
  patch -Np1 -i ${srcdir}/fix-build.patch
}

build() {
  cmake \
    -S $pkgname-$pkgver \
    -B build \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DWANT_DOCS_HTML=OFF \
    -DWANT_AUDIO=ON \
    -DWANT_FONT=ON \
    -DWANT_IMAGE=ON \
    -DWANT_DEMO=OFF \
    -Wno-dev
  make -C build
}

package() {
  make DESTDIR="$pkgdir" -C build install
  install -Dm644 "$srcdir"/$pkgname-$pkgver/LICENSE.txt -t "$pkgdir"/usr/share/licenses/$pkgname
}