# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

# Based on xorg-xinit package

pkgname=xenocara-xinit
_openbsdver=6.9
pkgver=1.4.1
pkgrel=1
pkgdesc="Xenocara initialisation program"
arch=(i686 x86_64)
url="https://www.xenocara.org"
license=('X11')
depends=('libx11' 'xenocara-xauth' 'xenocara-xrdb' 'xenocara-xmodmap' 'inetutils')
makedepends=('xenocara-util-macros')
optdepends=('xenocara-twm' 'xenocara-xconsole' 'xterm')
backup=('etc/X11/xinit/xserverrc'
        'etc/X11/xinit/xinitrc')
provides=('xorg-xinit')
conflicts=('xorg-xinit')
replaces=('xorg-xinit')
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/app/xinit-$pkgver.tar.lz{,.sig}
        move-serverauthfile-into-tmp.patch
        fix-startx-to-xauth-treat-ipv6-properly.patch
        fix-hostname.patch
        xserverrc)
options=(!emptydirs)
sha512sums=('1c60de8403efa366e7c047f012abbc87aa61acf41adbba11b601c750279d408b26464b672a7299c9f2862de914831501630fe3809566d541568cf9fe55632727'
            'SKIP'
            '2785de4871ec28de147943ed2264dce8ca223b75e8a658cbd98acd2d7d9134a9d294de2555ea5089f82de2835685ddb989e5f445d72fa6117912acbbc3ea91b3'
            '0661ab1bc767143c7eaef4414bec69b8f294730d6f73743a1009e97ec17105049bd2bd5d1ba402ffb64e47c5373a0e2a572b177fb1448ace3242eaf464009f80'
            '9a0b858c9960ad0260cd302d9e9c43d22c5e3a75ec92ebf34c5c2a5fc2f04967ecddd1e87382ce3ff16083418c0194d28e0ac7a3048666f2075baeaaca72fae2'
            'f86d96d76bcb340021e7904925f0029f8662e4dfc32489198b3a8695dca069da496539e2287249c763fe9c4d8d5d591fd18fe49a0bee822cbbd0eb712efbb89b')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/app/xinit"
  patch -p1 -i "$srcdir/move-serverauthfile-into-tmp.patch"
  patch -p1 -i "$srcdir/fix-startx-to-xauth-treat-ipv6-properly.patch"
  patch -p1 -i "$srcdir/fix-hostname.patch"
  autoreconf -fi
}

build() {
  cd "xenocara-$_openbsdver/app/xinit"
  ./configure --prefix=/usr \
      --with-xinitdir=/etc/X11/xinit \
      --with-wm=twm \
      --with-xconsole=xconsole
  make
}

package() {
  cd "xenocara-$_openbsdver/app/xinit"
  make DESTDIR="$pkgdir" install

  install -m644 "$srcdir/xserverrc" "$pkgdir/etc/X11/xinit/xserverrc"
  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
