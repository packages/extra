# Maintainer (Arch): Sven-Hendrik Haase <svenstaro@gmail.com>
# Contributor (Arch): Thomas Dziedzic < gostrc at gmail >
# Contributor (Arch): Stefan Husmann <stefan-husmann@t-online.de>
# Contributor (Arch): Mihai Militaru <mihai.militaru@gmx.com>
# Contributor (Arch): scippio <scippio@berounet.cz>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=freeimage
pkgver=3.18.0
_debver=$pkgver
_debrel=6
pkgrel=3
pkgdesc="Free software library project for developers who would like to support popular graphics image formats"
arch=('i686' 'x86_64')
license=('GPL-2' 'custom:FIPL')
url='https://freeimage.sourceforge.net/'
depends=('libjpeg-turbo' 'openjpeg2' 'libtiff' 'libraw')
makedepends=('mesa' 'glu' 'quilt')
source=("https://downloads.sourceforge.net/project/freeimage/Source%20Distribution/${pkgver}/FreeImage${pkgver//./}.zip"
        "https://deb.debian.org/debian/pool/main/f/freeimage/freeimage_${_debver}+ds2-${_debrel}+deb11u1.debian.tar.xz"
        "fix-build.patch")
sha512sums=('9d9cc7e2d57552c3115e277aeb036e0455204d389026b17a3f513da5be1fd595421655488bb1ec2f76faebed66049119ca55e26e2a6d37024b3fb7ef36ad4818'
            '22f9088f0a961329c245fab6e641ddd9ecbef8f83a344cdef3faed6df63415484e4ffdccd676464a6e1690f4eab286928c8e58348c90ea0032a86a472c6d2cd4'
            'ae113ded05f94b15168dabc7c8997a26e4136000792144da390a6ef570b367e861163041e772b2705daf1265ef0e34cee7fae743bfce5a54a035ed0817d369d8')

prepare() {
  cd FreeImage
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  rm Source/FreeImage/GetType.cpp
  rm -r Source/Lib* Source/ZLib Source/OpenEXR
  # can't be built due to private headers
  > Source/FreeImage/PluginG3.cpp
  > Source/FreeImageToolkit/JPEGTransform.cpp
}

build() {
  cd FreeImage
  sh gensrclist.sh
  sh genfipsrclist.sh

  # change build-flags for compiling
  if [[ $CARCH = "i686" ]]; then
    export CXXFLAGS="-Og"
    export CFLAGS="-Og"
  elif [[ $CARCH = "x86_64" ]]; then
    export CXXFLAGS="-fPIC"
    export CFLAGS="-fPIC"
  fi

  # additional patch before build, removal of non-free parts
  patch -Np1 -i "${srcdir}"/fix-build.patch

  make -f Makefile.gnu
  make -f Makefile.fip
}

package() {
  cd FreeImage
  make -f Makefile.gnu DESTDIR="${pkgdir}" install
  make -f Makefile.fip DESTDIR="${pkgdir}" install

  install -Dm644 "${srcdir}"/FreeImage/license-{fi,gplv2}.txt -t "${pkgdir}"/usr/share/licenses/${pkgname}
}
