#!/bin/ksh

# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Maintainer: Márcio Silva <coadde@hyperbola.info>

# Based on aspell-en

_pkgname='aspell'
_pkglang='pt'
_srclang="${_pkglang}"
_srcsv=''
pkgname="${_pkgname}-i18n-dict-${_pkglang}"
# Example: _srcname='aspell6-en'
_srcname="${_pkgname}${_srcsv}-${_srclang}"
pkgver='0.50.2'
_srcver='0.50-2'
pkgrel='2'
pkgdesc='Portuguese dictionary for Aspell'
arch=('i686' 'x86_64')
url='https://ftp.gnu.org/gnu/aspell/dict/0index.html'
license=('GPL-2')
groups=("${_pkgname}-i18n-dict" "${_pkgname}-dict"
        "g${_pkgname}-i18n-dict" "g${_pkgname}-dict"
        "gnu${_pkgname}-i18n-dict" "gnu${_pkgname}-dict"
        "gnu-${_pkgname}-i18n-dict" "gnu-${_pkgname}-dict")
depends=('aspell')
provides=("${_pkgname}-dict-${_pkglang}"
          "g${pkgname}" "g${_pkgname}-dict-${_pkglang}"
          "gnu${pkgname}" "gnu${_pkgname}-dict-${_pkglang}"
          "gnu-${pkgname}" "gnu-${_pkgname}-dict-${_pkglang}"
          "${pkgbase}-${_pkglang}-br" "${pkgbase}-${_pkglang}-pt"
          "${_pkgname}-${_pkglang}")
conflicts=("${pkgbase}-${_pkglang}-br" "${pkgbase}-${_pkglang}-pt"
           "${_pkgname}-${_pkglang}")
replaces=("${_pkgname}-${_pkglang}")
_source=("https://ftp.gnu.org/gnu/${_pkgname}/dict/${_srclang}/")
source=("${_source[0]}/${_srcname}-${_srcver}.tar.bz2")
sha512sums=('21f115eae4507ca6e8c726476ba40910886e2ed17161cddbe493b2fe1c55a30703f033f8062eb0b9543d85541aa15e9ecab508cb3bb7403881315ecdb0bc55cd')
validpgpkeys=()  # The signature file is not found.
unset _pkgname _pkglang _srclang _srcsv _source

build() {
  cd "${srcdir}/${_srcname}-${_srcver}"
  ./configure
  make 'V=1'
}

package() {
  cd "${srcdir}/${_srcname}-${_srcver}"
  make "DESTDIR=${pkgdir}" 'install'

  for i in 'COPYING' 'Copyright'; do
    install -Dm '644' "${i}" -t "${pkgdir}/usr/share/licenses/${pkgname}"
  done
  unset i _srcname _srcver
}
