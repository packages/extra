# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Contributor (Arch): Ionut Biru <ibiru@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=liblouis
pkgver=3.16.0
_debver=3.16.0
_debrel=1
pkgrel=1
pkgdesc="Free software braille translator and back-translator"
arch=(i686 x86_64)
url="http://liblouis.org/"
license=(LGPL-3)
depends=(glibc)
makedepends=(help2man python quilt)
source=(https://github.com/liblouis/liblouis/releases/download/v$pkgver/liblouis-$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/libl/liblouis/liblouis_$_debver-$_debrel.debian.tar.xz)
sha512sums=('b247e8e501b3b07c45e55ff5df79d7ee8174c12dba9cf7e11ae88e0bbb845c08330e6a1be5ebabc21258bcfd7276484f525e9acc56605ba0fd3ec910b7289a1b'
            '8147f70dd80ccc9685a2101205391c7ec630f3d5354b174165571bc7b543714a82a23bce32c454e4dab7e8cb3a4ce3a6d45df66d4fedaa6f43b1a000f4125718')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr --disable-static --enable-ucs4
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install

  cd python
  LD_PRELOAD+=":$srcdir/$pkgname-$pkgver/liblouis/.libs/liblouis.so"
  python setup.py install --root="$pkgdir" --prefix="/usr" --optimize=1

  install -Dm644 $srcdir/$pkgname-$pkgver/COPYING.LESSER $pkgdir/usr/share/licenses/$pkgname/COPYING.LESSER
}
# vim:set ts=2 sw=2 et:
