#!/sbin/openrc-run
# Copyright 2017-2021 Hyperbola Project
# Distributed under the terms of the GNU General Public License v2

CHROOT=/srv/torchroot
PIDFILE=/var/run/tor/tor.pid
CONFFILE=/etc/tor/torrc
SVCNAME=tor
GRACEFUL_TIMEOUT=${GRACEFUL_TIMEOUT:-60}
EXTRA_COMMANDS="torvercheck"

# See bug #523552, and https://trac.torproject.org/projects/tor/ticket/5525
# Graceful = wait 30 secs or so until all connections are properly closed.
extra_commands="checkconfig"
extra_started_commands="graceful gracefulstop reload"
description="Anonymizing overlay network for TCP"
description_checkconfig="Check for valid config file."
description_reload="Reload the configuration."
description_graceful="Gracefully restart."
description_gracefulstop="Gracefully stop."

depend() {
        need net
}

checkconfig() {
        # first check that it exists
        if [ ! -f ${CHROOT}${CONFFILE} ] ; then
                eerror "You need to setup ${CHROOT}${CONFFILE} first"
                eerror "Example is in ${CHROOT}${CONFFILE}.sample"
                return 1
        fi

        if [ ! -c ${CHROOT}/dev/random ] ; then
                mknod -m 666 ${CHROOT}/dev/null c 1 3
                mknod -m 644 ${CHROOT}/dev/random c 1 8
                mknod -m 644 ${CHROOT}/dev/urandom c 1 9
                mount -ro remount ${CHROOT}/dev
        fi

        checkpath --quiet --mode 755 --owner "${SVCNAME}":"${SVCNAME}" --directory `dirname ${CHROOT}${PIDFILE}`

        # now verify whether the configuration is valid
        /usr/bin/${SVCNAME} --verify-config -f ${CHROOT}${CONFFILE} > /dev/null 2>&1
        if [ $? -eq 0 ] ; then
                einfo "Tor configuration (${CHROOT}${CONFFILE}) is valid."
                return 0
        else
                eerror "Tor configuration (${CHROOT}${CONFFILE}) not valid."
                /usr/bin/${SVCNAME} --verify-config -f ${CHROOT}${CONFFILE}
                return 1
        fi
}

start() {
        checkconfig || return 1
        ebegin "Starting chrooted Tor"
        HOME=/var/lib/${SVCNAME}
        echo "Cleaning any files left over from a previous run..."
        rm -rf /srv/torchroot/var/lib/tor/*
        # Detect old version and upgrade
        Torchroothash=$(sha256sum /srv/torchroot/usr/bin/tor | awk '{print $1}')
        Toroutsidehash=$(sha256sum /usr/bin/tor | awk '{print $1}')
        if [ "$Torchroothash" != "$Toroutsidehash" ]
        then
            	echo "New version of Tor detected! Updating chroot before running."
                rm -rf /srv/torchroot
                wait
                /usr/bin/sh -c "/usr/libexec/tor-hardened-scripts/torchroot.sh"
                wait
        fi
        start-stop-daemon --start --pidfile "${CHROOT}${PIDFILE}" --quiet --exec chroot -- --userspec=tor:tor ${CHROOT} /usr/bin/${SVCNAME} -f "${CONFFILE}" --runasdaemon 1 --PidFile "${PIDFILE}" > /dev/null 2>&1
        eend $?
}

stop() {
        ebegin "Stopping chrooted Tor"
        start-stop-daemon --stop --pidfile "${CHROOT}${PIDFILE}"
	rm -f "${CHROOT}${PIDFILE}"
        eend $?
}

graceful() {
        gracefulstop
        start
        eend $?
}

gracefulstop() {
        local rc=0
        ebegin "Gracefully stopping chrooted Tor"
        ebegin "This can take up to ${GRACEFUL_TIMEOUT} seconds"
        start-stop-daemon -P --stop --signal INT -R ${GRACEFUL_TIMEOUT} --pidfile "${CHROOT}${PIDFILE}"
        rc=$?
        eend "done"
        eend $rc
}

reload() {
        if [ ! -f ${CHROOT}${PIDFILE} ]; then
                eerror "${SVCNAME} isn't running"
                return 1
        fi
        checkconfig || return 1
        ebegin "Reloading chrooted Tor configuration"
        start-stop-daemon --signal HUP --pidfile ${CHROOT}${PIDFILE}
        eend $?
}
