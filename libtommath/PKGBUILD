# Maintainer (Arch): Giovanni Scafora <giovanni@archlinux.org>
# Contributor (Arch): suasageandeggs <s_stoakley@hotmail.co.uk>
# Contributor (Arch): Michael Fellinger <manveru@www.weez-int.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=libtommath
pkgver=1.2.0
_debver=$pkgver
_debrel=6
pkgrel=3
pkgdesc="Highly optimized and portable routines for integer based number theoretic applications"
url='https://www.libtom.org/'
license=('custom:LibTom')
arch=('i686' 'x86_64')
depends=('glibc')
makedepends=('quilt')
options=(staticlibs)
source=("https://github.com/libtom/libtommath/releases/download/v${pkgver}/ltm-${pkgver}.tar.xz"
        "https://deb.debian.org/debian/pool/main/libt/libtommath/libtommath_${_debver}-${_debrel}+deb12u1.debian.tar.xz")
sha512sums=('6f9ccd0691831f07f86ddc81cb6145504b3d5da66dd3e92312c64cce0ea986fa4e08ba65ca8991aaebe56702c2d7c15f309696785b813dffb4c112a4ad04b203'
            'f5e15a925ac227d4d8fc6aec38423449089f37858e035fb64d04767a74efe6fbefb4b12f81e0cbeac6af336707f6e7c3f9198bb4ea07118528e49601837eba99')

prepare() {
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/fix-shift-count-overflow-on-x32 || true

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver

  make -f makefile.shared IGNORE_SPEED=1
}

package() {
  cd $pkgname-$pkgver

  make -f makefile.shared PREFIX=/usr DESTDIR="$pkgdir" INSTALL_GROUP=root install
  install -Dm644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
