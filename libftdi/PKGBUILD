# Maintainer (Arch): Kyle Keen <keenerd@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=libftdi
pkgver=1.5
pkgrel=1
pkgdesc='A library to talk to FTDI chips, optional python bindings.'
arch=('i686' 'x86_64')
url='https://www.intra2net.com/en/developer/libftdi/download.php'
license=('GPL-2')
depends=('libusb' 'confuse')
optdepends=('python: library bindings')
makedepends=('boost' 'cmake' 'python' 'swig')
source=(https://www.intra2net.com/en/developer/libftdi/download/${pkgname}1-$pkgver.tar.bz2
        fix_includes_path.patch)
sha512sums=('c525b2ab6aff9ef9254971ae7d57f3549a36a36875765c48f947d52532814a2a004de1232389d4fe824a8c8ab84277b08427308573476e1da9b7db83db802f6f'
            '11e6ff5f5a0f1a53a4815bb1cfc49cbfbb13eca2f7ecbe6c08b105d04910d58cdb7ed1a116bffdaa7c541e23c25b1f1d48f1dd13320d1d3c207a8e31bc52533f')

prepare() {
  cd "${pkgname}1-$pkgver"
  sed -i 's|LIB_SUFFIX 64|LIB_SUFFIX ""|' CMakeLists.txt
  sed -i "s|MODE=\"0664\", GROUP=\"plugdev\"|TAG+=\"uaccess\"|g" packages/99-libftdi.rules

  patch -p1 < ../fix_includes_path.patch
}

build() {
  cd "$srcdir/${pkgname}1-$pkgver"
  mkdir -p build
  cd build
  cmake .. -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_SKIP_BUILD_RPATH=ON \
    -DCMAKE_BUILD_TYPE=Release -DEXAMPLES=OFF -DFTDI_EEPROM=ON \
    -DFTDIPP=ON -DPYTHON_BINDINGS=ON -DLINK_PYTHON_LIBRARY=ON
  make
}

package() {
  cd "${pkgname}1-$pkgver/build"

  make DESTDIR="$pkgdir" install
  install -Dm644 "../packages/99-libftdi.rules" "$pkgdir/lib/udev/rules.d/69-libftdi.rules"
  install -Dm644 "$srcdir/${pkgname}1-$pkgver/"COPYING.GPL -t "${pkgdir}/usr/share/licenses/$pkgname"
  install -d "$pkgdir/usr/share/libftdi/examples" 
  cp -r ../examples/* "$pkgdir/usr/share/libftdi/examples"
}
