# Contributor (Arch): carstene1ns <arch carsten-teibes de>
# Contributor (Arch): Miguel Revilla <yo@miguelrevilla.com>
# Contributor (Arch): Fabian Melters <melters [at] gmail [dot] com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=ultimatestunts
pkgver=0.7.7.1
pkgrel=3
pkgdesc="Free and libre stunt-racing game."
arch=('i686' 'x86_64')
url='https://sourceforge.net/projects/ultimatestunts/'
license=('GPL-2')
depends=('sdl_image' 'freealut' 'libvorbis' 'mesa-libgl' 'glu')
groups=('games')
source=("https://downloads.sourceforge.net/sourceforge/$pkgname/$pkgname-srcdata-${pkgver//./}.tar.gz"
        "$pkgname-metaserver-cpp-add-includes.patch"
        "$pkgname.png"
        "$pkgname.desktop")
sha512sums=('c532d1434570775da8e0b8156659f96dcfbdf82890f07aebbe398bdcf2039a11c832ead8106c8522af1a4a847f685727dc6f7739443a99858621995452ce4741'
            '1dcd4314b531f0a39be5aa480004d20d2bb4e7b735e5966199df16b9cb3268bd13d77699c9d3eb36920c043ac582d1c709c2acac74b6dbcd63a932ae69297f67'
            '36f9800cf023c1382ef21a43c65d7d63a4d6dc2d9956e0ccba239826ca9ace3ddb1565ef8ab171cb56ce5f736cae9803510f172b97fdf8efc5ee72a2bb95bd2d'
            'fea5fe64c242530ef4b48e74b9d974f7cdda6462c98a7a4bb3774b90e5fca749d18cdbe2fb1edfad4ee00a0422235e422f8fa3d05edfa202203ce480a7cc2f0e')

prepare() {
  patch -d $pkgname-srcdata-${pkgver//./} -Np0 < $pkgname-metaserver-cpp-add-includes.patch
}

build() {
  cd $pkgname-srcdata-${pkgver//./}

  ./configure \
    --prefix=/usr \
    --bindir=/usr/games \
    --datarootdir=/usr/share/games \
    --sysconfdir=/etc
  make -j1
}

package() {
  cd $pkgname-srcdata-${pkgver//./}

  make \
    sysconfdir="$pkgdir/etc" \
    prefix="$pkgdir/usr" \
    bindir="$pkgdir/usr/games" \
    datarootdir="$pkgdir/usr/share/games" \
    install

  # .desktop entry
  install -Dm644 ../$pkgname.png "$pkgdir"/usr/share/pixmaps/$pkgname.png
  install -Dm644 ../$pkgname.desktop "$pkgdir"/usr/share/applications/$pkgname.desktop

  # cleanup
  find "$pkgdir/" -type d -name .svn -print0 | xargs -0 rm -rf

  # license
  install -Dm644 COPYING -t "${pkgdir}"/usr/share/licenses/$pkgname
}
