# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor (Arch): meatballhat <daniel.buch@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=irssi-xmpp
pkgver=0.54
_debver=$pkgver
_debrel=2
pkgrel=2
pkgdesc="irssi plugin to connect to the Jabber network"
url='https://cybione.org/~irssi-xmpp'
arch=('i686' 'x86_64')
license=('GPL-2')
depends=('glib2' 'loudmouth' 'irssi')
makedepends=('quilt')
source=("https://github.com/cdidier/irssi-xmpp/archive/v${pkgver}/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/i/irssi-plugin-xmpp/irssi-plugin-xmpp_${_debver}+git20191101+c13fa5-${_debrel}.debian.tar.xz")
sha512sums=('31ddd9e0147eec23d08b8bcbe242e831d612cb2d00f3b56776e9d0b87db9bd76d4bb147422cdf18c0567f0e0463ab828ab7a0ef80a2b35cfdc0a8be962b39f14'
            'a45d9115c8b689feb94b835031bf3402eafffb275653ffbe3b52b147042f1170ed17493d93ea415f4279ed70ec7da57c2a40799190bde63c1befa17c656dcbd2')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/GTalk-MUC-support.patch  || true
    rm -v debian/patches/strip_resource_08082009.patch || true

    quilt push -av
  fi
}

build() {
  cd ${pkgname}-${pkgver}
  make PREFIX=/usr IRSSI_INCLUDE=/usr/include/irssi
}

package() {
  cd ${pkgname}-${pkgver}
  make PREFIX=/usr DESTDIR="${pkgdir}" install
  install -Dm644 README.md NEWS -t "${pkgdir}/usr/share/doc/${pkgname}"
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
}