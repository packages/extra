# Maintainer (Arch): Tobias Borgert <tobias.borgert@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=simpleini
pkgver=4.17
_debver=$pkgver
_debrel=6
pkgrel=1
pkgdesc="Cross-platform, free software C++ library providing a simple API to read and write INI-style configuration files"
arch=('i686' 'x86_64')
url='https://github.com/brofield/simpleini'
license=('Expat')
depends=('glibc')
makedepends=('quilt')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/brofield/simpleini/archive/${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/libs/libsimpleini/libsimpleini_${_debver}+dfsg-${_debrel}.debian.tar.xz")
sha512sums=('3a207641093836023b61d07a9e9537035b2a91d6a5cb71edbcd6068f26916cb56f9825154b1bee2541b2471ad137afde06629300653bb3d7ffe9deb0c18bccf1'
            '9ce8f9f4c368366799f0ea027051b97d741d05699f035b13d11dd0ad512f2d0e046233aeff7f81c01621d98fcacd68307cfacf3a80911dcc8220ed39521c15d5')

prepare() {
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  export VERSION=$pkgver
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 LICENCE.txt -t "${pkgdir}/usr/share/licenses/$pkgname"
}
