# Maintainer (Arch): Gaetan Bisson <bisson@archlinux.org>
# Contributor (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Contributor (Arch): Thayer Williams <thayer@archlinux.org>
# Contributor (Arch): damir <damir@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=abook
pkgver=0.6.1
_debver=$pkgver
_debrel=3
pkgrel=3
pkgdesc="Text-based addressbook designed for usage with Mutt"
url='http://abook.sourceforge.net/'
arch=('i686' 'x86_64')
license=('GPL-2')
makedepends=('quilt')
depends=('readline')
source=("https://abook.sourceforge.io/devel/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/a/abook/abook_${_debver}-${_debrel}.debian.tar.xz"
        "gcc5.patch"
        "fix-build.patch")
sha512sums=('993895ef2fd6d98d50db610699f8f0a28112778065ac56484373eb814afd8ab7371201fc21506c090514f4b29570ec229dc62c606ab068301152567e2c47fe66'
            '2063ff5381ba11520b8431f82c5503a6fadb78dbfe6c682f3fc0e396457ec2f126c4af4f725c4f28c8d602212499c09f6623eefa078df5f9927bf7ee932a1805'
            '21ab6475512ea9baae1a6509749628d6b610e96e77c64139d09697d272d66556875058e91c9b948c4ebaf65795a97d8532bae25e5d7e71cc4a20af49897cd867'
            'a6647f3b20fae768edce9c9aa19a4063ce4a499d3fb94b399d840063547e9f44bf5577780e683a14c3f9e6d917cfd9e9e279916682270e4c72f79242dd6037d5')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  aclocal && automake --add-missing && autoconf
  sed 's/0.18/0.19/g' -i po/Makefile.in.in
  patch -p1 -i $srcdir/gcc5.patch
  patch -p1 -i $srcdir/fix-build.patch
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  ./configure --prefix=/usr --mandir=/usr/share/man
  make
  cd po && make update-po
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
