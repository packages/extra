# Maintainer (Arch): David Runge <dvzrv@archlinux.org>
# Contributor (Arch): Giovanni Scafora <giovanni@archlinux.org>
# Contributor (Arch): farid abdelnour <farid at atelier-labs.org>
# Contributor (Arch): Gustavo Alvarez <sl1pkn07 at gmail.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=twolame
pkgver=0.4.0
_debver=0.4.0
_debrel=2
pkgrel=1
pkgdesc="An optimized MPEG Audio Layer 2 (MP2) encoder"
arch=('i686' 'x86_64')
url="https://www.twolame.org/"
license=('LGPL-2.1')
depends=('glibc' 'libsndfile')
makedepends=('quilt')
source=("https://github.com/njh/${pkgname}/releases/download/${pkgver}/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/t/twolame/twolame_$_debver-$_debrel.debian.tar.xz")
sha512sums=('cc594bc8d2322922280f915a3c0aa52540cca0350d6498bc96f3f60fd6e53f951e775ea015a44bdb29ec883b46b31a0e5483f6a5c188b02e30008289273c7d03'
            '5e8ecc5a9b8984bdbe3cab2e50897807cbecbef45d958a01720e599783774a86169e854181520fc0b9ce459b1bc248b2017c896112d0555f48baabf75a79cdee')

prepare() {
  cd "${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  autoreconf -vfi
}

build() {
  cd "${pkgname}-${pkgver}"
  ./configure --prefix=/usr \
              --disable-static
  make
}

package() {
  cd "${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING ${pkgdir}/usr/share/licenses/${pkgname}/COPYING
}
