# Maintainer (Arch): Tom Gundersen <teg@jklm.no>
# Maintainer (Arch): Ionut Biru <ibiru@archlinux.org>
# Contributor (Artix): artoo <artoo@cromnix.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad <rachad @ hyperbola . info>

pkgbase=transmission
pkgname=('transmission-cli' 'transmission-gtk')
pkgver=3.00
_debver=3.00
_debrel=2
pkgrel=3
arch=('i686' 'x86_64')
url='http://www.transmissionbt.com/'
license=('GPL-3')
makedepends=('gtk' 'intltool' 'curl' 'libevent' 'quilt')
source=(https://github.com/transmission/transmission-releases/raw/master/transmission-${pkgver}.tar.xz
        https://deb.debian.org/debian/pool/main/t/transmission/transmission_${_debver}-${_debrel}.debian.tar.xz
        fix-icons.patch
        transmission-daemon.initd
        transmission-daemon.confd
        transmission-daemon.run)
sha512sums=('eeaf7fe46797326190008776a7fa641b6341c806b0f1684c2e7326c1284832a320440013e42a37acda9fd0ee5dca695f215d6263c8acb39188c5d9a836104a61'
            '219161b6833fe8d18df491ba61901b8520c2d4c8018b4819fb1b4475530088aeeb949013dffa34eb99b2b1cf97a5718ba7269d7816d217a797500cf4e8c2f253'
            '356fa8b87dc3adf7e0ef45b161debcaee588d3b4b2aebc8225f7f4f7e0e77e6d1755fbc3f39412cec5688852338f618c97b2726f7794977e50c25ad028257c71'
            '30d9fe7d06ca3c92ff436e9bbe84729a5b343b52965f8d1d3d43f14f5fa25881dbc9676c5d3d916cff552aad5872ed41c2930e9ce3a5f7c958dbeec0d1ab1ac4'
            '47e78c85857eefb9be6c25d1e789cfb2a276d5816ba911ba1511e764469b2d9998edafe44247aff784113ca28926748bc5fb2b879280b780fd69ffb1e4e40736'
            'a21cc167a3ec845adf1e2b20585e7632dd64147e0f8b10b9d447be0e228433f76ff6c0bc85dca300bf6335e37428c86d9cfb3fc03127fae503cc68bde007475b')

prepare() {
  cd $pkgbase-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/systemd_service_fixes.patch || true
    rm -v debian/patches/ayatana-indicators.patch || true
    rm -v debian/patches/transmission-daemon_execstop_service.patch || true

    quilt push -av
  fi

  # additional fixes
  patch -Np1 -i ${srcdir}/fix-icons.patch

  rm -f m4/glib-gettext.m4
  autoreconf -fi
}

build() {
  cd $pkgbase-$pkgver
  ./configure --prefix=/usr
  make
}

package_transmission-cli() {
  pkgdesc='Fast, easy, and free BitTorrent client (CLI tools, daemon and web client)'
  depends=('curl' 'libevent')
  install=transmission-cli.install

  cd $pkgbase-$pkgver

  for dir in daemon cli web utils; do
    make -C "$dir" DESTDIR="$pkgdir" install
  done

  # install services
  install -Dm 644 "${srcdir}/transmission-daemon.confd" "${pkgdir}/etc/conf.d/transmission-daemon"
  install -Dm 755 "${srcdir}/transmission-daemon.initd" "${pkgdir}/etc/init.d/transmission-daemon"
  install -D -m 755 "$srcdir/transmission-daemon.run" "$pkgdir/etc/sv/transmission-daemon/run"

  # license
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}

package_transmission-gtk() {
  pkgdesc='Fast, easy, and free BitTorrent client (GTK+ GUI)'
  depends=('curl' 'libevent' 'gtk' 'desktop-file-utils' 'hicolor-icon-theme')
  optdepends=('transmission-cli: daemon and web support')

  cd $pkgbase-$pkgver

  make -C gtk DESTDIR="$pkgdir" install
  make -C po DESTDIR="$pkgdir" install

  # license
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}
