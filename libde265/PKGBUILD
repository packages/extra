# Maintainer (Arch): Antonio Rojas <arojas@archlinux.org>
# Contributor (Arch): James An <james@jamesan.ca>
# Contributor (Arch): Daniel Nagy <danielnagy at gmx de>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=libde265
pkgver=1.0.11
_debver=$pkgver
_debrel=1
pkgrel=1
pkgdesc="Free and libre h.265 video codec implementation"
arch=('i686' 'x86_64')
url='https://github.com/strukturag/libde265'
license=('LGPL-3')
depends=('gcc-libs')
makedepends=('ffmpeg' 'sdl' 'qt-base' 'quilt')
optdepends=('ffmpeg: for sherlock265'
            'qt-base: for sherlock265'
            'sdl: dec265 YUV overlay output')
source=("https://github.com/strukturag/libde265/releases/download/v${pkgver}/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/libd/libde265/libde265_${_debver}-${_debrel}+deb12u2.debian.tar.xz")
sha512sums=('2ce28558c66e20714c07bf3011bc10dccabb770649903616bc32f1c4f18beba559ef7e0e42365ead77d7e813316b8c051039dc393cd351221cbab7248b3fa34c'
            '45128028a97d4e89e70e898a2e036a61a27e662b68384cbe3fe8a8dd3616b076c1c5d527af2784f9fabf98833db089e5b7823504f070eeb85cc5bdc68bbae2e8')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  autoreconf -vfi
}

build() {
  cd $pkgname-$pkgver
  ./configure \
    --prefix=/usr \
    --enable-static=no
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING $pkgdir/usr/share/licenses/$pkgname/COPYING
}
