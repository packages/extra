# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=shush
pkgver=1.2.3
_debver=$pkgver
_debrel=5
pkgrel=1
pkgdesc="Free and libre cron-wrapper, with multiple report formats, syslogging and more"
arch=('i686' 'x86_64')
url='http://web.taranis.org/shush/'
license=('Modified-BSD')
depends=('pcre')
makedepends=('quilt')
source=("${pkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/s/shush/shush_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/s/shush/shush_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('989562bc43ca486151cef0a8254a29678d6084396df04b6d4c8d4c281341aad24c2b248e3c907c3561efb6ecf39c968df1e3bf4d1c59c04390a3aaa16dc76be0'
            '21984f9ae8501d03040d6ccf047ece7201d0c9426fcd0b2827e4dd2c8fd4b855c6779f77f04de6b6bd3a6cfb210ce96d7d2292e5af6cf8704c6cf51b4ae8c2c3')

prepare() {
  cd "${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "${pkgname}-${pkgver}"
  ./configure --prefix=/usr
  make
}

package() {
  cd "${pkgname}-${pkgver}"
  install -Dm755 src/$pkgname -t "$pkgdir/usr/bin"
  install -Dm644 $pkgname.1 -t "$pkgdir/usr/share/man/man1"
  install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}