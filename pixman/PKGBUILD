# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer (Arch): Andreas Radke <andyrtr@archlinux.org>
# Contributor (Arch): Alexander Baldeck <alexander@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=pixman
_openbsdver=6.9
pkgver=0.38.4
pkgrel=2
pkgdesc="The pixel-manipulation library for X and cairo, provided by Xenocara"
arch=(i686 x86_64)
url="https://www.xenocara.org"
license=('Expat')
depends=('glibc')
makedepends=('xenocara-util-macros')
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/lib/pixman-$pkgver.tar.lz{,.sig})
sha512sums=('13c4ae06cb1a343e2f6eff5dc6c175bf36bce09a0774d608499e42d524e7e0e3cad4702dd5b9d37c423f7b37d0a548e3e970055529ea3aeb547009c3b56ce5d6'
            'SKIP')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/lib/pixman"
  autoreconf -vfi
}

build() {
  cd "xenocara-$_openbsdver/lib/pixman"
  ./configure --prefix=/usr --disable-static
  make
}

check() {
  cd "xenocara-$_openbsdver/lib/pixman"
  make check
}

package() {
  cd "xenocara-$_openbsdver/lib/pixman"
  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
