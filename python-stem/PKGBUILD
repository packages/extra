# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor (Arch): Spider.007 <archlinux AT spider007 DOT net>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad

pkgname=python-stem
_pypiname=${pkgname#*-}
pkgver=1.8.1
_debver=$pkgver
_debrel=2.1
pkgrel=2
pkgdesc="Python controller library for Tor"
url='https://stem.torproject.org/'
arch=('any')
license=('GPL-3' 'Simplified-BSD' 'CC0-1.0' 'Expat' 'Public-Domain' 'GPL-2')
depends=('python' 'python-cryptography' 'python-pynacl' 'procps-ng')
makedepends=('quilt')
optdepends=('tor: tor-server to talk to')
replaces=('stem')
provides=('stem')
source=("${pkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/p/python-stem/python-stem_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/p/$pkgname/${pkgname}_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('bad2f4f96e37caa3ee2928bb027318c4941ca3f6c0072c5f6e87fe647d2b68400ad352d9c27fa2a35c00f4f327aa9cc00e2907b21a9cbd26fab46ec21e2a038a'
            '035e8279a746b661355f87a33d8faa097c9a498d1a5c6177f9a605d29643f6f848acc9e17652862135648d37dbf1a921bfc5371a1559a65cee3b8a1da0695ff9')

prepare() {
  cd ${_pypiname}-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  # https://github.com/torproject/stem/issues/56
  sed -i '/MOCK_VERSION/d' run_tests.py
  # remove flaky integration tests
  sed -i test/settings.cfg \
    -e '/|test.integ.client.connection.TestConnection/d' \
    -e '/|test.integ.process.TestProcess/d' \
    -e '/|test.integ.installation.TestInstallation/d'
  rm test/integ/{client/connection,{installation,process}}.py
}

build() {
  cd ${_pypiname}-${pkgver}
  python setup.py build
}

package() {
  cd ${_pypiname}-${pkgver}
  python setup.py install --optimize=1 --root="${pkgdir}" --skip-build
  install -Dm644 debian/copyright LICENSE -t $pkgdir/usr/share/licenses/$pkgname/
}
