# Maintainer (Arch): Andreas Radke <andyrtr@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=libevdev
pkgver=1.11.0
_debver=1.11.0+dfsg
_debrel=1
pkgrel=2
pkgdesc="Wrapper library for evdev devices"
arch=(i686 x86_64)
url="https://www.freedesktop.org/wiki/Software/libevdev/"
license=(X11)
depends=('glibc')
makedepends=('python' 'check' 'doxygen' 'quilt')
source=(https://freedesktop.org/software/$pkgname/$pkgname-$pkgver.tar.xz{,.sig}
        https://deb.debian.org/debian/pool/main/libe/libevdev/libevdev_$_debver-$_debrel.debian.tar.xz)
sha512sums=('b3c6b4a1532c5142f3684b920dcdd497a5368aec516e8c389e2c8f9d1eaba16c907b2a7f2c82a3c3ff89bb4d0ae5503c736098c095f9f1bc98f0668e99bf639d'
            'SKIP'
            '402c7667b463eea870af738dccf95bf972425e55c264e297196c417046082ac8aa8f0a1f738ea6d0afa2af637970e0dae88a17d5fca6f6e5862f58621b00fda7')
validpgpkeys=('3C2C43D9447D5938EF4551EBE23B7E70B467F0BF') # Peter Hutterer (Who-T) <office@who-t.net>

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr \
    --disable-static
  make
}
check() {
  cd $pkgname-$pkgver
  # some failures are "expected"
  make check || true
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/COPYING"
}